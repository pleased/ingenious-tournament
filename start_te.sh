#!/bin/bash
echo "Building match containers"

cd dockerfiles/
docker build -f Dockerfile-ref -t ref .
docker build -f Dockerfile-lobby -t lobby .
docker build -f Dockerfile-player -t player .

cd ..

echo "Cleaning up docker images/containers"
#Clean up old containers
docker ps --filter "status=exited" | grep 'weeks ago' | awk '{print $1}' | xargs --no-run-if-empty docker rm
docker images | grep "<none>" | awk '{print $3}' | xargs docker rmi -f
docker rm TE_web


echo "Starting Tournament Engine docker"
#Start docker image
docker run -i -t -d -p "80:80" -v /var/run/docker.sock:/var/run/docker.sock -v ${PWD}/web:/app -v ${PWD}/mysql:/var/lib/mysql --name "TE_web" web_te:latest

## Seperate the db app and website in seperate containers.
#docker run -i -t -d -p "80:80" -v ${PWD}/web:/app --name "TE_web" web_te:latest
#docker run -t -t -d -v /var/run/docker.sock:/var/run/docker.sock --name "TE_app" app_te:latest
#docker run -t -t -d -v ${PWD}/mysql:/var/lib/mysql --name "TE_db" db_te:latest

echo "Setting up ....."
sleep 40
echo "Successfully Started docker container"
echo "Setting up database..."
#Create mysql database and cs-games user
docker exec TE_web /init_db.sh
echo "Successfully configured database"

#Set up default scheduler/ranker/engines

echo "Setting up application server"
#Start App server
docker exec TE_web /init_te.sh
echo "Successfully configured application server"

echo "Successfully Started Tournament Engine!"