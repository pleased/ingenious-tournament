#!/bin/bash
mysql -uroot -e "CREATE USER 'cs-games'@'localhost' IDENTIFIED BY 'cs-games1234';"
mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'cs-games'@'localhost'; FLUSH PRIVILEGES;"
mysql -uroot -e "CREATE DATABASE TE;"
mysql -uroot TE < /TE.sql

cd /var/www/html/TE/
composer install

mkdir uploads
mkdir uploads/ranker_uploads
mkdir uploads/scheduler_uploads
mkdir uploads/config_uploads
mkdir uploads/player_uploads
mkdir uploads/engine_uploads

chmod -R 0755 uploads
