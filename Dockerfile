FROM mattrayner/lamp:latest-1604

#Copy db config
ADD config/db_config.php usr/share/php/

#Install java and update installed packages (image uses old php and mysql versions)
RUN apt-get clean && \
	apt-get update && \
    apt-get upgrade -y && \
    apt-get dist-upgrade -y && \
    apt-get install -y  software-properties-common && \
    add-apt-repository ppa:webupd8team/java -y && \
    apt-get update && \
    echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
    apt-get install -y oracle-java8-set-default && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    wget https://services.gradle.org/distributions/gradle-3.4.1-bin.zip && \
    mkdir /opt/gradle && \
    unzip -d /opt/gradle gradle-3.4.1-bin.zip && \
    export PATH=$PATH:/opt/gradle/gradle-3.4.1/bin && \
    apt-get clean


RUN curl -fsSLO https://get.docker.com/builds/Linux/x86_64/docker-17.03.1-ce.tgz && \
    tar --strip-components=1 -xvzf docker-17.03.1-ce.tgz -C /usr/local/bin


#Add lib/ dependencies
ADD lib/ /usr/lib/jvm/java-8-oracle/jre/lib/ext/

ADD web/TE/db/TE.sql /

ADD init_db.sh /
ADD init_te.sh /

RUN chmod +x /init_db.sh
RUN chmod +x /init_te.sh

ADD /engine /TE

CMD ["./run.sh"]
