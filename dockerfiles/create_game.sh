#!/bin/bash

JARNAME="/ref.jar"
CONFIG="/config.json"
GAME=$3
LOBBY=$4
PLAYERS=$5
PORT=$6
HOSTNAME=$7

while ! test -f $CONFIG; do
  sleep 10
done

sleep 5

java -jar $JARNAME create -config $CONFIG -game $GAME -lobby $LOBBY -players $PLAYERS -hostname $HOSTNAME -port $PORT
