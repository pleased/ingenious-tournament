#!/bin/bash

#JARNAME=$(cat match_settings.json | jq '.ref.jarname')
#REFNAME=$(cat match_settings.json | jq '.ref.refname')
#PORT=$(cat match_settings.json | jq '.ref.port')

JARNAME="ref.jar"
REFNAME=$2
PORT=$3

#echo $JARNAME
#echo $REFNAME
while ! test -f /$JARNAME; do
  sleep 10
done

java -jar /$JARNAME server -C $REFNAME -port $PORT
