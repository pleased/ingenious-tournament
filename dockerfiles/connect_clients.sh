#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
#JARNAME=$(cat match_settings.json | jq '.player.jarname')
#JARNAME="${JARNAME%\"}"
#JARNAME="${JARNAME#\"}"
#PLAYERNAME=$(cat match_settings.json | jq '.player.playername')
#PLAYERPATH=$(cat match_settings.json | jq '.player.playerpath')
#GAME=$(cat match_settings.json | jq '.player.game')
#PORT=$(cat match_settings.json | jq '.player.port')
JARNAME="/player.jar"
PLAYERNAME=$3
PLAYERPATH=$2
GAME=$4
PORT=$5
HOSTNAME=$6

while ! test -f "/config.json"; do
  sleep 10
done
sleep 15

java -jar $JARNAME client -username $PLAYERNAME -engine $PLAYERPATH -game $GAME -hostname $HOSTNAME -port $PORT

#java -jar players/RandomMNK.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine" -game "mnk" -hostname REF -port 61234 &

