<?php

	session_start();

	include "TE/php/routes.php";

	if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];

    $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
	$uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
	if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
	$uri = '/' . trim($uri, '/');
    $uri = trim($uri, '/');
    $uri = substr($uri, 10, strlen($uri) - 10);

    if ($uri == 'home' || $uri == '') {
        home_page();
	} else if ($uri == 'set_displayname') {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
		    $dname = $_POST['displayname'];
		    set_displayname_call($dname, $user_id);
		}

	} else if ($uri == 'login') {
		login();
	} else if ($uri == 'logout') {
		if(!isset($_SESSION)) {
        	session_start();
    	}
		unset($_SESSION['access_token']);
		unset($_SESSION['user_email']);
		login();
	} else if ($uri == 'login_request') {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
		    $email = $_POST['email'];
		    $pass = $_POST['pass'];
		    login_in_authentication($email, $pass);
		}

	} else if($uri == 'google-login-request') {
		$code = $_GET['code'];

		google_login($code);

	} else if ($uri == 'create_tournament') {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
		    $name = $_POST['name'];
		    $engine = $_POST['engine'];
		    $ranker = $_POST['ranker'];
		    $scheduler = $_POST['scheduler'];
		    $max_users = $_POST['max_users'];
		    $max_players = $_POST['max_players'];
		    $limit_resources = $_POST['limit_resources'];
		    $limit_mem = $_POST['limit_memory'];
		    $mem_affix = $_POST['affix'];
		    $limit_cpu = $_POST['limit_cpu'];
		    $private = $_POST['private'];
		    create_tournament_call($user_id, $name, $engine, $ranker, $scheduler, $limit_resources, $max_users, $max_players, $private, $limit_mem, $limit_cpu, $mem_affix);
		}
    } else if ($uri == 'update_tournament') {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
			$tournament_id = $_POST['tournament_id'];
		    $name = $_POST['name'];
		    $ranker = $_POST['ranker'];
		    $max_users = $_POST['max_users'];
		    $max_players = $_POST['max_players'];
		    $private = $_POST['private'];
		    $mem = $_POST['limit_memory'];
		    $affix = $_POST['affix'];
		    $cpu = $_POST['limit_cpu'];

		    update_tournament_call($tournament_id, $user_id, $name, $ranker, $max_users, $max_players, $private, $mem, $affix, $cpu);
		}
	} else if ($uri == 'update_tournament_config') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$config_file = $_POST['config_file'];
			$tournament_id = $_POST['tournament_id'];
			$user_id = $_POST['user_id'];
			update_tournament_config_call($tournament_id, $user_id, $config_file);
		}
    } else if ($uri == 'add_player') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
		    $player_name = $_POST['player_name'];
		    $class_name = $_POST['class_name'];
		    add_player_call($player_name, $user_id, $class_name);
		}
    } else if ($uri == 'delete_player') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
		    $player_id = $_POST['player_id'];
		    delete_player_call($player_id, $user_id);
		}

    } else if ($uri == 'delete_user') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
			$tournament_id = $_POST['tournament_id'];
		    $admin_id = $_POST['admin_id'];
		    delete_user_call($admin_id, $user_id, $tournament_id);
		}

    } else if ($uri == 'add_scheduler') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['id'];
		    $scheduler_name = $_POST['name'];
		    add_scheduler_call($scheduler_name, $user_id);
		}
    } else if ($uri == 'update_scheduler') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['id'];
		    $scheduler_name = $_POST['name'];
		    $scheduler_id = $_POST['scheduler_id'];
		    update_scheduler_call($scheduler_name, $scheduler_id, $user_id);
		}
    } else if ($uri == 'update_ranker') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['id'];
		    $ranker_name = $_POST['name'];
		    $ranker_id = $_POST['ranker_id'];
		    update_ranker_call($ranker_name, $ranker_id, $user_id);
		}
    } else if ($uri == 'delete_scheduler') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$scheduler_id = $_POST['scheduler_id'];
		    $user_id = $_POST['user_id'];
		    delete_scheduler_call($scheduler_id, $user_id);
		}
   	} else if ($uri == 'add_ranker') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['id'];
		    $ranker_name = $_POST['name'];
		    $file = $_POST['file'];
		    add_ranker_call($ranker_name, $user_id, $file);
		}
    } else if ($uri == 'delete_ranker') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
		    $ranker_id = $_POST['ranker_id'];
		    delete_ranker_call($ranker_id, $user_id);
		}
    } else if ($uri == 'add_engine') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['id'];
		    $engine_name = $_POST['name'];
		    $classname = $_POST['classname'];
		    $public = $_POST['public'];
		    add_engine_call($engine_name, $user_id, $classname, $public);
		}
   	} else if ($uri == 'delete_engine') {
    	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$engine_id = $_POST['engine_id'];
		    $user_id = $_POST['user_id'];
		    delete_engine_call($engine_id, $user_id);
		}
   	} else if ($uri == 'add_player_to_tournament') {
   		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
			$player_id = $_POST['player_id'];
			$tournament_id = $_POST['tournament_id'];
		    add_player_to_tournament_call($user_id, $player_id, $tournament_id);
		}
   	} else if ($uri == 'delete_player_from_tournament') {
   		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user_id = $_POST['user_id'];
			$player_id = $_POST['player_id'];
			$tournament_id = $_POST['tournament_id'];
		    delete_player_from_tournament_call($user_id, $player_id, $tournament_id);
		}
   	} else {
		missing_page();
	}
	exit;
?>