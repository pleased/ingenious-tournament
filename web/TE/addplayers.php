<?php

	include("php/connect.php");

	// Get Tournament1 id
	$link = connect();
	$sql = "SELECT id FROM tournaments WHERE name = ?";
	$name = "Tournament1";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("s", $name);
	$res = $stmt->execute();
	$stmt->bind_result($tournament_id);
	$stmt->fetch();

	if (!$res) {
		die("No tournament1 exists...");
	}
	close($link);

	// Delete matches generated
	$link = connect();
	$sql = "INSERT INTO players (user_id, tournament_id, rating, status, name, dir) VALUES (?, ?, ?, ?, ?, ?)";
	$user_id = 1;
	$rating = 0;
	$status = 0;
	$name = "Player";
	$dir = "";
	$i = 1;
	$stmt = $link->prepare($sql);

	while ($i <= 20) {
		if ($i == 11) {
			$user_id = 2;
		}
		$name = $name.$i;
		/*echo $user_id . "<br>";
		echo $tournament_id . "<br>";
		echo $rating . "<br>";
		echo $status . "<br>";
		echo $name . "<br>";
		echo $dir . "<br>";*/

		$stmt->bind_param("iiiiss", $user_id, $tournament_id, $rating, $status, $name, $dir);
		$res = $stmt->execute();
		$name = "Player";
		$i = $i + 1;
	}
	close($link);

	header("Location: http://localhost/TE/secret.php");
	exit();


?>