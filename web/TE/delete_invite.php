<?php
	include "php/connect.php";
	include "php/sessions.php";

	if (isset($_GET)) {
		if (isset($_GET['invite_id'])) {
			$invite_id = $_GET['invite_id'];
		} else {
			header('Location: /TE/my_invitations.php?error=<strong>Could not accept the invite, since no invite has been selected.</strong>');
		}

	} else {
		header('Location: /TE/my_tournaments.php?error=<strong>Could not accept the invite since no invite was selected.</strong>');
	}

	$user_id = getUserId($_SESSION['user_email']);

	$link = connect();
	$sql = "SELECT tournament_id, status FROM tournament_invitations WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $invite_id);
	$stmt->execute();
	$stmt->bind_result($tournament_id, $status);
	$stmt->fetch();

	//Check admin of tournament
	$isAdmin = isUserAdmin($user_id, $tournament_id);

	if (!$isAdmin) {
		header("Location: /TE/my_tournaments.php?error=<strong>You are not admin of the tournament</strong>");
		exit();
	}

	$tournament_name = getTournamentName($tournament_id);

	$link = connect();
	$sql = "DELETE FROM tournament_invitations WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $invite_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>Your invitation could not be declined. Please contact an administrator.</strong>");
	} else {

		$success = "<strong>You successfully deleted the invitation.</strong>";
		header("Location: /TE/manage_tournament.php?id=$tournament_idsuccess=".$success);

	}


?>