-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 26, 2017 at 08:33 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 5.6.31-4+ubuntu16.04.1+deb.sury.org+4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `TE`
--

-- --------------------------------------------------------

--
-- Table structure for table `engines`
--

CREATE TABLE `engines` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `file` varchar(1024) NOT NULL,
  `path_to_ref` varchar(1024) NOT NULL,
  `public` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `engines`
--

INSERT INTO `engines` (`id`, `name`, `file`, `path_to_ref`, `public`, `user_id`) VALUES
(1, 'MNKReferee', '/var/www/html/TE/uploads/engine_uploads/MNKReferee.jar', 'za.ac.sun.cs.ingenious.games.mnk.MNKReferee', 1, 1),
(2, 'DomineeringReferee', '/var/www/html/TE/uploads/engine_uploads/DomineeringReferee.jar', 'za.ac.sun.cs.ingenious.games.domineering.DomineeringReferee', 1, 1),
(3, 'TicTacToeReferee', '/var/www/html/TE/uploads/engine_uploads/TicTacToeReferee.jar', 'za.ac.sun.cs.ingenious.games.tictactoe.TTTReferee', 1, 1),
(4, 'N-PuzzleReferee', '/var/www/html/TE/uploads/engine_uploads/N-PuzzleReferee.jar', 'za.ac.sun.cs.ingenious.games.npuzzle.PZLReferee', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `num_players` int(11) NOT NULL,
  `player_ids` varchar(10) NOT NULL,
  `status` int(11) NOT NULL,
  `user_ids` varchar(1024) NOT NULL,
  `result_id` int(11) NOT NULL,
  `archived` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `match_log_files`
--

CREATE TABLE `match_log_files` (
  `id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `log_dir` text NOT NULL,
  `archived` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `match_result`
--

CREATE TABLE `match_result` (
  `result_id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `result` varchar(1024) NOT NULL,
  `winner` int(11) NOT NULL,
  `archived` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `dir` varchar(1024) NOT NULL,
  `classname` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `user_id`, `status`, `name`, `dir`, `classname`) VALUES
(3, 3, 0, 'MNKRandomEngine', '/var/www/html/TE/uploads/player_uploads/MNKRandomEngine.jar', 'za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine'),
(4, 3, 0, 'MNKMCTSPlayer', '/var/www/html/TE/uploads/player_uploads/MNKMCTSPlayer.jar', 'za.ac.sun.cs.ingenious.games.domineering.DomineeringMCTSEngine'),
(6, 3, 0, 'NPuzzleAstar', '/var/www/html/TE/uploads/player_uploads/NPuzzleAstar.jar', 'za.ac.sun.cs.ingenious.games.npuzzle.engines.PZLAStarEngine'),
(7, 1, 0, 'MNKRandomEngine2', '/var/www/html/TE/uploads/player_uploads/MNKRandomEngine.jar', 'za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine'),
(8, 1, 0, 'MNKMCTSPlayer2', '/var/www/html/TE/uploads/player_uploads/MNKMCTSPlayer.jar', 'za.ac.sun.cs.ingenious.games.domineering.DomineeringMCTSEngine');

-- --------------------------------------------------------

--
-- Table structure for table `rankers`
--

CREATE TABLE `rankers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `file` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rankers`
--

INSERT INTO `rankers` (`id`, `user_id`, `name`, `file`) VALUES
(1, -1, 'EloRanker', '/var/www/html/TE/uploads/ranker_uploads/EloRanker.jar'),
(2, -1, 'ScoreRanker', '/var/www/html/TE/uploads/ranker_uploads/ScoreRanker.jar');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `file` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `name`, `file`) VALUES
(1, 'Standard Docker Image', ''),
(2, 'Low Memory Docker Image', ''),
(3, 'High Memory Docker Image', ''),
(4, 'Low CPU Docker Image', ''),
(5, 'High CPU Docker Image', '');

-- --------------------------------------------------------

--
-- Table structure for table `schedulers`
--

CREATE TABLE `schedulers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `file` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedulers`
--

INSERT INTO `schedulers` (`id`, `user_id`, `name`, `file`) VALUES
(12, -1, '2PlayerRoundRobin', '/var/www/html/TE/uploads/scheduler_uploads/2PlayerRoundRobin.jar'),
(13, -1, '1PlayerRoundRobin', '/var/www/html/TE/uploads/scheduler_uploads/1PlayerRoundRobin.jar'),
(14, -1, '2PlayerKnockout', '/var/www/html/TE/uploads/scheduler_uploads/2PlayerKnockout.jar');

-- --------------------------------------------------------

--
-- Table structure for table `tournaments`
--

CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `engine_id` int(11) NOT NULL,
  `ranker_id` int(11) NOT NULL,
  `scheduler_id` int(11) NOT NULL,
  `private` int(11) NOT NULL,
  `max_users` int(11) NOT NULL,
  `max_players` int(11) NOT NULL,
  `resources_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `config` varchar(1024) DEFAULT NULL,
  `archived` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournaments`
--

INSERT INTO `tournaments` (`id`, `user_id`, `name`, `engine_id`, `ranker_id`, `scheduler_id`, `private`, `max_users`, `max_players`, `resources_id`, `status`, `config`, `archived`) VALUES
(7, 3, 'ExampleTournament', 1, 2, 14, 0, 5, 5, 1, 0, '/var/www/html/TE/uploads/config_uploads/tournament_7.json', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tournament_invitations`
--

CREATE TABLE `tournament_invitations` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(1024) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tournament_players`
--

CREATE TABLE `tournament_players` (
  `tournament_player_id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` double(11,0) NOT NULL,
  `status` int(11) NOT NULL,
  `active_count` int(11) NOT NULL,
  `scheduled_count` int(11) NOT NULL,
  `archived` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournament_players`
--

INSERT INTO `tournament_players` (`tournament_player_id`, `tournament_id`, `player_id`, `user_id`, `rating`, `status`, `active_count`, `scheduled_count`, `archived`) VALUES
(23, 7, 3, 3, 0, 0, 0, 0, 0),
(24, 7, 4, 3, 0, 0, 0, 0, 0),
(25, 7, 7, 1, 0, 0, 0, 0, 0),
(26, 7, 8, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tournament_users`
--

CREATE TABLE `tournament_users` (
  `tournament_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin` int(11) NOT NULL,
  `archived` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournament_users`
--

INSERT INTO `tournament_users` (`tournament_id`, `user_id`, `admin`, `archived`) VALUES
(7, 3, 1, 0),
(7, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` varchar(100) NOT NULL,
  `display_name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `display_name`) VALUES
(1, 'root', '6beacfd4bb1ed6715291fa1000692ff3', 'root'),
(3, 'linde.henry@gmail.com', '2d664feb111ebc50c56165966d077f8e', 'Henry');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `engines`
--
ALTER TABLE `engines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `match_log_files`
--
ALTER TABLE `match_log_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `match_result`
--
ALTER TABLE `match_result`
  ADD PRIMARY KEY (`result_id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rankers`
--
ALTER TABLE `rankers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedulers`
--
ALTER TABLE `schedulers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tournament_invitations`
--
ALTER TABLE `tournament_invitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tournament_players`
--
ALTER TABLE `tournament_players`
  ADD PRIMARY KEY (`tournament_player_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `engines`
--
ALTER TABLE `engines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `match_log_files`
--
ALTER TABLE `match_log_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `match_result`
--
ALTER TABLE `match_result`
  MODIFY `result_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `rankers`
--
ALTER TABLE `rankers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `schedulers`
--
ALTER TABLE `schedulers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tournament_invitations`
--
ALTER TABLE `tournament_invitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tournament_players`
--
ALTER TABLE `tournament_players`
  MODIFY `tournament_player_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
