<?php
	include "php/connect.php";
	include "php/sessions.php";
	include "php/errors.php";
	include "php/socket.php";

	//Get user id
	$user_id = getUserId($_SESSION['user_email']);

	//Check if user is admin
	$isAdmin = isUserAdmin($user_id, $tournament_id);

	if (!$isAdmin) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>You do not have sufficient rights to pause the tournament.</strong>");
		exit();
	}

	//Check if tournament is already started
	$status = getTournamentStatus($tournament_id);

	if ($status == 3) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The tournament has already been Stopped</strong>");
		exit();
	} else {
		$link = connect();
		$sql = "UPDATE tournaments SET status = ? WHERE id = ?";
		$status = 3;
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ii", $status, $tournament_id);
		$res = $stmt->execute();

		if (!$res) {
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The tournament could not be paused..</strong>");
			exit();
		}

		$message = "Stop_tournament $tournament_id\n";
		send_to_server($message);

		header("Location: /TE/manage_tournament.php?id=$tournament_id&success=<strong>The tournament has successfully been Stopped!</strong>");
		exit();
	}



?>