<?php
	include "php/connect.php";

	require_once '/var/www/html/TE/vendor/autoload.php';

	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		//Get tournament id and user that is inviting
		$user_id = $_POST['user_id'];
		$tournament_id = $_POST['tournament_id'];
		$tournament_name = getTournamentName($tournament_id);
		$user_name = getUserName($user_id);
		$link_to_invitation = "localhost/TE/tournament_invitations.php";

		//Check if the user submitted a single email or a csv file.
	    $isMultiple = $_POST['single_or_multiple'];
	    if ($isMultiple == 1) {

			$csv = array_map('str_getcsv', file($_FILES['email_csv']['tmp_name']));
		    array_walk($csv, function(&$a) use ($csv) {
		      $a = array_combine($csv[0], $a);
		    });
		    array_shift($csv); # remove column header

		    $to = Array();
		    $link = connect();
		    $sql = "INSERT INTO tournament_invitations (tournament_id, user_id, email, status) VALUES (?,?,?,?);";
		    $stmt = $link->prepare($sql);
		    $failed = Array();

	    	foreach ($csv as $key => $email) {
	    		$temp = "".$email["Email"]."";
	    		if (!isValidEmail($email["Email"])) {
	    			array_push($failed, $email["Email"]);
	    			continue;
	    		}

	    		$link1 = connect();
	    		$sql1 = "SELECT id FROM users WHERE email = ?;";
	    		$stmt1 = $link1->prepare($sql1);
	    		$stmt1->bind_param("s", $email['Email']);
	    		$stmt1->execute();
	    		$stmt1->store_result();
	    		$num_rows = $stmt1->num_rows;
	    		if ($num_rows == 0) {
	    			$id = -1;
	    		} else {
	    			$stmt1->bind_result($id);
	    			$stmt1->fetch();
	    		}
	    		$status = 0;
	    		$stmt->bind_param("iisi", $tournament_id, $id, $temp, $status);
	    		$res = $stmt->execute();

	    		if ($res) {
	    			array_push($to, $temp);
	    		} else {
	    			array_push($failed, $temp);
	    		}
	    	}
	    	$message = new Swift_Message("$tournament_name Invitation");
				$message->setTo($to);
				$message->setFrom(["webmaster@sun.ac.za" => 'webmaster']);
				$message->setBody("Hello,\n You have been invited to join $user_name on Tournament Engine for the Inginious Framework \nTo join simple visit $link_to_invitation to view all outstanding invitations.\n \n Enjoy!");

			$fail = mail_now($message);

			//Check for no failed recipients
			if ($fail[0] != 0) {
				array_push($failed, $fail);
			}

			//Check if anything failed
			if (empty($failed)) {
				header("Location: /TE/manage_tournament.php?id=$tournament_id&success=<strong>Successfully emailed!</strong>");
			} else {
				if (!isset($_SESSION)) {
					session_start();
				}
				$_SESSION['failed'] = $failed;
				header("Location: /TE/manage_tournament.php?id=$tournament_id&success=<strong>Successfully emailed!</strong>&failed=1");
			}

	    } else {
	    	$email = $_POST['single_email'];

	    	$link = connect();
		    $sql = "INSERT INTO tournament_invitations (tournament_id, user_id, email, status) VALUES (?,?,?,?);";
		    $stmt = $link->prepare($sql);
		    $status = 0;
		    $stmt->bind_param("iisi", $tournament_id, $user_id, $email, $status);
		    $res = $stmt->execute();

		    if (!$res) {
		    	header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>Invite failed..</strong>&failed=1");
		    }

			// Create a message
			$message = new Swift_Message("$tournament_name Invitation");
			$message->setTo(["$email" => '$email']);
			$message->setFrom(["webmaster@sun.ac.za" => 'webmaster']);
			$message->setBody("Hello,\n You have been invited to join $user_name on <strong>Tournament Engine for the Inginious Framework</strong>\nTo join simple visit $link_to_invitation to view all outstanding invitations.\n \n Enjoy!");

			// Send the message
			$fail = mail_now($message);

			if ($fail != 0) {
				$_SESSION['failed'] = $fail;
				header("Location: /TE/manage_tournament.php?id=$tournament_id&success=<strong>Successfully emailed!</strong>&failed=1");
			} else {
				header("Location: /TE/manage_tournament.php?id=$tournament_id&success=<strong>Successfully emailed!</strong>");
			}

		}
	}

	function mail_now($message) {
		// Create the Transport
		$transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
			->setUsername('linde.henry@gmail.com')
			->setPassword('qebevrngnqmswccr');

		// Create the Mailer using your created Transport
		$mailer = new Swift_Mailer($transport);

		$result = $mailer->send($message,$failures);

		if (!$result) {
			return $failures;
		} else {
			return 0;
		}

	}

	function isValidEmail($email){
	    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
	}


?>