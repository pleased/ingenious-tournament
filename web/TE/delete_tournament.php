<?php
	include "php/connect.php";
	include "php/sessions.php";
	include "php/errors.php";
	include "php/socket.php";

	//Get user id
	$user_id = getUserId($_SESSION['user_email']);

	//Check if user is admin
	$isAdmin = isUserAdmin($user_id, $tournament_id);

	if (!$isAdmin) {
		header("Location: /TE/archived_tournament.php?id=$tournament_id&error=<strong>You do not have sufficient rights to start the tournament.</strong>");
		exit();
	}

	//Check if tournament is already started
	$status = getTournamentStatus($tournament_id);

	$tournament_name = getTournamentName($tournament_id);

	/**
	 Delete archive folder
	*/

	$dir = "/var/www/html/TE/archives/Tournaments/$tournament_name/";
	$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
	$files = new RecursiveIteratorIterator($it,
	             RecursiveIteratorIterator::CHILD_FIRST);
	foreach($files as $file) {
	    if ($file->isDir()){
	        rmdir($file->getRealPath());
	    } else {
	        unlink($file->getRealPath());
	    }
	}
	rmdir($dir);


	/**
	* Deletes tournament from database
	*/
	$link = connect();
	$sql = "DELETE FROM tournaments WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/archived_tournament.php?id=$tournament_id&error=<strong>The tournament could not be deleted..</strong>");
		exit();
	}

	/**
		Delete tournament users from database
	*/
	$link = connect();
	$sql = "DELETE FROM tournament_users WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/archived_tournament.php?id=$tournament_id&error=<strong>The tournament could not be deleted..</strong>");
		exit();
	}

	/**
		Delete tournament players from database
	*/
	$link = connect();
	$sql = "DELETE FROM tournament_players WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/archived_tournament.php?id=$tournament_id&error=<strong>The tournament could not be deleted..</strong>");
		exit();
	}

	$sql = "DELETE matches,match_log_files,match_result
			FROM matches
			JOIN match_log_files ON matches.id = match_log_files.match_id
			JOIN match_result ON matches.id = match_result.match_id
			WHERE matches.tournament_id = ?";

	/**
	* Deletes all matches/results/log_entries from database
	*/
	$link = connect();
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/archived_tournament.php?id=$tournament_id&error=<strong>The tournament could not be deleted..</strong>");
		exit();
	}

	if ($status != 0) {
		$message = "Delete_tournament $tournament_id\n";
		send_to_server($message);
	}

	header("Location: /TE/archived_tournaments.php?success=<strong>The tournament has successfully been deleted!</strong>");

	exit();


	function create_dirs($dir) {
		$dirs = explode("/", $dir);
		$folder = "";
		foreach ($dirs as $key => $val) {
			$folder .= $val . "/";
			if (!file_exists($folder)) {
				mkdir($folder);
			}
		}
	}

?>