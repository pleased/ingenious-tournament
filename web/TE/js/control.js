
$(document).ready(function() {
	updateUpcomingMatches();
	updateOngoingMatches();
	updateJoinedTournaments();
	updateTournamentInfo();
	updateTournamentPlayers();
	updateTournamentInvitations();
	updateMyInvitations();
	updatePlayerInformation();
	updatePlayerStats();
	updateLatestMatches();
	setInterval(function(){updateUpcomingMatches();}, 10000);
	setInterval(function(){updateOngoingMatches();}, 10000);
	setInterval(function(){updateJoinedTournaments();}, 2000);
	setInterval(function(){updateTournamentInfo();}, 2000);
	setInterval(function(){updateTournamentPlayers();}, 2000);
	setInterval(function(){updateTournamentInvitations();}, 2000);
	setInterval(function(){updateMyInvitations();}, 2000);
	setInterval(function(){updatePlayerInformation();}, 2000);
	setInterval(function(){updatePlayerStats();}, 2000);
	setInterval(function(){updateLatestMatches();}, 10000);
});



function updateTournamentPlayers() {
	var $_GET = {};
	if(document.location.toString().indexOf('?') !== -1) {
	    var query = document.location
	                   .toString()
	                   // get the query string
	                   .replace(/^.*?\?/, '')
	                   // and remove any existing hash string (thanks, @vrijdenker)
	                   .replace(/#.*$/, '')
	                   .split('&');
	    for(var i=0, l=query.length; i<l; i++) {
	       var aux = decodeURIComponent(query[i]).split('=');
	       $_GET[aux[0]] = aux[1];
	    }
	}
	var id = $_GET['id'];
	var url = "php/get_tournament_players.php?id="+id;

	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: url,
		cache: false,
		async : true,
		success : function(response) {
			$('#my_players').html(response);
		}
	});
}

function updateTournamentInfo() {
	var $_GET = {};
	if(document.location.toString().indexOf('?') !== -1) {
	    var query = document.location
	                   .toString()
	                   // get the query string
	                   .replace(/^.*?\?/, '')
	                   // and remove any existing hash string (thanks, @vrijdenker)
	                   .replace(/#.*$/, '')
	                   .split('&');
	    for(var i=0, l=query.length; i<l; i++) {
	       var aux = decodeURIComponent(query[i]).split('=');
	       $_GET[aux[0]] = aux[1];
	    }
	}
	var id = $_GET['id'];
	var url = "php/get_tournament_info.php?id="+id;

	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: url,
		cache: false,
		async : true,
		success : function(response) {
			$('#tournament_information').html(response);
		}
	});
}

function updatePlayerInformation() {
	var $_GET = {};
	if(document.location.toString().indexOf('?') !== -1) {
	    var query = document.location
	                   .toString()
	                   // get the query string
	                   .replace(/^.*?\?/, '')
	                   // and remove any existing hash string (thanks, @vrijdenker)
	                   .replace(/#.*$/, '')
	                   .split('&');
	    for(var i=0, l=query.length; i<l; i++) {
	       var aux = decodeURIComponent(query[i]).split('=');
	       $_GET[aux[0]] = aux[1];
	    }
	}
	var id = $_GET['player_id'];
	var url = "php/get_player_information.php?player_id="+id;

	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: url,
		cache: false,
		async : true,
		success : function(response) {
			$('#player_tournaments').html(response);
		}
	});
}

function updatePlayerStats() {
	var $_GET = {};
	if(document.location.toString().indexOf('?') !== -1) {
	    var query = document.location
	                   .toString()
	                   // get the query string
	                   .replace(/^.*?\?/, '')
	                   // and remove any existing hash string (thanks, @vrijdenker)
	                   .replace(/#.*$/, '')
	                   .split('&');
	    for(var i=0, l=query.length; i<l; i++) {
	       var aux = decodeURIComponent(query[i]).split('=');
	       $_GET[aux[0]] = aux[1];
	    }
	}
	var id = $_GET['player_id'];
	var url = "php/get_player_statistics.php?player_id="+id;

	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: url,
		cache: false,
		async : true,
		success : function(response) {
			$('#player_stats').html(response);
		}
	});
}

function updateTournamentInvitations() {
	var $_GET = {};
	if(document.location.toString().indexOf('?') !== -1) {
	    var query = document.location
	                   .toString()
	                   // get the query string
	                   .replace(/^.*?\?/, '')
	                   // and remove any existing hash string (thanks, @vrijdenker)
	                   .replace(/#.*$/, '')
	                   .split('&');
	    for(var i=0, l=query.length; i<l; i++) {
	       var aux = decodeURIComponent(query[i]).split('=');
	       $_GET[aux[0]] = aux[1];
	    }
	}
	var id = $_GET['id'];
	var url = "php/get_tournament_invitations.php?tournament_id="+id;
	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: url,
		cache: false,
		async : true,
		success : function(response) {
			$('#tournament_invitations').html(response);
		}
	});
}

function updateMyInvitations() {
	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: 'php/get_my_invitations.php',
		cache: false,
		async : true,
		success : function(response) {
			$('#my_invitations').html(response);
		}
	});
}

function updateLatestMatches() {
	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: 'php/get_latest_matches.php',
		cache: false,
		async : true,
		success : function(response) {
			$('#latest_results').html(response);
		}
	});
}

function updateJoinedTournaments() {
	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: 'php/get_my_tournaments.php',
		cache: false,
		async : true,
		success : function(response) {
			$('#my_tournaments').html(response);
		}
	});
}

function updateUpcomingMatches() {
	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: 'php/get_upcoming_matches.php',
		cache: false,
		async : true,
		success : function(response) {
			$('#upcoming_matches').html(response);
		}
	});
}

function updateOngoingMatches() {
	$.ajax({
		type: 'POST',
		dataType : 'text',
		url: 'php/get_ongoing_matches.php',
		cache: false,
		async : true,
		success : function(response) {
			$('#ongoing_matches').html(response);
		}
	});
}


