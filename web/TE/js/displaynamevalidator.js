$(document).ready(function() {
    $("#displayname").keyup(function() {
        var displayname = $("#displayname").val();
        var url = "php/check_displayname.php?displayname=" + displayname;
        $.ajax({
            type: 'POST',
            dataType : 'text',
            url: url,
            cache: false,
            async : true,
            success : function(response) {
                $('#displayname_message').html(response);
            }
        });
    });
});