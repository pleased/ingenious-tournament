$(document).ready(function() {
	$('#invite_users').click(function () {

		console.log("CLICKED!");
		if ($('#overlay').css('display') == 'none') {
			$('#overlay').css('display', 'block');
			console.log("BLOCK!");
		} else {
			$('#overlay').css('display', 'none');
			console.log("HIDE!");
		}


	});

	$('#close_invites').click(function () {
		$('#overlay').hide();
	});

	$('#csv').change(function () {

		if ($('#csv').prop('checked') == true) {
			$('.multiple_invite').show();
			$('.single_invite').hide();
		} else {
			$('.multiple_invite').hide();
			$('.single_invite').show();
		}
	});
});
