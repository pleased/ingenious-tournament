<?php
	include "php/connect.php";
	include "php/sessions.php";
	include "php/errors.php";
	include "php/socket.php";

	//Get user id
	$user_id = getUserId($_SESSION['user_email']);

	//Check if user is admin
	$isAdmin = isUserAdmin($user_id, $tournament_id);
	$tournament_name = getTournamentName($tournament_id);


	if ($isAdmin) {
		$folder = "/var/www/html/TE/archives/Tournaments/$tournament_name/";
		$filename = "/var/www/html/TE/archives/Tournaments/$tournament_name.zip";

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
		    new RecursiveDirectoryIterator($folder),
		    RecursiveIteratorIterator::LEAVES_ONLY
		);

		foreach ($files as $name => $file) {
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir()) {
		        // Get real and relative path for current file
		        $filePath = $file;//->getRealPath();
		        $relativePath = substr($filePath, strlen($folder));
		        //echo $filePath . "<br>";
		        //echo $relativePath . "<br>";

		        // Add current file to archive
		        $zip->addFile($filePath, $relativePath);
		    }
		}

		// Zip archive will be created only after closing object
		$zip->close();


		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filename));
		readfile($filename);

	} else {
		/* TODO : What if user is not allowed to download everything */

		$folder = "/var/www/html/TE/archives/Tournaments/$tournament_name/";
		$filename = "/var/www/html/TE/archives/Tournaments/$tournament_name.zip";

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
		    new RecursiveDirectoryIterator($folder),
		    RecursiveIteratorIterator::LEAVES_ONLY
		);

		$player_names = getPlayerNames($user_id, $tournament_id);

		foreach ($files as $name => $file) {
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir()) {
		        // Get real and relative path for current file
		        $filePath = $file->getRealPath();
		        $relativePath = substr($filePath, strlen($folder));

		        foreach ($player_names as $key => $player_name) {
		        	if ((strpos(basename($filePath), $player_name) !== false) || strpos(basename($filePath), "player_scores") !== false) {
		        		// Add current file to archive
		        		$zip->addFile($filePath, $relativePath);
		        	}
		        }
		    }
		}
		// Zip archive will be created only after closing object
		$zip->close();


		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filename));
		readfile($filename);
	}

?>