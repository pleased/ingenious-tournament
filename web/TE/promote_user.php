<?php
	include "php/connect.php";
	include "php/sessions.php";

	if (isset($_POST)) {
		if (isset($_POST['tournament_id'])) {
			$tournament_id = $_POST['tournament_id'];
		} else {
			header('Location: /TE/my_tournaments.php?error=<strong>Could not promote the user because no tournament is selected</strong>');
			exit();
		}

		if (isset($_POST['user_id'])) {
			$user_id = $_POST['user_id'];
		} else {
			header('Location: /TE/my_tournaments.php?error=<strong>Could not promote the user because no user is selected</strong>');
			exit();
		}
	} else {
		header('Location: /TE/my_tournaments.php?error=<strong>Could not promote the user because no user or tournament is selected</strong>');
		exit();
	}

	//Get Current admins
	$link = connect();
	$sql = "SELECT user_id FROM tournament_users WHERE tournament_id = ? AND admin = ?";
	$admin = 1;
	$stmt = $link->prepare($sql);
	$stmt->bind_param("ii", $tournament_id, $admin);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($admin_ids);

	while ($stmt->fetch()) {
		if ($user_id == $admin_ids) {
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The user is already an admin</strong>");
			exit();
		}
	}

	//Check tournament creator
	$creator_id = getTournamentCreator($tournament_id);

	if ($creator_id == $user_id) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The user created the tournament and is already an admin</strong>");
		exit();
	}

	//Check if the user is in the tournament.
	$link = connect();
	$sql = "SELECT * FROM tournament_users WHERE tournament_id = ? AND user_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("ii", $tournament_id, $user_id);
	$stmt->execute();;
	$stmt->store_result();
	$num_rows = $stmt->num_rows;

	if ($num_rows == 0) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The user is not in the tournament.</strong>");
		exit();
	}

	//Check that the user promoting is an admin
	$promoter_id = getUserId($_SESSION['user_email']);
	$link = connect();
	$sql = "SELECT * FROM tournament_users WHERE tournament_id = ? AND user_id = ? AND admin = ?";
	$admin = 1;
	$stmt = $link->prepare($sql);
	$stmt->bind_param("iii", $tournament_id, $promoter_id, $admin);
	$stmt->execute();;
	$stmt->store_result();
	$num_rows = $stmt->num_rows;

	if ($num_rows == 0) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>You do not have admin rights to promote the user</strong>");
		exit();
	}


	$link = connect();
	$sql = "UPDATE tournament_users SET admin= ? WHERE user_id = ? AND tournament_id = ?";
	$admin = 1;
	$stmt = $link->prepare($sql);
	$stmt->bind_param("iii", $admin, $user_id, $tournament_id);
	$res = $stmt->execute();

	$user_name = getDisplayName($user_id);
	if (!$res) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>Something went wrong with promoting the user..</strong>");
		exit();
	} else {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&success=<strong>User \"$user_name\" has successfully been promoted!</strong>");
		exit();
	}


?>