<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tournament Rankings</title>
		<link href="css/default.css" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- jQuery library -->
		<script src="http://code.jquery.com/jquery-3.2.1.min.js"
  				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  				crossorigin="anonymous">
  		</script>
		<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  				integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  				crossorigin="anonymous">
		</script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							   	<li><a href="engines.php">Referees</a></li>
							   	<li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li class="active"><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>

		<?php
			include "php/connect.php";
            include "php/errors.php";

            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }
        ?>

        <?php

        	//Get tournament name
        	$link = connect();
			$sql = "SELECT name FROM tournaments WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $tournament_id);
			$stmt->execute();
			$stmt->bind_result($tournament_name);
			$stmt->fetch();

			$str = "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">
					<h2>$tournament_name Tournament Rankings</h2>
				</div>
				<div class=\"col-md-3\"></div>
			</div>";

			echo $str;

			//Get all players in tournament
			$link = connect();
			$sql = "SELECT user_id, player_id, rating FROM tournament_players WHERE tournament_id = ? ORDER BY rating DESC";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $tournament_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			$stmt->bind_result($user_idd, $player_id, $player_rating);

			if ($num_rows == 0) {
				$str = "<div class=\"row\">
						<div class=\"col-md-3\"></div>
						<div class=\"col-md-6\">
						<b>This tournament has no players yet.<br></b>
						</div>
						<div class=\"col-md-3\"></div>";
				echo $str;
			} else {
				$str = "<div class=\"row\">
						<div class=\"col-md-3\"></div>
						<div class=\"col-md-6\">
							<table class=\"tournament_table\">
								<tr>
									<th>Rank</th>
									<th>User</th>
									<th>Player Name</th>
									<th class=\"center_cell\">Points</th>
								</tr>";
				$i = 1;
				$user_id = getuserId($_SESSION['user_email']);
				while ($stmt->fetch()) {
					if ($user_id == $user_idd) {
						$str .= "<tr style=\"font-weight:bold\" bgcolor=\"lightgrey\">";
					} else {
						$str .= "<tr>";
					}
					$str .= "<td>$i</td>";
					$link = connect();

					//Get name of user
					$user_name = getDisplayName($user_idd);
					$player_name = getPlayerName($player_id);
					$player_rating = $player_rating;
					$i = $i + 1;
					$str .= "<td>$user_name</td>
							<td>$player_name</td>
							<td class=\"center_cell\">$player_rating</td></tr>";

				}
				$str .= "</table>
					</div>
					<div class=\"col-md-3\"></div>
				</div>";
				echo $str;
			}

        ?>

		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde.</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>
