<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Archived Tournaments</title>
		<link href="css/default.css" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- jQuery library -->
		<script src="http://code.jquery.com/jquery-3.2.1.min.js"
  				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  				crossorigin="anonymous">
  		</script>
		<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  				integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  				crossorigin="anonymous">
		</script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script src="js/pagination.js"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li ><a href="dashboard.php">Home <span class="sr-only">(	current)</span></a></li>
								<li class="dropdown active">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							           <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							    <li><a href="engines.php">Referees</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>

		<?php
			include "php/connect.php";
            include "php/errors.php";

            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }
            $str = "<div class=\"row\">
					<div class=\"col-md-2\"></div>
					<div class=\"col-md-8\">
						<h2>Archived Tournaments</h2>
					</div>
					<div class=\"col-md-2\"></div>
				</div>";
            $user_id = getUserId($_SESSION['user_email']);

            $link = connect();
            $sql = "SELECT tournament_id, admin FROM tournament_users WHERE user_id = (?) AND archived = 1";
            $stmt = $link->prepare($sql);
            $stmt->bind_param("i", $user_id);
            $stmt->execute();
            $stmt->store_result();
			$num_rows = $stmt->num_rows;
            $stmt->bind_result($tournament_id, $admin);


            if (!$stmt) {
				die("Joined tournaments cannot be displayed.");
			}

			if ($num_rows == 0) {
				$str .= "<div class=\"row\">
					<div class=\"col-md-2\"></div>
					<div class=\"col-md-8\">
						<b>No Archived Tournaments</b>
					</div>
					<div class=\"col-md-2\"></div>
				</div>";
				echo $str;
			} else {
				echo $str;
				$str = "<div class=\"row\">
				<div class=\"col-md-2\"></div>
				<div class=\"col-md-8\">
					<table class=\"tournament_table\">
						<tr>
							<th class=\"center_cell\">Tournament Name</th>
							<th class=\"center_cell\">Referee</th>
							<th class=\"center_cell\">Creator</th>
							<th class=\"center_cell\">Status</th>
							<th class=\"center_cell\">Type</th>
						</tr>";


				while ($stmt->fetch()) {
					$link1 = connect();
					$sql = "SELECT id, name, engine_id, user_id, status, private FROM tournaments WHERE id = (?)";
					$stmt1 = $link1->prepare($sql);
					$stmt1->bind_param("i", $tournament_id);
					$stmt1->execute();
					$stmt1->store_result();
					$num_rows = $stmt1->num_rows;
					$stmt1->bind_result($id, $name, $engine_id, $user_id, $status, $private);

					if (!$stmt1) {
						die("Archived tournaments cannot be displayed because you have not joined any tournaments.");
					}

					if ($num_rows == 0) {
						/** TODO : no joined tournaments **/
					}

					while ($stmt1->fetch()) {
						$str .= "<tr>";
						$str .= "<td class=\"center_cell\"> $name</td>";
						$link2 = connect();
						$sql = "SELECT name FROM engines WHERE id = ?";
						$stmt2 = $link2->prepare($sql);
						$stmt2->bind_param("i", $engine_id);
						$stmt2->execute();
						$stmt2->bind_result($engine_name);
						$stmt2->fetch();

						$str .= "<td class=\"center_cell\">". $engine_name ."</td>";


						$user_name = getUserName($user_id);


						$str .= "<td class=\"center_cell\">". $user_name."</td>";

						if ($status == 0) {
							$stat = "<p style=\"color:red\">Not Started";
						} else if ($status == 1) {
							$stat = "<p style=\"color:green\">Running";
						} else if($status == 2) {
							$stat = "<p style=\"color:orange\">Paused";
						} else if ($status == 3) {
							$stat = "<p style=\"color:green\">Completed!";
						} else {
							$stat = "ERROR";
						}

						if ($private == 0) {
							$private = " <p style=\"color:green\">Public";
						} else {
							$private = " <p style=\"color:orange\">Private";
						}

						if ($admin) {
							$buttons = "<td><a class=\"btn btn-primary btn-full\" href=\"tournament_ranking.php?id=".$tournament_id."\">Rankings</a></td><td><a class=\"btn btn-primary btn-full\" href=\"get_archived_tournament.php?id=". $tournament_id ."\">Request Archive</a></td><td><a class=\"btn btn-danger btn-full\" href=\"delete_tournament.php?id=". $tournament_id ."\">Delete Archive</a></td>";
						} else {
							$buttons = "<td><a class=\"btn btn-primary btn-full\" href=\"tournament_ranking.php?id=".$tournament_id."\">Rankings</a></td><td><a class=\"btn btn-primary btn-full\" href=\"get_archived_tournament.php?id=". $tournament_id ."\">Request Archive</a></td><td><a class=\"btn btn-danger btn-full no-click\" href=\"delete_tournament.php?id=". $tournament_id ."\" disabled>Delete Archive</a></td>";
						}


						$str .= "<td class=\"center_cell\">$stat</td><td class=\"center_cell\">$private</td>$buttons</tr>";
					}

				}

				$str .= "</table>
					</div>
					<div class=\"col-md-2\"></div>
					</div>";
					echo $str;

			}
        ?>




		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
	<script src="js/control.js"></script>
</html>
