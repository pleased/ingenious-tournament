<?php

	include("php/connect.php");

	// Get Tournament1 id
	$link = connect();
	$sql = "SELECT id FROM tournaments WHERE name = ?";
	$name = "Tournament1";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("s", $name);
	$res = $stmt->execute();
	$stmt->bind_result($tournament_id);
	$stmt->fetch();

	if (!$res) {
		die("No tournament1 exists...");
	}
	close($link);

	// Delete matches generated
	$link = connect();
	$sql = "DELETE FROM players WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	close($link);

	header("Location: http://localhost/TE/secret.php");
	exit();


?>