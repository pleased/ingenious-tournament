<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>My Game Referees</title>
		<link href="css/default.css" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- jQuery library -->
		<script src="http://code.jquery.com/jquery-3.2.1.min.js"
  				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  				crossorigin="anonymous">
  		</script>
		<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  				integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  				crossorigin="anonymous">
		</script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							    <li class="active"><a href="engines.php">Engines</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
		<?php
			include "php/connect.php";
            include "php/errors.php";

            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }
        ?>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h2>My Referees</h2>
			</div>
			<div class="col-md-3"></div>
		</div>
		<?php

			$user_id = getUserId($_SESSION['user_email']);

			$link = connect();
			$sql = "SELECT id, name, user_id FROM engines WHERE user_id = ? ";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $user_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows();
			$stmt->bind_result($engine_id, $engine_name, $engine_user);


			if ($num_rows == 0) {
				$str = "<div class=\"row\">
						<div class=\"col-md-3\"></div>
						<div class=\"col-md-6\">
							<strong>You currently have no referees, add a referee using the button below.</strong>
						</div>
						<div class=\"col-md-3\"></div>
					</div>";
				echo $str;
			} else {
				$str = "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">
					<table class=\"tournament_table\">
						<tr>
							<th>Referee Name</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>";

				while ($stmt->fetch()) {
					$str .= "<tr><td>". $engine_name . "</td>";

					$str .= "<td><a class=\"btn btn-warning btn-full\" href=\"edit_engine.php?id=$engine_id\">Edit</a></td>";

					$form1 = "<form method=\"POST\" action=\"/index.php/delete_engine\">
						<input type=\"hidden\" value=$engine_id name=\"engine_id\">
						<input type=\"hidden\" value=$user_id name=\"user_id\">
						<input type=\"submit\" class=\"btn btn-danger btn-full\" value=\"Delete Referee\">
						</form>";
					$str .= "<td>$form1</td>
						</tr>";
				}
				$str .= "</table>";
				echo $str;
			}

			echo "<br><br><a class=\"btn btn-primary btn-overwrite\" href=\"add_engine.php\">Add Referee</a>
				</div>
				<div class=\"col-md-3\"></div>
			</div>";


		?>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h2>Public Referees</h2>
			</div>
			<div class="col-md-3"></div>
		</div>
		<?php

			$user_id = getUserId($_SESSION['user_email']);

			$link = connect();
			$sql = "SELECT id, name, user_id FROM engines WHERE public = ? ";
			$stmt = $link->prepare($sql);
			$public = 1;
			$stmt->bind_param("i", $public);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows();
			$stmt->bind_result($engine_id, $engine_name, $engine_user);


			if ($num_rows == 0) {
				$str = "<div class=\"row\">
						<div class=\"col-md-3\"></div>
						<div class=\"col-md-6\">
							<strong>There are currently no public referees.</strong>
						</div>
						<div class=\"col-md-3\"></div>
					</div>";
				echo $str;
			} else {
				$str = "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">
					<table class=\"tournament_table\">
						<tr>
							<th class=\"center_cell\">#</th>
							<th class=\"center_cell\">Referee Name</th>
						</tr>";
				$i = 1;
				while ($stmt->fetch()) {
					$str .= "<tr><td class=\"center_cell\">$i</td>";
					$str .= "<td class=\"center_cell\"> ". $engine_name . "</td></tr>";
					$i++;
				}
				$str .= "</table>";
				echo $str;
			}

			echo "</div>
				<div class=\"col-md-3\"></div>
			</div>";


		?>

		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde.</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>
