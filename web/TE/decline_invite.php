<?php
	include "php/connect.php";
	include "php/sessions.php";

	if (isset($_GET)) {
		if (isset($_GET['invite_id'])) {
			$invite_id = $_GET['invite_id'];
		} else {
			header('Location: /TE/my_invitations.php?error=<strong>Could not accept the invite, since no invite has been selected.</strong>');
		}

	} else {
		header('Location: /TE/my_tournaments.php?error=<strong>Could not accept the invite since no invite was selected.</strong>');
	}

	$user_id = getUserId($_SESSION['user_email']);

	$link = connect();
	$sql = "SELECT tournament_id, status FROM tournament_invitations WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $invite_id);
	$stmt->execute();
	$stmt->bind_result($tournament_id, $status);
	$stmt->fetch();

	//Check that the user isnt already in the tournament.
	if (isUserInTournament($user_id, $tournament_id)) {
		header("Location: /TE/my_invitations.php?error=<strong>Your invitation was declined, but you are already in the tournament. Should you wish to leave do so from the my_tournament page.</strong>");
	}

	//Check status of invitation.
	if ($status != 0) {
		header("Location: /TE/my_invitations.php?error=<strong>Your invitation has already been accepted/declined</strong>");
	}

	//Maybe more check ?

	$tournament_name = getTournamentName($tournament_id);

	$link = connect();
	$sql = "UPDATE tournament_invitations SET status= ? WHERE id = ?";
	$status = 2;
	$stmt = $link->prepare($sql);
	$stmt->bind_param("ii", $status, $invite_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/my_invitations.php?id=$tournament_id&error=<strong>Your invitation could not be declined. Please contact an administrator.</strong>");
	} else {

		$success = "<strong>You successfully declined the invitation.</strong>";
		header('Location: '.'/TE/my_invitations.php?success='.$success);

	}


?>