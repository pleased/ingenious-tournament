<?php

	include("php/connect.php");

	// Delete matches generated
	$link = connect();
	$sql = "DELETE FROM matches WHERE 1";
	$stmt = $link->prepare($sql);
	$res = $stmt->execute();

	if (!$res) {
		die("Could not delete matches...");
	}
	close($link);
	//Delete all match results
	$sql = "DELETE FROM match_result WHERE 1";
	$link = connect();
	$stmt = $link->prepare($sql);
	$res = $stmt->execute();

	$sql = "DELETE FROM match_log_files WHERE 1";
	$link = connect();
	$stmt = $link->prepare($sql);
	$res = $stmt->execute();

	if (!$res) {
		die("Could not delete match results");
	}

	close($link);
	//Set all tournaments not starteds
	$sql = "UPDATE tournaments SET status = ? WHERE 1";
	$link = connect();
	$stmt = $link->prepare($sql);
	$status = 0;
	$stmt->bind_param("i", $status);
	$res = $stmt->execute();

	if (!$res) {
		die("Could not set tournament to \"Not running\"");
	}

	close($link);

	$sql = "UPDATE tournament_players SET scheduled_count = ? , active_count = ? WHERE 1";
	$link = connect();
	$stmt = $link->prepare($sql);
	$status = 0;
	$active = 0;
	$scheduled = 0;
	$stmt->bind_param("ii", $active, $scheduled);
	$res = $stmt->execute();

	header("Location: http://localhost/TE/secret.php");
	exit();

?>