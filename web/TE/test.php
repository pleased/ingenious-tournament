<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Manage Tournament XX</title>
		<link href="css/default.css" rel="stylesheet">

		<!-- Latest compiled and minified CSS -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- jQuery library -->
		<script src="http://code.jquery.com/jquery-3.2.1.min.js"
  				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  				crossorigin="anonymous">
  		</script>
		<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  				integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  				crossorigin="anonymous">
		</script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script src="js/pagination.js"></script>

	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li ><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown active">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							        </ul>
							      </li>
							    <li><a href="engines.php">Engines</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>

<?php
	include "php/connect.php";
    include "php/errors.php";
	$user_id = 0;
	$str = "<div class=\"row\">
	<div class=\"col-md-2\"></div>
	<div class=\"col-md-8\">
		<h2>Latest Match Results</h2>
	</div>
	<div class=\"col-md-2\"></div>
</div>";

$user_id = 5;
$curr_t_id = 0;

$link = connect();
$sql = "SELECT id,tournament_id, player_ids FROM matches WHERE result_id = ? AND user_ids LIKE ? ORDER BY tournament_id";
$stmt = $link->prepare($sql);
$res = -1;
$user_like = "%$user_id%";
$stmt->bind_param("is", $res, $user_like);
$stmt->execute();
$stmt->store_result();
$num_rows = $stmt->num_rows;
$stmt->bind_result($match_id, $tournament_id, $player_ids);


$str = "<div class=\"row\">
		<div class=\"col-md-2\"></div>
		<div class=\"col-md-8\">
			<table class=\"tournament_table\">
				<tr>
					<th class=\"center_cell\">Tournament Name</th>
				</tr>";


$first = 1;
$tournament_pos = 1;
$table_names = Array();
$table_counter = 0;

while ($stmt->fetch()) {
	if ($tournament_id == $curr_t_id) {
		$playerids = explode(",", $player_ids);
		$players = "<br>" . getPlayerName($playerids[0]) . "<br>";
		for ($i = 1; $i < count($playerids); $i++) {
				$player_name = getPlayerName($playerids[$i]);
				$players .= "<b class=\"center\">vs</b><br> " .  $player_name . "<br>";
			}
		$players .= "<br>";
		$str .= "<tr>
					<td class=\"center_cell\">$tournament_pos</td>
					<td class=\"center_cell\">$players</td>
					<td class=\"center_cell\">
					<a href=\"match.php?id=$match_id\" class=\"btn btn-full btn-primary\">View Match</a>
								</td>
							</tr>";
	} else {
		$curr_t_id = $tournament_id;
		$tournament_pos = 1;

		if ($first) {
			$first = 0;
		} else {
			$str .= "</tbody><div class=\"col-md-12 text-center\">
      				<ul class=\"pagination pagination-lg pager\" id=\"myPager_$tournament_name\"></ul>
      					</div></table></td></tr>";
		}

		echo $str;
		$tournament_name = getTournamentName($tournament_id);
		array_push($table_names, "$tournament_name");
		$str = "<tr>
					<td class=\"center_cell\">
						$tournament_name
					</td>
					<td class=\"tournament_matches\">
					<div class=\"table-responsive\">
					<table id=\"tournament_match_table\" class=\"table-bordered tournament_table_inside\" >
						<thead>
						<tr>
							<th class=\"center_cell\">#</th>
							<th class=\"center_cell\">Players</th>
							<th class=\"center_cell\">View Match</th>
						</tr>
						</thead><tbody id=\"myTable_$tournament_name\">";

		$playerids = explode(",", $player_ids);
		$players = "<br>" . getPlayerName($playerids[0]) . "<br>";
		for ($i = 1; $i < count($playerids); $i++) {
				$player_name = getPlayerName($playerids[$i]);
				$players .= "<b class=\"center\">vs</b><br> " .  $player_name . "<br>";
			}
		$players .= "<br>";
		$str .= "<tr>
					<td class=\"center_cell\">$tournament_pos</td>
					<td class=\"center_cell\">";

		$str .= "$players</td>
					<td class=\"center_cell\">
					<a href=\"match.php?id=$match_id\" class=\"btn btn-full btn-primary\">View Match</a>
								</td>
							</tr>";
	}
	$tournament_pos++;
}

$str .= "</tbody><div class=\"col-md-12 text-center\">
      				<ul class=\"pagination pagination-lg pager\" id=\"myPager_$tournament_name\"></ul>
      					</div></table></div></td></tr></table></div><div class=\"col-md-2\"></div>";
echo $str;

$str = "<script>
			$(document).ready(function(){
				";


		for ($i=0; $i < sizeof($table_names); $i++) {
				$str .= "$('#myTable_$table_names[$i]').pageMe({pagerSelector:'#myPager_$table_names[$i]',showPrevNext:true,hidePageNumbers:false,perPage:3});
				";
		}
		$str .= "});
		</script>";
		echo $str;
?>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">All Rights Reserved.</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>