<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Create Tournament</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/default.css" rel="stylesheet">
		<link rel="stylesheet" href="css/w3.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li ><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown active">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							    <li><a href="engines.php">Referees</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>

		<?php
			include "php/connect.php";
            include "php/errors.php";

            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }
        ?>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 login-wrapper">
				<b id="sign-up-header">Create Tournament</b>
				<form action="/index.php/create_tournament" method="post" enctype="multipart/form-data">
					<br>
					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Tournament Name:</div><div class="col-md-6"> <input class="form-control" type="name" name="name"></div>
					</div>

					<?php
						//Get user id
						$user_id = getUserId($_SESSION['user_email']);
						echo "<input class=\"form-control\" name=\"user_id\" type=\"hidden\" value=\"". $user_id ."\">";

					?>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Referee:</div><div class="col-md-6">
						<select name="engine" class="form-control">

								<?php
									$sql = "SELECT id,name FROM engines WHERE public = 1 OR user_id = ?";
									$link = connect();
									$stmt = $link->prepare($sql);
									$stmt->bind_param("i", $user_id);
									$stmt->execute();
									$stmt->store_result();
									$num_rows = $stmt->num_rows;
									$stmt->bind_result($engine_id, $engine_name);
									if ($num_rows == 0) {
										$error = "<strong>Please add a referee before you can create a tournament.</strong>";
										header('Location: /TE/engines.php?error='.$error);
										exit;
									}

									while ($stmt->fetch()) {
										echo "<option data-value=\"$engine_id\">$engine_name</option>";
									}

								?>
							</select>
							</div>
					</div>


					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Configuration file *.json:</div><div class="col-md-6"> <input class="form-control" type="file" name="config_file"></div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Ranker:</div><div class="col-md-6">
						  <select name="ranker" class="form-control">
								<?php

									$sql = "SELECT id,name FROM rankers WHERE user_id = ? OR user_id = -1";
									$link = connect();
									$stmt = $link->prepare($sql);
									$stmt->bind_param("i", $user_id);
									$stmt->execute();
									$stmt->store_result();
									$num_rows = $stmt->num_rows;
									$stmt->bind_result($ranker_id, $ranker_name);
									if ($num_rows == 0) {
										$error = "<strong>Please add a ranker before you can create a tournament.</strong>";
										header('Location: /TE/rankers.php?error='.$error);
										exit;
									}

									while ($stmt->fetch()) {
										echo "<option data-value=\"$ranker_id\">$ranker_name</option>";
									}

								?>
							</select>
							</div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Scheduler:</div><div class="col-md-6">
						<select name="scheduler" class="form-control">
								<?php

									$sql = "SELECT id, name FROM schedulers WHERE user_id = ? OR user_id = -1";
									$link = connect();
									$stmt = $link->prepare($sql);
									$stmt->bind_param("i", $user_id);
									$stmt->execute();
									$stmt->store_result();
									$num_rows = $stmt->num_rows;
									$stmt->bind_result($scheduler_id, $scheduler_name);
									if ($num_rows == 0) {
										$error = "<strong>Please add a scheduler before you can create a tournament.</strong>";
										header('Location: /TE/schedulers.php?error='.$error);
										exit;
									}

									while ($stmt->fetch()) {
										echo "<option data-value=\"$scheduler_id\">$scheduler_name</option>";
									}

								?>
							</select> </div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Compute Resources:</div><div class="col-md-6">
						<input id="resource_limit" type="checkbox" name="limit_resources">
						</div>
					</div>

					<div id="memory_limit" class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Memory:</div><div class="col-md-3">
						<input class="form-control" type="number" name="limit_memory" min="0">
						</div><div class="col-md-3">
						<select class="form-control" name="affix">
							<option value="b">Bytes</option>
							<option value="k">KiloBytes</option>
							<option value="m">MegaBytes</option>
							<option value="g">GigaBytes</option>
						</select>
					</div>
					</div>

					<div id="cpu_limit" class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">CPU:</div><div class="col-md-6">
						<input class="form-control" type="number" name="limit_cpu" min="0" max="4" step="0.1">
						</div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Max Users:</div><div class="col-md-6"> <input class="form-control" type="number" name="max_users" min="2" max="100"></div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Max players per user:</div><div class="col-md-6"> <input class="form-control" type="number" name="max_players" min="1" max="100"></div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Private Tournament:</div><div class="col-md-6"> <input class="form-control" type="checkbox" name="private"></div>
					</div>

					<br>

  					<div class="row">
  					<div class="col-md-5"></div>
  					<div class="col-md-4">
  						<input type="submit" class="btn btn-primary button-overwrite" value="Create Tournament">
  					</div>
  					<br><br>
  					<hr class="seperator">
  					<br><br>
				</form>
				<br>
			</div>
		</div>
		<script>
			$( document ).ready(function() {

			  // Get the form fields and hidden div
			  var checkbox = $("#resource_limit");
			  var cpu = $("#cpu_limit");
			  var mem = $("#memory_limit");

			  cpu.hide();
			  mem.hide();

			  checkbox.change(function() {
			    if (checkbox.is(':checked')) {
			      // Show the hidden fields.
			      cpu.show();
			      mem.show();
			    } else {
			      // Make sure that the hidden fields are indeed
			      // hidden.
			      cpu.hide();
			      mem.hide();
			    }
			  });
			});
		</script>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>
