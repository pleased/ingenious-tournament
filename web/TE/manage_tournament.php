<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Manage Tournament XX</title>
		<link href="css/default.css" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- jQuery library -->
		<script src="http://code.jquery.com/jquery-3.2.1.min.js"
  				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  				crossorigin="anonymous">
  		</script>
		<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  				integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  				crossorigin="anonymous">
		</script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="js/invite_users.js"></script>
		<script src="js/control.js"></script>

	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li ><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown active">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							    <li><a href="engines.php">Referees</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
		<div id="overlay">

		<?php
			include "php/connect.php";
            include "php/errors.php";


            //Get user id
			$user_id = getUserId($_SESSION['user_email']);

			//Check if user is admin
			$isAdmin = isUserAdmin($user_id, $tournament_id);



			echo "<div class=\"invite_users_popup\"><div class=\"row\" id=\"invite_users_div\">
					    <div class=\"col-md-4\"></div>
					    <div class=\"col-md-4 login-wrapper\">
					    	<a class=\"btn btn-danger\" id=\"close_invites\">Close</a>
					    	<form method=\"post\" action=\"invite_users.php\" class=\"invite_users_form form-group\" enctype=\"multipart/form-data\">
					        <div class=\"row\">
					        	<div class=\"col-md-2\"></div>
					        	<div class=\"col-md-8\">
					        		<h3>Invite Users</h3>
					        	</div>
					        	<div class=\"col-md-2\"></div>
					        </div>
					        <br>
					        <div class=\"row\">
					        	<div class=\"col-md-7\">
					          		<label for=\"csv\" class=\"form-label\">Multiple invite</label>
					          	</div>
					          	<div class=\"col-md-5\">
					          	<input type=\"checkbox\" name=\"single_or_multiple\" id=\"csv\" value=\"1\">
					          	</div>

					        </div>
					        <br>
					        <div class=\"row single_invite\">
					        	<div class=\"col-md-7\">
					        		<label for=\"email\" class=\"form-label\">Single Email address</label>
					        	</div>
					        	<div class=\"col-md-5\">
					        		<input type=\"email\" name=\"single_email\" id=\"sngl_email\" placeholder=\"example@server.com\">
					        	</div>
					        </div>
					        <br>
					        <div class=\"row multiple_invite\">
					        <div class=\"col-md-7\">
					        		<label for=\"email_csv\">Select CSV file</label>
					        	</div>
					        	<div class=\"col-md-5\">
					        		<input class=\"form-control\" type=\"file\" name=\"email_csv\" id=\"email_csv\" enctype=\"multipart/form-data\"/>
					        	</div>
					        </div>
					        <br>
					        <div class=\"row\">
					        <input type=\"hidden\" name=\"tournament_id\" value=\"$tournament_id\">
					        <input type=\"hidden\" name=\"user_id\" value=\"$user_id\">
					          	<input type=\"submit\" class=\"btn btn-primary\" value=\"Invite Users\">
					        </div>
					      </form>
					    </div>
					    <div class=\"col-md-4\"></div>
					    </div></div>";

		?>
		</div>
		<div class="content">
		<?php

			if (!$isAdmin) {
				header("Location: /TE/my_tournaments.php?error=<strong>You do not have sufficient rights to view manage tournament details.</strong>");
				exit();
			}
			if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }

            $link = connect();
            $sql = "SELECT name, ranker_id, max_users, max_players, private, resources_id FROM tournaments WHERE id = ?";
            $stmt = $link->prepare($sql);
            $stmt->bind_param("i", $tournament_id);
            $stmt->execute();
            $stmt->bind_result($tournament_name, $ranker_id, $max_users, $max_players, $private, $resources_id);
            $stmt->fetch();

            $link = connect();
            $sql = "SELECT name FROM rankers WHERE id = ?";
            $stmt = $link->prepare($sql);
            $stmt->bind_param("i", $ranker_id);
            $stmt->execute();
            $stmt->bind_result($ranker_name);
            $stmt->fetch();

            if ($resources_id == -1) {
            	$mem = 0;
            	$cpu = 0;
            	$affix = 'b';
            } else {
            	$link = connect();
	            $sql = "SELECT memory, cpu FROM resources WHERE id = ?";
	            $stmt = $link->prepare($sql);
	            $stmt->bind_param("i", $resources_id);
	            $stmt->execute();
	            $stmt->bind_result($mem, $cpu);
	            $stmt->fetch();


	            $affix = substr($mem, -1);
	            $mem = substr($mem, 0, -1);

            }


		?>
			<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 login-wrapper">
				<b id="sign-up-header">Update Tournament</b>
				<form action="/index.php/update_tournament" method="post">
					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Tournament Name:</div><div class="col-md-6"> <input class="form-control" type="name" name="name" <?php echo "value=\"$tournament_name\""; ?>></div>
					</div>


					<?php
						$user_id = getUserId($_SESSION['user_email']);
						echo "<input class=\"form-control\" name=\"user_id\" type=\"hidden\" value=\"". $user_id ."\">";
						echo "<input class=\"form-control\" name=\"tournament_id\" type=\"hidden\" value=\"". $tournament_id ."\">";
					?>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Ranker:</div><div class="col-md-6">
							<select name="ranker" class="form-control" id="rankers">
								<?php

									$sql = "SELECT name, id FROM rankers WHERE user_id = ? OR user_id = -1";
									$link = connect();
									$stmt = $link->prepare($sql);
									$stmt->bind_param("i", $user_id);
									$stmt->execute();
									$stmt->store_result();
									$num_rows = $stmt->num_rows;
									$stmt->bind_result($ranker_names, $ranker_ids);
									if ($num_rows == 0) {
										$error = "No rankers available";
										header('Location: /TE/create_tournament.php?error='.$error);
									}
									echo "<option data-value=\"$ranker_id\" selected=\"selected\">$ranker_name</option>";
									while ($stmt->fetch()) {
										if ($ranker_name == $ranker_names) {
										} else {
											echo "<option data-value=\"$ranker_ids\">$ranker_names</option>";
										}

									}

								?>
							</select> </div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Compute Resources:</div><div class="col-md-6">
						<input id="resource_limit" type="checkbox" name="limit_resources" <?php if ($resources_id == -1) { } else { echo "checked"; } ?>>
						</div>
					</div>

					<div id="memory_limit" class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Memory:</div><div class="col-md-3">
						<input class="form-control" type="number" name="limit_memory" min="0" <?php echo "value=\"$mem\"" ?>>
						</div><div class="col-md-3">
						<select class="form-control" name="affix">
							<?php
							if ($affix == 'b') {
								echo "<option value=\"b\" selected=\"selected\">Bytes</option>";
								echo "<option value=\"k\">KiloBytes</option>";
								echo "<option value=\"m\">MegaBytes</option>";
								echo "<option value=\"g\">GigaBytes</option>";
							} else if ($affix == 'k') {
								echo "<option value=\"b\">Bytes</option>";
								echo "<option value=\"k\" selected=\"selected\">KiloBytes</option>";
								echo "<option value=\"m\">MegaBytes</option>";
								echo "<option value=\"g\">GigaBytes</option>";
							} else if ($affix == 'm') {
								echo "<option value=\"b\">Bytes</option>";
								echo "<option value=\"k\">KiloBytes</option>";
								echo "<option value=\"m\" selected=\"selected\">MegaBytes</option>";
								echo "<option value=\"g\">GigaBytes</option>";
							} else if ($affix == 'g') {
								echo "<option value=\"b\">Bytes</option>";
								echo "<option value=\"k\">KiloBytes</option>";
								echo "<option value=\"m\">MegaBytes</option>";
								echo "<option value=\"g\" selected=\"selected\">GigaBytes</option>";
							}
							?>
						</select>
					</div>
					</div>

					<div id="cpu_limit" class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">CPU:</div><div class="col-md-6">
						<input class="form-control" type="number" name="limit_cpu" min="0" max="1" step="0.1"  <?php echo "value=\"$cpu\"" ?>>
						</div>
					</div>



					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Max Users:</div><div class="col-md-6"> <input class="form-control" type="number" name="max_users" min="1" max="100"
						<?php
							echo "value=\"$max_users\"";
							?>
						/></div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Max players per user:</div><div class="col-md-6"> <input class="form-control" type="number" name="max_players" min="1" max="100" <?php echo "value=\"$max_players\"";?>/></div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Private Tournament:</div><div class="col-md-6"> <input class="form-control" type="checkbox" name="private" <?php if ($private == 1) {echo "checked";}?>/></div>
					</div>

					<br>

  					<div class="row">
  					<div class="col-md-5"></div>
  					<div class="col-md-4">
  						<input type="submit" class="btn btn-primary button-overwrite" value="Update Tournament">
  					</div>
  					</div>
  					<br><br>
  					</form>
  					<hr class="seperator">
  					<b id="sign-up-header">Change Configuration File</b>
  					<br><br>
  					<form action="/index.php/update_tournament_config" method="post" enctype="multipart/form-data">
  						<?php
  							echo "<input class=\"form-control\" name=\"user_id\" type=\"hidden\" value=\"". $user_id ."\">";
							echo "<input class=\"form-control\" name=\"tournament_id\" type=\"hidden\" value=\"". $tournament_id ."\">";
  						?>
  						<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Configuration file *.json:</div><div class="col-md-6"> <input class="form-control" type="file" name="config_file"></div>
					</div>
					<br><br>
					<div class="row">
  					<div class="col-md-5"></div>
  					<div class="col-md-4">
  						<input type="submit" class="btn btn-primary button-overwrite" value="Update Configuration File">
  					</div>
  					</div>
  					</form>
  					<br><br>
				<br>
			</div>
		</div>
				</div>
				<br><br>

			<?php
				$str = "<div class=\"row\">
					<div class=\"col-md-3\"></div>
					<div class=\"col-md-6\">
						<h2>Current Users</h2>
					</div>
					<div class=\"col-md-3\"></div>
					</div>";

				$link = connect();
				$sql = "SELECT user_id, admin FROM tournament_users WHERE tournament_id = ?";
				$stmt = $link->prepare($sql);
				$stmt->bind_param("i", $tournament_id);
				$stmt->execute();
				$stmt->store_result();
				$num_rows = $stmt->num_rows();
				$stmt->bind_result($user_id, $admin);

				$str .= "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\"><table class=\"tournament_table\">
						<tr>
							<th class=\"center-cell text-center\">User</th>
							<th class=\"center-cell text-center\">Admin</th>
						</tr>";
				while ($stmt->fetch()) {
					$user_name = getDisplayName($user_id);
					if ($admin == 1) {
						$admin = "<strong class=\"green\">Yes</strong>";
						$form = "<form class=\"promote\" method=\"POST\" action=\"demote_user.php\">
								<input type=\"hidden\" value=$tournament_id name=\"tournament_id\">
								<input type=\"hidden\" value=$user_id name=\"user_id\">
								<input type=\"submit\" class=\"btn btn-warning btn-full\" value=\"Demote\">
								</form>";

					} else {
						$admin = "<strong class=\"red\">No</strong>";
						$form = "<form method=\"POST\" action=\"promote_user.php\">
								<input type=\"hidden\" value=$tournament_id name=\"tournament_id\">
								<input type=\"hidden\" value=$user_id name=\"user_id\">
								<input type=\"submit\" class=\"btn btn-primary btn-full\" value=\"Promote\">
								</form>";
					}
					$admin_id = getUserId($_SESSION['user_email']);
					$form1 = "<form method=\"POST\" action=\"/index.php/delete_user\">
							<input type=\"hidden\" value=$tournament_id name=\"tournament_id\">
							<input type=\"hidden\" value=$admin_id name=\"admin_id\">
							<input type=\"hidden\" value=$user_id name=\"user_id\">
							<input type=\"submit\" class=\"btn btn-danger btn-full\" value=\"Kick User\">
							</form>";
					$str .= "<tr><td class=\"center-cell text-center\">$user_name</td><td class=\"center-cell text-center\">$admin</td><td class=\"center-cell text-center\">$form</td><td>$form1</td></tr>";
				}
				$form = "<div class=\"\">
					    <a id=\"invite_users\" class=\"btn-primary btn\">Invite Users</a>
					  </div>";

				$str .= "</table>
						<br>$form</div><div class=\"col-md-3\"></div>
						</div><br><br>";
				echo $str;
			?>
			<div id="tournament_invitations"></div>
			<?php
				$str = "<div class=\"row\">
					<div class=\"col-md-3\"></div>
					<div class=\"col-md-6\">
						<h2>Current Players</h2>
					</div>
					<div class=\"col-md-3\"></div>
					</div>";
				$link = connect();
				$sql = "SELECT user_id, player_id, active_count, scheduled_count FROM tournament_players WHERE tournament_id = ? AND status <> ?";
				$stmt = $link->prepare($sql);
				$status = 4;
				$stmt->bind_param("ii", $tournament_id, $status);
				$stmt->execute();
				$stmt->store_result();
				$num_rows = $stmt->num_rows();
				$stmt->bind_result($user_idd, $player_id, $active_count, $scheduled_count);
				$str .= "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">";

				if ($num_rows == 0) {
					$str .= "<b>No players in the tournament!<br></b>";
				} else {
					$str .= "<table class=\"tournament_table\">
						<tr>
							<th>User</th>
							<th>Player Name</th>
							<th>Active Matches</th>
							<th>Scheduled Matches</th>
						</tr>";
					while ($stmt->fetch()) {
						//Get player name
						$player_name = getPlayerName($player_id);
						$user_name = getDisplayName($user_idd);
						$form = "<form method=\"POST\" action=\"/index.php/delete_player_from_tournament\">
							<input type=\"hidden\" value=$tournament_id name=\"tournament_id\">
							<input type=\"hidden\" value=$user_id name=\"user_id\">
							<input type=\"hidden\" value=$player_id name=\"player_id\">
							<input type=\"submit\" class=\"btn btn-danger btn-full\" value=\"Kick Player\">
							</form>";

						$active = "<strong style=\"color: red\">$active_count active</strong>";
						$scheduled = "<strong style=\"color: orange\">$scheduled_count scheduled</strong>";
						$str .= "<tr><td>$user_name</td><td>$player_name</td><td class=\"center-cell text-center\">$active</td><td class=\"center-cell text-center\">$scheduled</td><td>$form</td></tr>";
					}
				}
				$str .= "</table>
							</div>
							<div class=\"col-md-3\"></div>
						</div>";
				echo $str;



			?>

			<br><br>
			<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						<h2>Tournament Controls</h2>
					</div>
					<div class="col-md-3"></div>
				</div>

			<?php

				$str = "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-2\">
					<a class=\"btn btn-primary btn-overwrite\" href=\"start_tournament.php?id=$tournament_id\">Start Tournament</a>
				</div>
				<div class=\"col-md-2\">
					<a class=\"btn btn-primary btn-overwrite\" href=\"stop_tournament.php?id=$tournament_id\" onclick=\"return confirm('Are you sure? \\r\\n Stopping the tournament means not being able to resume.')\">Stop Tournament</a>
				</div>
				<div class=\"col-md-2\">
					<a class=\"btn btn-primary btn-overwrite\" href=\"pause_tournament.php?id=$tournament_id\">Pause Tournament</a>
				</div>
				<div class=\"col-md-2\">
					<a class=\"btn btn-primary btn-overwrite\" href=\"archive_tournament.php?id=$tournament_id\" onclick=\"return confirm('Are you sure? \\r\\nDeleting the tournament will archive ALL relevant files/logs. The log files of matches will be available under archived tournaments.')\">Delete Tournament</a>
				</div>

				<div class=\"col-md-3\"></div>
			</div>";
				echo $str;

			?>

		</div>
		<script type="text/javascript">
			$( document ).ready(function() {

			  // Get the form fields and hidden div
			  var checkbox = $("#resource_limit");
			  var cpu = $("#cpu_limit");
			  var mem = $("#memory_limit");

			  cpu.hide();
			  mem.hide();
			  if (checkbox.is(':checked')) {
			      // Show the hidden fields.
			      cpu.show();
			      mem.show();
			    } else {
			      // Make sure that the hidden fields are indeed
			      // hidden.
			      cpu.hide();
			      mem.hide();
			    }

			  checkbox.change(function() {
			    if (checkbox.is(':checked')) {
			      // Show the hidden fields.
			      cpu.show();
			      mem.show();
			    } else {
			      // Make sure that the hidden fields are indeed
			      // hidden.
			      cpu.hide();
			      mem.hide();
			    }
			  });
			});
		</script>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde.</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>
