<html lang="en">
<!DOCTYPE html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/default.css" rel="stylesheet">
		<link rel="stylesheet" href="css/default.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-10">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="login.php">Home <span class="sr-only">(current)</span></a></li>
								<li><a href="tournaments.php">Tournaments</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li class="active"><a href="login.php">Login</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>

		<?php
            include "php/errors.php";
            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }

            if (isset($_SESSION['user_email'])) {
            	header("Location: /index.php/home/");
            }
		?>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 login-wrapper">
				<b id="sign-up-header">Login</b>
				<form action="/index.php/login_request" method="post">
					<br>
					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Username:</div><div class="col-md-6"> <input class="form-control" type="text" name="email"></div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Password:</div><div class="col-md-6"> <input class="form-control" type="password" name="pass"></div>
					</div>
					<br>
					<div class="row">
  					<div class="col-md-5"></div>
  					<div class="col-md-4">
  						<a href="">Forgot Password?</a><br><br>
  					</div>
  					<div class="col-md-3"></div>
  					</div>

  					<div class="row">
  					<div class="col-md-5"></div>
  					<div class="col-md-4">
  						<input type="submit" class="btn btn-primary button-overwrite" value="Login">
  					</div>
  					<?php
  						require_once 'vendor/autoload.php';
						/* -------- Constants --------- */
						########## Google Settings.Client ID, Client Secret from https://console.developers.google.com #############
						$client_id = '368593521306-0529lu50jg199mto8i8nhsod8p73ruhd.apps.googleusercontent.com';
						$client_secret = '7wjvj6un20l_J32tDkYhmX2g';
						$redirect_uri = 'http://bladevm10.cs.sun.ac.za/index.php/google-login-request/';

						###################################################################

						$client = new Google_Client();
						$client->setClientId($client_id);
						$client->setClientSecret($client_secret);
						$client->setRedirectUri($redirect_uri);
						$client->setScopes(array(
						"https://www.googleapis.com/auth/userinfo.email",
						));

						$service = new Google_Service_Oauth2($client);

						//if we have access_token continue, or else get login URL for user
						if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
						  $client->setAccessToken($_SESSION['access_token']);
						} else {
						  $authUrl = $client->createAuthUrl();
						}

						echo '<div style="margin:20px">';
						if (isset($authUrl)){
						    //show login url
						    echo '<div>';
						    echo '<a class="login" href="' . $authUrl . '"><img src="images/google-login-button.png" /></a>';
						    echo '</div>';
						    unset($authUrl);
						} else {

						    $user = $service->userinfo->get(); //get user info

						   	echo $user->email;
						   	echo $user->name;
						}
						echo '</div>';

  					?>

  					</div>
  					<hr class="seperator">
  					<br><br>
				</form>
				<div class="black-border">
					<b id="sign-up-header">Don't have an account?</b>
					<br><br>
					<div class="row">
						<div class="col-md-5"></div>
						<div class="col-md-4">
							<a class="btn btn-primary button-overwrite" href="#" disabled>Sign Up</a>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
				<br>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde.</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>
