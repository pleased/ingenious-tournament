<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>View Tournament</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/default.css" rel="stylesheet">
		<link rel="stylesheet" href="css/w3.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li ><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown active">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							    <li><a href="engines.php">Referees</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
		<?php
			include "php/connect.php";
        	include("php/errors.php");

        	if (!isset($_SESSION)) {
				session_start();
			}

			if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }

			$id = getUserId($_SESSION['user_email']);
		?>

		<div id="tournament_information"></div>
        <div id="my_players"></div>

        <?php

        	$form = "<div class=\"row\">
        			<div class=\"col-md-3\"></div>
        			<div class=\"col-md-6\">
        			<h2>Add a player to the Tournament</h2>
        			</div>
        			<div class=\"col-md-3\"></div>
        			</div>
        	<form method=\"POST\" action=\"/index.php/add_player_to_tournament\">
						<div class=\"row\">
							<div class=\"col-md-3\"></div>
							<div class=\"input-label col-md-1 form-group\"><strong>Player:</strong></div>
							<div class=\"col-md-4 form-group\">
							<input type=\"hidden\" value=$tournament_id name=\"tournament_id\">
							<input type=\"hidden\" value=$id name=\"user_id\">
							<select name=\"player_id\" class=\"form-control\" id=\"players\">";

									$sql = "SELECT id,name FROM players WHERE user_id = ?";
									$link2 = connect();
									$stmt2 = $link2->prepare($sql);
									$stmt2->bind_param("i", $id);
									$stmt2->execute();
									$stmt2->store_result();
									$num_rows_p = $stmt2->num_rows;
									$stmt2->bind_result($player_id, $player_name);
									if ($num_rows_p == 0) {
										$form .= "<option value=\"You have no available players.\"</option>";
									} else {
										while ($stmt2->fetch()) {
											$form .= "<option data-value=\"". $player_id ."\">$player_name</option>";
										}
									}
							$form .= "</select></div>
									</div>
						<div class=\"row\">
						<div class=\"col-md-3\"></div>
						<div class=\"col-md-6\"><input type=\"submit\" class=\"btn btn-primary button-full\" value=\"Add Player\"></div>
						<div class=\"col-md-3\"></div>
						</div>
						</form>";
			$str = "</table>
							<br><br>
							$form
							<div class=\"row\">
							<div class=\"col-md-3\"></div>
							<div class=\"col-md-6\"></div>
							<div class=\"col-md-3\"></div>
							</div>
						</div>
					</div>";
			echo $str;

		?>

		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
	<script src="js/control.js"></script>
</html>
