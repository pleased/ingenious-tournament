<?php
	include("connect.php");
	include("errors.php");

    if (isset($error)) {
    	echo $error;
    } else if (isset($success)) {
    	echo $success;
    }
    if (isset($_GET['id'])) {
		$tournament_id = $_GET['id'];
	}
	//Get user id
	$id = getUserId($_SESSION['user_email']);

	if (!isset($tournament_id)) {
		header('Location: '.'/TE/my_tournaments.php');
	}

	//Get Tournament name
	$tournament_name = getTournamentName($tournament_id);

	//Setup header for My players
	$str = "<div class=\"row\">
		<div class=\"col-md-3\"></div>
		<div class=\"col-md-6\">
			<h2>$tournament_name - My players</h2>
		</div>
		<div class=\"col-md-3\"></div>
	</div>";



	//Get player ids from tournament
	$link = connect();
	$sql = "SELECT player_id, rating, status, active_count, scheduled_count FROM tournament_players WHERE user_id = (?) AND tournament_id = (?) AND archived = 0";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("ii", $id, $tournament_id);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($player_id, $rating, $status, $active_count, $scheduled_count);

	if (!$stmt) {
		/* TODO: Error handling */
		die("Could not display players.");
	}

	if ($num_rows == 0) {
		echo $str;
		$str = "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">
					<strong>No players in tournament.</strong>
				</div>
				<div class=\"col-md-3\"></div>
			</div><br><br>";
		echo $str;
		$str = "";
		/*$form = "<div class=\"row\">
			<div class=\"col-md-3\"></div>
			<div class=\"col-md-6\">
				<form method=\"POST\" action=\"add_player.php\">
					<input type=\"hidden\" value=$tournament_id name=\"tournament_id\">
					<input type=\"submit\" class=\"btn btn-primary button-full\" value=\"Add Player\">
					</form>
			</div>
			<div class=\"col-md-3\"></div>
		</div>";
		echo $form;*/
	} else {
		$str .= "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">
					<table class=\"tournament_table\">
						<tr>
							<th>Player Name</th>
							<th>Active Matches</th>
							<th>Scheduled Matches</th>
							<th>Rating</th>
							<th>Status</th>
							<th>Delete</th>
						</tr>";
		while ($stmt->fetch()) {

			$sql = "SELECT name FROM players WHERE id = ?";
			$link1 = connect();
			$stmt1 = $link1->prepare($sql);
			$stmt1->bind_param("i", $player_id);
			$stmt1->execute();
			$stmt1->store_result();
			$num_rows = $stmt1->num_rows;
			$stmt1->bind_result($player_name);
			$stmt1->fetch();

			if ($num_rows != 0) {
				$str .= "<tr><td>$player_name</td>";
				$active = "<strong style=\"color: red\">$active_count active</strong>";
				$scheduled = "<strong style=\"color: orange\">$scheduled_count scheduled</strong>";


				$str .= "<td class=\"center-cell text-center\">$active</td>";
				$str .= "<td class=\"center-cell text-center\">$scheduled</td>";
				$str .= "<td class=\"center-cell text-center\">$rating</td>";
				$form = "<form method=\"POST\" action=\"/index.php/delete_player_from_tournament\">
						<input type=\"hidden\" value=$tournament_id name=\"tournament_id\">
						<input type=\"hidden\" value=$id name=\"user_id\">
						<input type=\"hidden\" value=$player_id name=\"player_id\">
						<input type=\"submit\" class=\"btn btn-danger button-full\" value=\"Delete Player\">
						</form>";
				if ($status == 4) {
					$stat = "<strong><p style=\"color: red\">Deactivated</p></strong>";
				} else {
					$stat = "<strong><p style=\"color: green\">Working</p></strong>";
				}

				$str .= "<td>$stat</td>
					<td>$form</td>";
				$str .= "</tr>";
			}
		}
		$str .= "</table></div></div><br><br>";
	}
	echo $str;
?>