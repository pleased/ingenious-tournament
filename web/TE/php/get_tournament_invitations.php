<?php
	include("connect.php");
	include("errors.php");
	$str = "<div class=\"row\">
		<div class=\"col-md-3\"></div>
		<div class=\"col-md-6\">
			<h2>Tournament Invitations</h2>
		</div>
		<div class=\"col-md-3\"></div>
	</div>";
	if (!isset($_SESSION)) {
		session_start();
	}

	$id = getUserId($_SESSION['user_email']);
	$tournament_id = $_GET['tournament_id'];

	/*TODO :  Ensure user viewing is admin*/

	$link = connect();
	$sql = "SELECT id, user_id, email, status FROM tournament_invitations WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);

	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($invite_id, $invited_id, $invited_email, $status);

	if ($num_rows == 0) {
		$str .= "<div class=\"row\">
			<div class=\"col-md-3\"></div>
			<div class=\"col-md-6\">
				<b>No Tournament Invitations.</b>
			</div>
			<div class=\"col-md-3\"></div>
		</div>";
		echo $str;
	} else {
		echo $str;
		$str = "<div class=\"row\">
		<div class=\"col-md-3\"></div>
		<div class=\"col-md-6\">
			<table class=\"tournament_table\">
				<tr>
					<th class=\"center_cell\">Email</th>
					<th class=\"center_cell\">Status</th>
				</tr>";
		while ($stmt->fetch()) {
			$str .= "<tr>";
			$str .= "<td class=\"center_cell\">$invited_email</td>";

			if ($status == 0) {
				$stat = "<p style=\"color:blue\">Pending..</p>";
				$buttons = "<td><a href=\"\" class=\"btn btn-full btn-warning\">Edit</a></td><td class=\"center_cell\"><a class=\"btn btn-danger btn-full\" href=\"decline_invite.php?invite_id=$invite_id\">Decline Invite</a></td>";
			} else if ($status == 1) {
				$stat = "<p style=\"color:green\">Accepted</p>";
				$buttons = "<td><a href=\"\" class=\"btn btn-full btn-warning\">Edit</a></td><td class=\"center_cell\"><a class=\"btn btn-danger btn-full\" href=\"delete_invite.php?invite_id=$invite_id\">Delete Invite</a></td>";
			} else if ($status == 2) {
				$stat = "<p style=\"color:red\">Declined/Deleted</p>";
				$buttons = "<td class=\"center_cell\"><a class=\"btn btn-danger btn-full\" href=\"delete_invite.php?invite_id=$invite_id\">Delete Invite</a></td>";
			}

			$str .= "<td class=\"center_cell\">$stat</td>";


			$str .= "$buttons</tr>";
		}

		$str .= "</table>
			</div>
			<div class=\"col-md-3\"></div>
			</div>";
			echo $str;
		}

?>