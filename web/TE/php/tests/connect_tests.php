<?php

use PHPUnit\Framework\TestCase;
/* To run phpunit --bootstrap ../connect.php connect_tests.php */
/* Ensure the database is the test database */

final class connect_tests extends TestCase {
    public function test_isUserAdmin() {
        $this->assertEquals(True, isUserAdmin(1,1));
        $this->assertEquals(False, isUserAdmin(2,1));
    }

    public function test_isUserInTournament() {
        $this->assertEquals(True, isUserInTournament(1,1));
        $this->assertEquals(False, isUserInTournament(2,1));
    }

    public function test_getTournamentStatus() {
        $this->assertEquals(0, getTournamentStatus(1));
    }

    public function test_getUserId() {
        $this->assertEquals(1, getUserId("root"));
    }

    public function test_getMatchLogDir() {
        $this->assertEquals("", getMatchLogDir(1));
    }

    public function test_getTournamentName() {
        $this->assertEquals("TestTournament", getTournamentName(1));
    }

    public function test_getNumTournamentUsers() {
        $this->assertEquals(1, getNumTournamentUsers(1));
    }

    public function test_getMaxTournamentUsers() {
        $this->assertEquals(5, getMaxTournamentUsers(1));
    }

    public function test_getNumUsersPlayers() {
        $this->assertEquals(2, getNumUsersPlayers(1,1));
    }

    public function test_getTournamentCreator() {
        $this->assertEquals(1, getTournamentCreator(1));
    }

    public function test_getUserName() {
        $this->assertEquals("root", getUserName(1));
    }

    public function test_getDisplayName() {
        $this->assertEquals("root", getDisplayName(1));
    }

    public function test_getPlayerId() {
        $this->assertEquals(1, getPlayerId("MNKRandomEngine"));
    }

    public function test_getPlayerFile() {
        $this->assertEquals("/var/www/html/TE/uploads/player_uploads/MNKRandomEngine.jar", getPlayerFile(1));
    }

    public function test_getPlayerNames() {
        $this->assertEquals(Array("MNKRandomEngine", "MNKMCTSPlayer"), getPlayerNames(1,1));
    }

    public function test_getPlayerClassname() {
        $this->assertEquals("za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine", getPlayerClassname(1));
    }

    public function test_getPlayerName() {
        $this->assertEquals("MNKRandomEngine", getPlayerName(1));
    }

    public function test_getPlayerOwnerId() {
        $this->assertEquals(1, getPlayerOwnerId(1));
    }

    public function test_getTournamentEngineName() {
        $this->assertEquals("MNKReferee", getTournamentEngineName(1));
    }

    public function test_getEngineName() {
        $this->assertEquals("MNKReferee", getEngineName(1));
    }

    public function test_getSchedulerName() {
        $this->assertEquals("2PlayerRoundRobin", getSchedulerName(1));
        $this->assertEquals("1PlayerRoundRobin", getSchedulerName(2));
    }

    public function test_getRankerName() {
        $this->assertEquals("EloRanker", getRankerName(1));
        $this->assertEquals("ScoreRanker", getRankerName(2));
    }

    public function test_getResourcesName() {
        $this->assertEquals("Standard Docker Image", getResourcesName(1));
    }


}


