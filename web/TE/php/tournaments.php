<?php
	include "connect.php";

	/**
     * Create a tournament with the specified settings
     * @param user_id - Id of user that is creating the tournament
     * @param name - Name of the tournament to create
     * @param engine - Name of the engine to be used for the tournament
     * @param ranker - Name of the ranker to be used for the tournament
     * @param scheduler - Name of the scheduler to be used fo the tournament
     * @param resources - Name of the resource image to be used for matches in the tournament
     * @param max_users - Max number of users allowed to join the tournament
     * @param max_players - Max number of players allowed to be submitted by each user of the tournament
     * @param private - is the tournament private
     * @return Redirects to the appropriate page
    */
	function create_tournament($user_id, $name, $engine, $ranker, $scheduler, $limit_resources, $max_users, $max_players, $private, $limit_mem, $limit_cpu, $mem_affix) {


		if ($limit_resources == "on") {
			$mem = $limit_mem . $mem_affix;
			$limit_resources = getResourceId($mem, $limit_cpu);
			if ($limit_resources == -1) {
				$link = connect();
				$sql = "INSERT INTO resources (memory, cpu) VALUES (?, ?)";
				$stmt = $link->prepare($sql);
				$stmt->bind_param("sd", $mem, $limit_cpu);
				if (!$stmt->execute()) {
					$error = "<strong>Error adding resouce limitations, please contact an adminstrator</strong>";
					header('Location: '.'/TE/create_tournament.php?error='.$error);
					exit;
				} else {
					$limit_resources = getResourceId($mem, $limit_cpu);
				}
			}
		} else {
			$limit_resources = -1;
		}

		$res = validate($user_id, $name, $engine, $ranker, $scheduler, $limit_resources, $max_users, $max_players, $private);

		$ranker_id = get_ranker_id($ranker, $user_id);
		$scheduler_id = get_scheduler_id($scheduler, $user_id);
		$engine_id = get_engine_id($engine, $user_id);

		if ($private == 'on') {
			$private = 1;
		} else {
			$private = 0;
		}

		$tournament_id = add_tournament($user_id, $name, $engine_id, $ranker_id, $scheduler_id, $limit_resources, $max_users, $max_players, $private);

		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/config_uploads/";
		$target_file = $target_dir . "tournament_" . $tournament_id . ".json";
		$uploadOk = 1;
		$FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(isset($_FILES["config_file"])) {
			if (file_exists($target_file)) {
			    $error = "<strong>Sorry, File already exists</strong>";
				header('Location: '.'/TE/create_tournament.php?error='.$error);
				exit;
			} else if ($_FILES["file"]["size"] > 1000000) {
			    $error = "<strong>Sorry, your configuration file is too large</strong>";
				header('Location: '.'/TE/create_tournament.php?error='.$error);
				exit;
			}

		    if (!move_uploaded_file($_FILES["config_file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Configuration file could not be uploaded, please contact an adminstrator</strong>";
				header('Location: '.'/TE/create_tournament.php?error='.$error);
				exit;
			}
		} else {
			$error = "<strong>No config file specified..</strong>";
			header('Location: '.'/TE/create_tournament.php?error='.$error);
			exit;
		}
		update_tournament_config($tournament_id, $target_file);
		add_user_to_tournament($tournament_id, $user_id);

		header("Location: /TE/my_tournaments.php?success=<strong>Successfully created Tournament: $name </strong>");
		exit;

	}

	/**
     * Update the tournaments config file in the filesystem
     * @param tournament id - The tournament to update
     * @param user_id - User id trying to update
     * @param config_file - The uplaoded config file
     * @return Redirects to the appropriate page
    */
	function update_tournament_config_file($tournament_id, $user_id, $config_file) {

		$status = getTournamentStatus($tournament_id);

		if ($status == 1) {
			$error = "<strong>Cannot change configuration file while tournament is active</strong>";
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=".$error);
			exit;
		}


		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/config_uploads/";
		$target_file = $target_dir . "tournament_" . $tournament_id . ".json";
		unlink($target_file);
		$uploadOk = 1;
		$FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(isset($_FILES["config_file"])) {
			if (file_exists($target_file)) {
			    $error = "<strong>Sorry, File already exists</strong>";
				header('Location: '.'/TE/create_tournament.php?error='.$error);
				exit;
			} else if ($_FILES["config_file"]["size"] > 1000000) {
			    $error = "<strong>Sorry, your configuration file is too large</strong>";
				header('Location: '.'/TE/create_tournament.php?error='.$error);
				exit;
			} else if ($_FILES["config_file"]["type"] != "application/json") {
				$error = "<strong>Sorry, only .json files are allowed</strong>";
				header('Location: '.'/TE/create_tournament.php?error='.$error);
				exit;
			}

		    if (!move_uploaded_file($_FILES["config_file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Configuration file could not be uploaded, please contact an adminstrator</strong>";
				header('Location: '.'/TE/create_tournament.php?error='.$error);
				exit;
			}
		} else {
			$error = "<strong>No config file specified..</strong>";
			header('Location: '.'/TE/create_tournament.php?error='.$error);
			exit;
		}

		update_tournament_config($tournament_id, $target_file);

		$success = '<strong>Successfully updated the tournament config file!</strong>';
		header("Location: /TE/manage_tournament.php?id=$tournament_id&success=$success");
		exit;
	}

	/**
     * Update the tournaments config file in the database
     * @param tournament id - The tournament to update
     * @param config - The file location of the updated config
     * @return res - Result of executing query (True OR Flase)
    */
	function update_tournament_config($tournament_id, $config) {
		$sql = "UPDATE tournaments SET config = ? WHERE id = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("si", $config, $tournament_id);
		$res = $stmt->execute();
		return $res;
	}

	/**
     * Update the tournament
     * @param tournament id - The tournament to update
     * @param user_id - User id trying to update
     * @param name - Updated name of the tournament
     * @param ranker - Updated ranker name to be used
     * @param name - Updated max user allowed to join the tournament
     * @param max_players - Updated max players allowed per user
     * @param private - Private of public tournament
     * @return Redirects to the appropriate page
    */
	function update_tournament($tournament_id, $user_id, $name, $ranker, $max_users, $max_players, $private, $mem, $affix, $cpu) {

		if (validate_update($tournament_id, $user_id, $name, $ranker, $max_users, $max_players, $private)) {
			$link = connect();

			$ranker_id = get_ranker_id($ranker, $user_id);
			$memm = $mem . $affix;
			$limit_resources = getResourceId($memm, $cpu);
			if ($limit_resources == -1) {
				$link = connect();
				$sql = "INSERT INTO resources (memory, cpu) VALUES (?, ?)";
				$stmt = $link->prepare($sql);
				$stmt->bind_param("sd", $memm, $cpu);
				if (!$stmt->execute()) {
					$error = "<strong>Error adding resouce limitations, please contact an adminstrator</strong>";
					header("Location: /TE/manage_tournament.php?id=$tournament_id&error=$error");
					exit;
				} else {
					$limit_resources = getResourceId($memm, $cpu);
				}
			}

			if ($private == 'on') {
				$private = 1;
			} else {
				$private = 0;
			}

			$sql = "UPDATE tournaments SET name = ?, ranker_id = ?, max_users = ?, max_players = ?, private = ?, resources_id = ? WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("siiiiii", $name, $ranker_id, $max_users, $max_players, $private, $limit_resources, $tournament_id);
			$res = $stmt->execute();
			if (!$res) {
				$error = '<strong>Failed to update the tournament details. Please contact an administrator.</strong>';
				header("Location: /TE/manage_tournament.php?id=$tournament_id&error=".$error);
				exit;
			} else {
			 	$success = '<strong>Successfully updated the tournament details!</strong>';
				header("Location: /TE/manage_tournament.php?id=$tournament_id&success=$success");
				exit;
			 }

		}


	}

	/**
     * Get the ranker id
     * @param ranker - Name of the ranker
     * @param user_id - User id the ranker belongs to
     * @return ranker_id
    */
	function get_ranker_id($ranker, $user_id) {
		$sql = "SELECT id FROM rankers WHERE (user_id = ? OR user_id = -1) AND name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("is", $user_id, $ranker);
		$stmt->execute();
		$stmt->bind_result($ranker_id);
		$stmt->fetch();
		return $ranker_id;
	}

	/**
     * Get the scheduler id
     * @param scheduler - Name of the scheduler
     * @param user_id - User id the scheduler belongs to
     * @return scheduler_id
    */
	function get_scheduler_id($scheduler, $user_id) {
		$sql = "SELECT id FROM schedulers WHERE (user_id = ? OR user_id = -1) AND name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("is", $user_id, $scheduler);
		$stmt->execute();
		$stmt->bind_result($scheduler_id);
		$stmt->fetch();
		return $scheduler_id;
	}

	/**
     * Get the engine id
     * @param engine - Name of the engine
     * @param user_id - User id the engine belongs to
     * @return engine_id
    */
	function get_engine_id($engine, $user_id) {

		$sql = "SELECT id FROM engines WHERE (user_id = ? OR public = 1) AND name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("is", $user_id, $engine);
		$stmt->execute();
		$stmt->bind_result($engine_id);
		$stmt->fetch();
		return $engine_id;
	}

	/**
     * Get the resource id
     * @param resource - Name of the resource
     * @return resource_id
    */
	function get_resources_id($resources) {

		$sql = "SELECT id FROM resources WHERE name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("s", $resources);
		$stmt->execute();
		$stmt->bind_result($resource_id);
		$stmt->fetch();

		return $resource_id;
	}

	/**
     * Validate updated tournament settings
     * @param tournament_id - Id of the tournament
     * @param user_id - Id of user that is creating the tournament
     * @param name - Name of the tournament
     * @param ranker - Id of the ranker
     * @param max_users - Max number of users allowed to join the tournament
     * @param max_players - Max number of players allowed to be submitted by each user of the tournament
     * @param private - is the tournament private
     * @return True if all settings are valid OR False otherwise
    */
	function validate_update($tournament_id, $user_id, $name, $ranker, $max_users, $max_players, $private){
		$sql = "SELECT * FROM tournaments WHERE name = ? AND id <> ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("si", $name, $tournament_id);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows >= 1) {
			$error = '<strong>Please choose a unique tournament name.</strong>';
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=".$error);
			exit;
		}


		$sql = "SELECT * FROM rankers WHERE (user_id = ? OR user_id = -1) AND name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("is", $user_id, $ranker);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows == 0) {
			$error = '<strong>Ranker does not exist.</strong>';
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=".$error);
			exit;
		}

		$sql = "SELECT user_id, count(*) as num_players FROM tournament_players WHERE tournament_id = ? GROUP BY user_id";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $tournament_id);
		$stmt->execute();
		$stmt->bind_result($user_id, $num_players);

		while ($stmt->fetch()) {
			if ($max_players < $num_players) {
				$error = "<strong>The tournament already contains $num_rows players. Please kick some players should you wish to decrease the maximum allowed players.</strong>";
				header("Location: /TE/manage_tournament.php?id=$tournament_id&error=".$error);
				exit;
			}
		}

		$sql = "SELECT * FROM tournament_users WHERE tournament_id = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $tournament_id);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($max_users < $num_rows) {
			$error = "<strong>The tournament already contains $num_rows users. Please kick some users and their players should you wish to decrease the maximum allowed users.</strong>";
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=".$error);
			exit;
		}

		return true;

	}

	/**
     * Validate tournament settings
     * @param user_id - Id of user that is creating the tournament
     * @param name - Name of the tournament to create
     * @param engine - Name of the engine to be used for the tournament
     * @param ranker - Name of the ranker to be used for the tournament
     * @param scheduler - Name of the scheduler to be used fo the tournament
     * @param resources - Name of the resource image to be used for matches in the tournament
     * @param max_users - Max number of users allowed to join the tournament
     * @param max_players - Max number of players allowed to be submitted by each user of the tournament
     * @param private - is the tournament private
     * @return True if all settings are valid OR False otherwise
    */
	function validate($user_id, $name, $engine, $ranker, $scheduler, $resources, $max_users, $max_players, $private) {
		$sql = "SELECT * FROM tournaments WHERE name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("s", $name);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows >= 1) {
			$error = '<strong>Please choose a unique tournament name.</strong>';
			header('Location: '.'/TE/create_tournament.php?error='.$error);
			exit;
		}


		$sql = "SELECT * FROM rankers WHERE (user_id = ? OR user_id = -1) AND name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("is", $user_id, $ranker);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows == 0) {
			$error = '<strong>Ranker does not exist.</strong>';
			header('Location: '.'/TE/create_tournament.php?error='.$error);
			exit;
		}

		$sql = "SELECT * FROM schedulers WHERE (user_id = ? OR user_id = -1) AND name = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("is", $user_id, $scheduler);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows == 0) {
			$error = '<strong>Scheduler does not exist.</strong>';
			header('Location: '.'/TE/create_tournament.php?error='.$error);
			exit;
		}

		return true;

	}

	/**
     * Add a user to the specified tournament
     * @param tournament_id - The id of the tournament to add the user to
     * @param user_id - Id of the user to add
     * @return Redirects to the appropriate page
    */
	function add_user_to_tournament($tournament_id, $user_id) {
		$link = connect();
		$sql = "INSERT INTO tournament_users (tournament_id, user_id, admin) VALUES (?,?,?)";
		$stmt = $link->prepare($sql);
		$admin = 1;
		$stmt->bind_param("iii", $tournament_id, $user_id, $admin);
		$res = $stmt->execute();

		if (!$res) {
			$stmt1 = $link->prepare("DELETE FROM tournaments WHERE id = (?)");
			$stmt1->bind_param("i", $tournament_id);
			$stmt1->execute();
			$error = "<strong>Tournament could not be created...</strong>";
			header('Location: /TE/create_tournament.php?error='.$error);
			exit;
		} else {
			return;
		}

	}

	/**
     * Add the tournament to the database
     * @param user_id - Id of user that is creating the tournament
     * @param name - Name of the tournament to create
     * @param engine - ID of the engine to be used for the tournament
     * @param ranker - ID of the ranker to be used for the tournament
     * @param scheduler - ID of the scheduler to be used fo the tournament
     * @param resources - ID of the resource image to be used for matches in the tournament
     * @param max_users - Max number of users allowed to join the tournament
     * @param max_players - Max number of players allowed to be submitted by each user of the tournament
     * @param private - is the tournament private
     * @return Redirects to the appropriate page
    */
	function add_tournament($user_id, $name, $engine, $ranker, $scheduler, $resources, $max_users, $max_players, $private) {

		$sql = "INSERT INTO `tournaments`(`user_id`, `name`, `engine_id`, `ranker_id`, `scheduler_id`, `private`, `max_users`, `max_players`, `resources_id`, `status`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 0)";
		$link = connect();
		$stmt = $link->prepare($sql);
		$name = strip_tags($name);
		$stmt->bind_param("isiiiiiii", $user_id, $name, $engine, $ranker, $scheduler, $private, $max_users, $max_players, $resources);
		$stmt->store_result();


		if (!$stmt->execute()) {
			$error = "<strong>Tournament could not be created..</strong>";
			header('Location: /TE/create_tournament.php?error='.$error);
			exit;
		} else {
			$stmt1 = $link->prepare("SELECT LAST_INSERT_ID()");
			$stmt1->execute();
			$stmt1->bind_result($last_id);
			$stmt1->fetch();
			return $last_id;
		}
	}

	/**
     * Start the specified tournament
     * @param id - Tournament id to start
     * @return Redirects to the appropriate page
    */
	function start_tournament($id) {
		$sql = "UPDATE tournaments SET status = ? WHERE id = ? ";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ii", 1, $id);
		$stmt->execute();
		$res = $stmt->get_results();
		if (!$res) {
			$error = "<strong>Tournament could not be started..</strong>";
			header('Location: /TE/manage_tournament.php?error='.$error);
			exit;
		} else {
			$success = "<strong>Tournament could not be started..</strong>";
			header('Location: /TE/manage_tournament.php?success='.$success);
		}
	}

?>