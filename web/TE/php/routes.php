<?php
	include "connect.php";
	include "authentication.php";
	include "tournaments.php";
	include "players.php";
	include "schedulers.php";
	include "rankers.php";
	include "engines.php";
	include "users.php";

	/**
	 * Navigates the browser to the home page.
	*/
	function home_page() {
		header('Location: '.'/TE/dashboard.php');
	}

	/**
	 * Navigates the browser to the login page
	*/
	function login() {
		header('Location: '.'/TE/login.php');
	}

	/**
	 * Navigates the browser to the authentication of a google login
	*/
	function google_login($code) {
		authenticate_google_login($code);
	}

	function set_displayname_call($dname, $user_id) {
		set_display_name($dname, $user_id);
	}

	/**
	 * Navigates the browser to the 404 page
	*/
	function missing_page() {
		header('Location: '.'/TE/wrong.php');
	}

	function login_in_authentication($email, $pass) {
		authenticate_login($email, $pass);
	}

	function google_login_authentication() {
		authenticate_google_login();
	}

	function create_tournament_call($user_id, $name, $engine, $ranker, $scheduler, $limit_resources, $max_users, $max_players, $private, $limit_mem, $limit_cpu, $mem_affix) {
		create_tournament($user_id, $name, $engine, $ranker, $scheduler, $limit_resources, $max_users, $max_players, $private, $limit_mem, $limit_cpu, $mem_affix);
	}

	function update_tournament_call($tournament_id, $user_id, $name, $ranker, $max_users, $max_players, $private, $mem, $affix, $cpu) {
		update_tournament($tournament_id, $user_id, $name, $ranker, $max_users, $max_players, $private, $mem, $affix, $cpu);
	}

	function update_tournament_config_call($tournament_id, $user_id, $config_file) {
		update_tournament_config_file($tournament_id, $user_id, $config_file);
	}

	function start_tournament_call($id) {
		start_tournament($id);
	}

	function add_scheduler_call($scheduler_name, $user_id) {
		add_scheduler($scheduler_name, $user_id);
	}

	function update_scheduler_call($scheduler_name, $scheduler_id, $user_id) {
		update_scheduler($scheduler_name, $scheduler_id, $user_id);
	}

	function update_ranker_call($ranker_name, $ranker_id, $user_id) {
		update_ranker($ranker_name, $ranker_id, $user_id);
	}

	function delete_scheduler_call($scheduler_id, $user_id) {
		delete_scheduler($scheduler_id, $user_id);
	}

	function add_ranker_call($ranker_name, $user_id, $file) {
		add_ranker($ranker_name, $user_id, $file);
	}

	function delete_ranker_call($ranker_id, $user_id) {
		delete_ranker($ranker_id, $user_id);
	}

	function add_player_call($player_name, $user_id, $class_name) {
		add_player($player_name, $user_id, $class_name);
	}

	function delete_player_call($player_id, $user_id) {
		delete_player($player_id, $user_id);
	}

	function delete_user_call($admin_id, $user_id, $tournament_id) {
		delete_user($admin_id, $user_id, $tournament_id);
	}

	function add_player_to_tournament_call($user_id, $player_id, $tournament_id) {
		add_player_to_tournament($user_id, $player_id, $tournament_id);
	}

	function delete_player_from_tournament_call($user_id, $player_id, $tournament_id) {
		delete_player_from_tournament($user_id, $player_id, $tournament_id);
	}

	function add_engine_call($engine_name, $user_id, $classname, $public) {
		add_engine($engine_name, $user_id, $classname, $public);
	}

	function delete_engine_call($engine_id, $user_id) {
		delete_engine($engine_id, $user_id);
	}
?>