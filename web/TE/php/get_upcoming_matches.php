<?php
	include("connect.php");
	include("errors.php");
	$str = "<div class=\"row\">
	<div class=\"col-md-2\"></div>
	<div class=\"col-md-8\">
		<h2>Upcoming Matches</h2>
	</div>
	<div class=\"col-md-2\"></div>
</div>";

	//Get user ID for relating to db
	if (!isset($_SESSION)) {
		session_start();
	}
	$id = getUserId($_SESSION['user_email']);


	//Get match IDs that contain the user ID. i.e The user participated.
	$link = connect();
	$sql = "SELECT id,tournament_id, player_ids FROM matches WHERE result_id = ? AND status = ? AND user_ids LIKE ? AND archived = 0 ORDER BY tournament_id";
	$stmt = $link->prepare($sql);
	$res = -1;
	$status = 0;
	$user_like = "%$id%";
	$stmt->bind_param("iis", $res, $status, $user_like);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($match_id, $tournament_id, $player_ids);

	if (!$stmt) {
		die ("Upcoming matches could not be displayed.");
	}

	if ($num_rows == 0) {
		$str .= "<div class=\"row\">
			<div class=\"col-md-2\"></div>
			<div class=\"col-md-8\">
				<b>No matches scheduled</b>
			</div>
			<div class=\"col-md-2\"></div>
		</div>";
		echo $str;
	} else {

		$str .= "<div class=\"row\">
		<div class=\"col-md-2\"></div>
		<div class=\"col-md-8\">
			<table class=\"tournament_table\">
				<tr>
					<th class=\"center_cell\">Tournament Name</th>
				</tr>";


		$first = 1;
		$tournament_pos = 1;
		$table_names = Array();
		$table_counter = 0;

		while ($stmt->fetch()) {
			if ($tournament_id == $curr_t_id) {
				$playerids = explode(",", $player_ids);
				$players = "<br>" . getPlayerName($playerids[0]) . "<br>";
				for ($i = 1; $i < count($playerids); $i++) {
						$player_name = getPlayerName($playerids[$i]);
						$players .= "<b class=\"center\">vs</b><br> " .  $player_name . "<br>";
					}
				$players .= "<br>";
				$str .= "<tr>
							<td class=\"center_cell\">$tournament_pos</td>
							<td class=\"center_cell\">$players</td>
							<td class=\"center_cell\">
							<a href=\"match.php?id=$match_id\" class=\"btn btn-full btn-primary\">View Match</a>
										</td>
									</tr>";
			} else {
				$curr_t_id = $tournament_id;
				$tournament_pos = 1;

				if ($first) {
					$first = 0;
				} else {
					$str .= "</tbody><div class=\"col-md-12 text-center\">
		      				<ul class=\"pagination pagination-lg pager\" id=\"upcoming_pager_$tournament_name\"></ul>
		      					</div></table></td></tr>";
				}

				echo $str;
				$tournament_name = getTournamentName($tournament_id);
				array_push($table_names, "$tournament_name");
				$str = "<tr>
							<td class=\"center_cell\">
								$tournament_name
							</td>
							<td class=\"tournament_matches\">
							<div class=\"table-responsive\">
							<table class=\"table-bordered tournament_table_inside\" >
								<thead>
								<tr>
									<th class=\"center_cell\">#</th>
									<th class=\"center_cell\">Players</th>
									<th class=\"center_cell\">View Match</th>
								</tr>
								</thead><tbody id=\"upcoming_table_$tournament_name\">";

				$playerids = explode(",", $player_ids);
				$players = "<br>" . getPlayerName($playerids[0]) . "<br>";
				for ($i = 1; $i < count($playerids); $i++) {
						$player_name = getPlayerName($playerids[$i]);
						$players .= "<b class=\"center\">vs</b><br> " .  $player_name . "<br>";
					}
				$players .= "<br>";
				$str .= "<tr>
							<td class=\"center_cell\">$tournament_pos</td>
							<td class=\"center_cell\">";

				$str .= "$players</td>
							<td class=\"center_cell\">
							<a href=\"match.php?id=$match_id\" class=\"btn btn-full btn-primary\">View Match</a>
										</td>
									</tr>";
			}
			$tournament_pos++;
		}

		$str .= "</tbody><div class=\"col-md-12 text-center\">
		      				<ul class=\"pagination pagination-lg pager\" id=\"upcoming_pager_$tournament_name\"></ul>
		      					</div></table></div></td></tr></table></div><div class=\"col-md-2\"></div>";
		echo $str;

		$str = "<script>
					$(document).ready(function(){
						";


		for ($i=0; $i < sizeof($table_names); $i++) {
				$str .= "$('#upcoming_table_$table_names[$i]').pageMe({pagerSelector:'#upcoming_pager_$table_names[$i]',showPrevNext:true,hidePageNumbers:false,perPage:3});
				";
		}
		$str .= "});
		</script>";
		echo $str;
	}
?>