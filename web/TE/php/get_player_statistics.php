<?php
	include("connect.php");
	include("errors.php");

	if (isset($player_id)) {
		$player_name = getPlayerName($player_id);
		$str = "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">
					<h2>$player_name Statistics</h2>
				</div>
				<div class=\"col-md-3\"></div>
			</div>";

		if (!isset($_SESSION)) {
			session_start();
		}

		$id = getUserId($_SESSION['user_email']);
		$email = $_SESSION['user_email'];

		//Get player statistics

		$link = connect();
		$sql = "SELECT id, result_id FROM matches WHERE player_ids LIKE (?) AND status = (?) AND archived = 0";
		$stmt = $link->prepare($sql);
		$like_id = "%".$player_id."%";
		$status = 2;
		$stmt->bind_param("si", $like_id, $status);
		$stmt->execute();
		$stmt->bind_result($match_id, $result_id);

		$total_matches = 0;
		$won = 0;
		$lost = 0;
		$draw = 0;
		$forfeit = 0;
		while ($stmt->fetch()) {
			if ($result_id == -1) {
				continue;
			}
			$total_matches = $total_matches + 1;

			if (getMatchWinner($result_id) == $player_id) {
				$won = $won +1;
			} else if (getMatchWinner($result_id) == -1) {
				$draw = $draw + 1;
			} else {
				$lost = $lost + 1;
			}
		}
		if ($total_matches != 0) {
			$ratio = (int)$won/(int)$total_matches * (100);
		} else {
			$ratio = 0;
		}

		$str .= "<div class=\"row\">
			<div class=\"col-md-3\"></div>
			<div class=\"col-md-6\">
			<table class=\"tournament_info\">
			<tr>
			<th>Player Name</th>
			<td class=\"text-center\">$player_name</td>
			</tr>
			<tr>
			<th>Matches Played</th>
			<td class=\"text-center\">$total_matches</td>
			</tr>
			<tr>
			<th>Matches Won</th>
			<td class=\"text-center\">$won</td>
			</tr>
			<tr>
			<th>Matches Lost</th>
			<td class=\"text-center\">$lost</td>
			</tr>
			<tr>
			<th>Matches Drawn</th>
			<td class=\"text-center\">$draw</td>
			</tr>
			</tr>
			<tr>
			<th>Win Ratio</th>
			<td class=\"text-center\">$ratio %</td>
			</tr>
			</table>
			</div>
			<div class=\"col-md-3\"></div>
		</div>";


		echo $str;
	} else {
		$error = "<strong>No player specified. Please choose a player to view.</strong>";
		header('Location: '.'/TE/my_players.php?error='.$error);
	}

?>