<?php
	include "connect.php";

	/**
     * Add a ranker to the filesystem and the database
     * @param ranker_name - Name of the ranker to add
     * @param user_id - Id of the user adding the ranker
     * @return Redirects to the appropriate page
    */
	function add_ranker($ranker_name, $user_id, $file) {

		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/ranker_uploads/";
		$link = connect();
		$sql = "SELECT name FROM rankers WHERE 1";
		$stmt = $link->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($r_name);

		while ($stmt->fetch()) {
			if ($r_name == $ranker_name) {
				$error = "<strong>$ranker_name already exists. Please choose a unique name</strong>";
				header('Location: '.'/TE/add_ranker.php?error='.$error);
				exit;
			}
		}


		 $target_file = $target_dir . $ranker_name . ".jar";
		 $uploadOk = 1;
		 $FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(isset($_FILES["file"])) {
			if ($target_file == ($target_dir.".jar")) {
			    $error = "<strong>Please enter a name.</strong>";
				header('Location: '.'/TE/add_ranker.php?error='.$error);
				exit;
			} else if (file_exists($target_file)) {
			    $error = "<strong>Sorry, File already exists</strong>";
				header('Location: '.'/TE/add_ranker.php?error='.$error);
				exit;
			} else if ($_FILES["file"]["size"] > 5000000) {
			    $error = "<strong>Sorry, your file is too large</strong>";
				header('Location: '.'/TE/add_ranker.php?error='.$error);
				exit;
			}

		    if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Ranker could not be uploaded, please contact an adminstrator</strong>";
				header('Location: '.'/TE/add_ranker.php?error='.$error);
				exit;
			}
		} else {
			$error = "<strong>No file specified..</strong>";
			header('Location: '.'/TE/add_ranker.php?error='.$error);
			exit;
		}

		$link = connect();
		$sql = "INSERT INTO rankers (user_id, name, file) VALUES (?, ?, ?)";
		$stmt = $link->prepare($sql);
		$file = $target_file;
		$ranker_name = strip_tags($ranker_name);
		$stmt->bind_param("iss", $user_id, $ranker_name, $file);
		if ($stmt->execute()) {
			$success = "<strong>Ranker \"$ranker_name\" successfully added</strong>";
			header('Location: '.'/TE/rankers.php?success='.$success);
			exit;
		} else {
			$error = "<strong>Ranker could not be added</strong>";
			header('Location: '.'/TE/add_ranker.php?error='.$error);
			exit;
		}
	}


	/**
     * Delete a ranker from the filesystem and the database
     * @param ranker_name - Name of the ranker to delete
     * @param user_id - Id of the user deleting the ranker
     * @return Redirects to the appropriate page
    */
	function delete_ranker($ranker_id, $user_id) {
		if (tournamentUsesRanker($ranker_id)) {
			$error = "<strong>Could not remove the ranker because it is currently being used in a tournament.</strong>";
			header('Location: '.'/TE/rankers.php?error='.$error);
			exit;
		}

		//Get ranker dir
		$link = connect();
		$sql = "SELECT file FROM rankers WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $ranker_id);
		$stmt->execute();
		$stmt->bind_result($file_name);
		$stmt->fetch();

		//Delete ranker entry
		$link = connect();
		$sql = "DELETE FROM rankers WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $ranker_id);
		$res = $stmt->execute();

		if (!$res) {
			$error = "<strong>Could not remove the ranker, please contact the adminstrator</strong>";
			header('Location: '.'/TE/rankers.php?error='.$error);
			exit;
		} else {
			unlink($file_name);
			$success = "<strong>Ranker was successfully deleted</strong>";
			header('Location: '.'/TE/rankers.php?success='.$success);
			exit;
		}

	}

	function update_ranker($ranker_name, $ranker_id, $user_id) {
		if (tournamentUsesRanker($ranker_id)) {
			$error = "<strong>Could not remove the ranker because it is currently being used in a tournament.</strong>";
			header('Location: '.'/TE/rankers.php?error='.$error);
			exit;
		}

		$link = connect();
		$sql = "SELECT name, file FROM rankers WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $ranker_id);
		$stmt->execute();
		$stmt->bind_result($old_name, $old_target_file);
		$stmt->fetch();


		$link = connect();
		$sql = "SELECT name FROM rankers WHERE id <> ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $ranker_id);
		$stmt->execute();
		$stmt->bind_result($s_name);

		while ($stmt->fetch()) {
			if ($s_name == $ranker_name) {
				$error = "<strong>$ranker_name already exists. Please choose a unique name</strong>";
				header("Location: /TE/edit_ranker.php?id=$ranker_id&error=".$error);
				exit;
			}
		}
		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/ranker_uploads/";
		$target_file = $target_dir . $ranker_name . ".jar";
		$uploadOk = 1;
		$FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(file_exists($_FILES['file']['tmp_name'])) {
			unlink($old_target_file);
			if ($target_file == ($target_dir.".jar")) {
			    $error = "<strong>Please enter a name.</strong>";
				header("Location: /TE/edit_ranker.php?id=$ranker_id&error=".$error);
				exit;
			} else if ($_FILES["file"]["size"] > 5000000) {
			    $error = "<strong>Sorry, your file is too large</strong>";
				header("Location: /TE/edit_ranker.php?id=$ranker_id&error=".$error);
				exit;
			}
		    if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Ranker could not be uploaded, please contact an adminstrator</strong>";
				header("Location: /TE/edit_ranker.php?id=$ranker_id&error=".$error);
				exit;
			}

			$link = connect();
			$sql = "UPDATE rankers SET name = ?, file = ? WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ssi", $ranker_name, $target_file, $ranker_id);
			if (!$stmt->execute()) {
				$error = "<strong>Ranker could not be updated</strong>";
				header("Location: /TE/edit_ranker.php?id=$ranker_id&error=".$error);
				exit;
			}
		} else {
			$link = connect();
			$sql = "UPDATE rankers SET name = ? , file = ? WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ssi", $ranker_name, $target_file, $ranker_id);
			if (!rename($old_target_file, $target_file)) {
		        $error = "<strong>Ranker could not be changed, please contact an adminstrator</strong>";
				header("Location: /TE/edit_ranker.php?id=$ranker_id&error=".$error);
				exit;
			}
			if (!$stmt->execute()) {
				$error = "<strong>Ranker could not be updated</strong>";
				header("Location: /TE/edit_ranker.php?id=$ranker_id&error=".$error);
				exit;
			}
		}


		$success = "<strong>Ranker \"$ranker_name\" successfully updated</strong>";
		header("Location: /TE/edit_ranker.php?id=$ranker_id&success=".$success);
		exit;

	}

?>