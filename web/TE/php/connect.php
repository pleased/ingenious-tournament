<?php
	/**
     * Connects to the database
     *
     * @return link - the link to the database.
    */
    if (!function_exists('connect')) {
    	function connect() {
    		$configs = include("db_config.php");
			$link = new mysqli($configs->host, $configs->username, $configs->pass, $configs->database);

			if (!$link) {
				    echo "Error: Unable to connect to MySQL." . PHP_EOL;
					    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
					    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
						    exit;
			}

			return $link;
		}
	}


	/**
     * Check if the tournament has the required number of players
     *
     * @return boolean
    */
	if (!function_exists('hasEnoughPlayers')) {
    	function hasEnoughPlayers($tournament_id) {
			//Check that enough players have joined the tournament
			$link = connect();
			$sql = "SELECT * FROM tournament_players WHERE tournament_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $tournament_id);
			$stmt->execute();
			$stmt->store_result();
			$num_players = $stmt->num_rows;
			close($link);

			$link = connect();
			$sql = "SELECT engine_id FROM tournaments WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $tournament_id);
			$stmt->execute();
			$stmt->bind_result($engine_id);
			$stmt->fetch();
			close($link);

			if ($num_players >= $min_players) {
				return True;
			} else {
				return False;
			}
		}
	}

	/**
     * Check if user is admin of tournament
     *
     * @return True or false
    */
	if (!function_exists('isUserAdmin')) {
    	function isUserAdmin($user_id, $tournament_id) {
			//check if user is admin of tournament
			$link = connect();
			$sql = "SELECT * FROM tournament_users WHERE user_id = ? AND admin = ? AND tournament_id = ?";
			$stmt = $link->prepare($sql);
			$admin = 1;
			$stmt->bind_param("iii", $user_id, $admin, $tournament_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			close($link);
			if ($num_rows == 0) {
				return False;
			} else {
				return True;
			}
		}
	}

	/**
     * Check if user is admin of tournament
     *
     * @return True or false
    */
	if (!function_exists('getResourceId')) {
    	function getResourceId($mem, $cpu) {
			//check if user is admin of tournament
			$link = connect();
			$sql = "SELECT id FROM resources WHERE memory = ? AND cpu = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("sd", $mem, $cpu);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			if ($num_rows == 0) {
				close($link);
				return -1;
			} else {
				$stmt->bind_result($id);
				$stmt->fetch();
				close($link);
				return $id;
			}

		}
	}

	/**
     * Check if user is admin of tournament
     *
     * @return True or false
    */
	if (!function_exists('getcpuResources')) {
    	function getcpuResources($id) {
			//check if user is admin of tournament
			$link = connect();
			$sql = "SELECT cpu FROM resources WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($cpu);
			$stmt->fetch();
			close($link);
			return $cpu;
		}
	}

	/**
     * Check if user is admin of tournament
     *
     * @return True or false
    */
	if (!function_exists('getMemResources')) {
    	function getMemResources($id) {
			//check if user is admin of tournament
			$link = connect();
			$sql = "SELECT memory FROM resources WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($mem);
			$stmt->fetch();
			close($link);
			return $mem;
		}
	}

	/**
     * Check if the user has already joined the tournament
     * @param user id
     * @param tournament id
     * @return True or False
    */
	if (!function_exists('isUserInTournament')) {
    	function isUserInTournament($user_id, $tournament_id) {
			//check if user is in the tournament
			$link = connect();
			$sql = "SELECT * FROM tournament_users WHERE user_id = ? AND tournament_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ii", $user_id, $tournament_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			close($link);
			if ($num_rows == 0) {
				return False;
			} else {
				return True;
			}
		}
	}

	/**
     * Get tournament status
     * @param tournament id
     * @return status - Tournament status
    */
	if (!function_exists('getTournamentStatus')) {
    	function getTournamentStatus($tournament_id) {
			//Get tournament status
			$link = connect();
			$sql = "SELECT status FROM tournaments WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $tournament_id);
			$stmt->execute();
			$stmt->bind_result($status);
			$stmt->fetch();
			close($link);
			return $status;
		}
	}


	/**
     * Get user id of specified email
     * @param email
     * @return user_id - Corresponding user_id
    */
	if (!function_exists('getUserId')) {
    	function getUserId($email) {
			//Get user id of user logged in.
			$link = connect();
			$sql = "SELECT id FROM users WHERE email = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("s", $email);
			$stmt->execute();
			$stmt->bind_result($id);
			$stmt->fetch();
			close($link);
			return $id;
		}
	}

	/**
     * Get log dir of match id
     * @param
     * @return dir - log location
    */
	if (!function_exists('getMatchLogDir')) {
    	function getMatchLogDir($log_id) {
			//Get log directory for match with id
			$link = connect();
			$sql = "SELECT log_dir FROM match_log_files WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $log_id);
			$stmt->execute();
			$stmt->bind_result($dir);
			$stmt->fetch();
			close($link);
			return $dir;
		}
	}

	/**
     * Get tournament name
     * @param tournament id
     * @return tournament name
    */
	if (!function_exists('getTournamentName')) {
    	function getTournamentName($id) {
			//Get tournament name
			$link = connect();
			$sql = "SELECT name FROM tournaments WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($tournament_name);
			$stmt->fetch();
			close($link);
			return $tournament_name;
		}
	}


	/**
     * Is the engine used in a tournament
     * @param engine id
     * @return True or False
    */
	if (!function_exists('tournamentUsesEngine')) {
    	function tournamentUsesEngine($engine_id) {
			$link = connect();
			$sql = "SELECT * FROM tournaments WHERE engine_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $engine_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			close($link);
			if ($num_rows > 0) {
				return True;
			} else {
				return False;
			}
		}
	}

	/**
     * Is the ranker used in a tournament
     * @param ranker_id
     * @return True or False
    */
	if (!function_exists('tournamentUsesRanker')) {
    	function tournamentUsesRanker($ranker_id) {
			$link = connect();
			$sql = "SELECT * FROM tournaments WHERE ranker_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $ranker_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			close($link);
			if ($num_rows > 0) {
				return True;
			} else {
				return False;
			}
		}
	}

	/**
     * Is the scheduler used in a tournament
     * @param scheduler id
     * @return True or False
    */
	if (!function_exists('tournamentUsesScheduler')) {
    	function tournamentUsesScheduler($scheduler_id) {
			$link = connect();
			$sql = "SELECT * FROM tournaments WHERE scheduler_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $scheduler_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			close($link);
			if ($num_rows > 0) {
				return True;
			} else {
				return False;
			}
		}
	}

	/**
     * Get number of users in tournament
     * @param tournament id
     * @return number of users
    */
	if (!function_exists('getNumTournamentUsers')) {
    	function getNumTournamentUsers($id) {
			//Return the number of users in the tournament
			$link = connect();
			$sql = "SELECT * FROM tournament_users WHERE tournament_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			close($link);
			return $num_rows;
		}
	}

	/**
     * Get maximum allowed tournament users
     * @param tournament id
     * @return Max allowed users
    */
	if (!function_exists('getMaxTournamentUsers')) {
    	function getMaxTournamentUsers($id) {
			//Return the number of users in the tournament
			$link = connect();
			$sql = "SELECT max_users FROM tournaments WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($max_users);
			$stmt->fetch();
			close($link);
			return $max_users;
		}
	}

	/**
     * Get number of players submitted by a user
     * @param tournament id
     * @param user id
     * @return num_rows - Num players
    */
	if (!function_exists('getNumUsersPlayers')) {
    	function getNumUsersPlayers($tournament_id, $user_id) {
			//Return the number of players the user has in the tournament
			$link = connect();
			$sql = "SELECT * FROM tournament_players WHERE tournament_id = ? AND user_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ii", $tournament_id, $user_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			close($link);
			return $num_rows;
		}
	}

	/**
     * Get tournament creator
     * @param tournament id
     * @return user_id - Tournament Creator
    */
	if (!function_exists('getTournamentCreator')) {
    	function getTournamentCreator($id) {
			//Get tournament creator
			$link = connect();
			$sql = "SELECT user_id FROM tournaments WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($user_id);
			$stmt->fetch();
			close($link);
			return $user_id;
		}
	}


	/**
     * Get users name
     * @param user_id
     * @return user name
    */
	if (!function_exists('getUserName')) {
    	function getUserName($id) {
			//Get user name
			$link = connect();
			$sql = "SELECT email FROM users WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($user_name);
			$stmt->fetch();
			close($link);
			return $user_name;
		}
	}

	/**
     * Get display name of user
     * @param user_id
     * @return Display name
    */
	if (!function_exists('getDisplayName')) {
    	function getDisplayName($id) {
			//Get user name
			$link = connect();
			$sql = "SELECT display_name FROM users WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($display_name);
			$stmt->fetch();
			close($link);
			return $display_name;
		}
	}

	/**
     * Get player name
     * @param player id
     * @return player name
    */
	if (!function_exists('getPlayerId')) {
    	function getPlayerId($player_name) {
			//Get player ID
			$link = connect();
			$sql = "SELECT id FROM players WHERE name = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("s", $player_name);
			$stmt->execute();
			$stmt->bind_result($player_id);
			$stmt->fetch();
			close($link);
			return $player_id;
		}
	}

	/**
     * Get location of player jar file
     * @param player id
     * @return player_dir
    */
	if (!function_exists('getPlayerFile')) {
    	function getPlayerFile($player_id) {
			//Get player file
			$link = connect();
			$sql = "SELECT dir FROM players WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $player_id);
			$stmt->execute();
			$stmt->bind_result($player_dir);
			$stmt->fetch();
			close($link);
			return $player_dir;
		}
	}

	/**
     * Get all names belonging to a user in a specified tournament
     * @param user id
     * @param tournament id
     * @return player names - array
    */
	if (!function_exists('getPlayerNames')) {
    	function getPlayerNames($user_id, $tournament_id) {
			//Get player file
			$link = connect();
			$sql = "SELECT name FROM players
					JOIN tournament_players ON players.id = tournament_players.player_id
					WHERE tournament_players.user_id = ?
					AND tournament_players.tournament_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ii", $user_id, $tournament_id);
			$stmt->execute();
			$stmt->bind_result($player_name);
			$player_names = Array();
			while ($stmt->fetch()) {
				array_push($player_names, $player_name);
			}
			close($link);
			return $player_names;
		}
	}

	/**
     * Get player class name
     * @param player id
     * @return class name
    */
	if (!function_exists('getPlayerClassname')) {
    	function getPlayerClassname($player_id) {
			//Get player file
			$link = connect();
			$sql = "SELECT classname FROM players WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $player_id);
			$stmt->execute();
			$stmt->bind_result($player_class);
			$stmt->fetch();
			close($link);
			return $player_class;
		}
	}


	/**
     * Get a players name
     * @param player id
     * @return player name
    */
	if (!function_exists('getPlayerName')) {
    	function getPlayerName($player_id) {
			//Get player name
			$link = connect();
			$sql = "SELECT name FROM players WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $player_id);
			$stmt->execute();
			$stmt->bind_result($player_name);
			$stmt->fetch();
			close($link);
			return $player_name;
		}
	}

	/**
     * Get a players owner id
     * @param player id
     * @return user_id - owner of player
    */
	if (!function_exists('getPlayerOwnerId')) {
    	function getPlayerOwnerId($player_id) {
			//Get player rating
			$link = connect();
			$sql = "SELECT user_id FROM players WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $player_id);
			$stmt->execute();
			$stmt->bind_result($player_owner);
			$stmt->fetch();
			close($link);
			return $player_owner;
		}
	}

	/**
     * Get engine name of a tournament
     * @param tournament id
     * @return Engine name
    */
	if (!function_exists('getTournamentEngineName')) {
    	function getTournamentEngineName($id) {
			//Get tournaments engine name
			$link = connect();
			$sql = "SELECT engine_id FROM tournaments WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($engine_id);
			$stmt->fetch();
			close($link);
			return getEngineName($engine_id);
		}
	}

	/**
     * Get engine name
     * @param engine id
     * @return engine name
    */
	if (!function_exists('getEngineName')) {
    	function getEngineName($id) {
			//Get engine name
			$link = connect();
			$sql = "SELECT name FROM engines WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($engine_name);
			$stmt->fetch();
			close($link);
			return $engine_name;
		}
	}

	/**
     * Get a schedulers name
     * @param scheduler id
     * @return Scheduler name
    */
	if (!function_exists('getSchedulerName')) {
    	function getSchedulerName($id) {
			//Get scheduler name
			$link = connect();
			$sql = "SELECT name FROM schedulers WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($scheduler_name);
			$stmt->fetch();
			close($link);
			return $scheduler_name;
		}
	}

	/**
     * Get a rankers name
     * @param ranker id
     * @return Ranker name
    */
	if (!function_exists('getRankerName')) {
    	function getRankerName($id) {
			//Get ranker name
			$link = connect();
			$sql = "SELECT name FROM rankers WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($ranker_name);
			$stmt->fetch();
			close($link);
			return $ranker_name;
		}
	}

	/**
     * Get match winners id from result id
     * @param result id
     * @return player_id - Winner of the match
    */
	if (!function_exists('getMatchWinner')) {
    	function getMatchWinner($result_id) {
			//Get winner of match
			$link = connect();
			$sql = "SELECT winner FROM match_result WHERE result_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $result_id);
			$stmt->execute();
			$stmt->bind_result($winner_id);
			$stmt->fetch();
			close($link);
			return $winner_id;
		}
	}

	/**
     * Get result id of match
     * @param match_id
     * @return result id
    */
	if (!function_exists('getWinnerId')) {
    	function getWinnerId($match_id) {
			//Get winner of match
			$link = connect();
			$sql = "SELECT result_id FROM matches WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $match_id);
			$stmt->execute();
			$stmt->bind_result($result_id);
			$stmt->fetch();
			close($link);
			return $result_id;
		}
	}

	/**
     * Get name of resource
     * @param resource id
     * @return resource name
    */
	if (!function_exists('getResourcesName')) {
    	function getResourcesName($id) {
			//Get resources name
			$link = connect();
			$sql = "SELECT name FROM resources WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt->bind_result($resources_name);
			$stmt->fetch();
			close($link);
			return $resources_name;
		}
	}

	/**
     * Closes the connection to the datatbase
     * @return
    */
	if (!function_exists('close')) {
    	function close($link) {
			$link->close();
			return;
		}
	}

?>