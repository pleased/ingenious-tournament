<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/TE/php/connect.php";

	$link = connect();
	$sql = "SELECT display_name FROM users WHERE 1=1";
	$stmt = $link->prepare($sql);
	$stmt->execute();
	$stmt->bind_result($display_name);

	$val = trim($_GET['displayname']);
	if (empty($val)) {
		echo "<div class=\"col-md-8 col-md-push-2\">
                            <div class=\"alert alert-danger fade in\">
                                <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                                <strong> The display name can not be empty. </strong>
                            </div>
                        </div>";
        exit;
	}

	while ($stmt->fetch()) {
		if ($display_name == $val) {
			echo "<div class=\"col-md-8 col-md-push-2\">
                            <div class=\"alert alert-danger fade in\">
                                <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                                <strong> The name \"$val\" already exists, please choose another </strong>
                            </div>
                        </div>";
			exit;
		}
	}

	echo "";
	exit;

?>