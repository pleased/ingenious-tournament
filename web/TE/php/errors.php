<?php

    include ("connect.php");

    if(!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['user_email'])) {
        if ($_SERVER['PHP_SELF'] == "/TE/login.php") {

        } else {
            header('Location: '.'/TE/login.php');
        }
    } else {
        $link = connect();
        $sql = "SELECT display_name FROM users WHERE email = ?";
        $stmt = $link->prepare($sql);
        $stmt->bind_param("s", $_SESSION['user_email']);
        $stmt->execute();
        $stmt->bind_result($display_name);
        $stmt->fetch();

        if (empty($display_name)) {
            header("Location: /TE/profile.php?success=<strong>In order to use the tournament engine please select a display name for public use.</strong>");
            exit();
        }
    }


    if(isset($_GET)) {
        if (isset($_GET['error'])) {
            $error =   ' <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <div class="alert alert-danger fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                '.$_GET['error'].'
                            </div>
                        </div>
                    </div>';
        }
        if (isset($_GET['success'])) {
            $success =  ' <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <div class="alert alert-success fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                '.$_GET['success'].'
                            </div>
                        </div>
                    </div>';
        }
        if (isset($_GET['failed'])) {
            $success =  ' <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <div class="alert alert-danger fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                The following invitations failed to get created:<br>';
                                $failed = $_SESSION['failed'];
                                foreach ($failed as $key => $value) {
                                    $success .= "$value <br>";
                                }
                            $success .= '</div>
                        </div>
                    </div>';
        }
        if (isset($_GET['player_id'])) {
            $player_id = $_GET['player_id'];
        }
        if (isset($_GET['id'])) {
    		$tournament_id = $_GET['id'];
    	}
    }
?>