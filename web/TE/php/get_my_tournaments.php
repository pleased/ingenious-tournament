<?php
	include("connect.php");
	include("errors.php");
	$str = "<div class=\"row\">
		<div class=\"col-md-2\"></div>
		<div class=\"col-md-8\">
			<h2>My Tournaments</h2>
		</div>
		<div class=\"col-md-2\"></div>
	</div>";
	if (!isset($_SESSION)) {
		session_start();
	}

	$id = getUserId($_SESSION['user_email']);

	$link = connect();
	$sql = "SELECT tournament_id, admin FROM tournament_users WHERE user_id = (?) AND archived = 0";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($tournament_id, $admin);

	if (!$stmt) {
		die("Joined tournaments cannot be displayed.");
	}

	if ($num_rows == 0) {
		$str .= "<div class=\"row\">
			<div class=\"col-md-2\"></div>
			<div class=\"col-md-8\">
				<b>No Joined Tournaments</b>
			</div>
			<div class=\"col-md-2\"></div>
		</div>";
		echo $str;
	} else {
		echo $str;
		$str = "<div class=\"row\">
		<div class=\"col-md-2\"></div>
		<div class=\"col-md-8\">
			<table class=\"tournament_table\">
				<tr>
					<th class=\"center_cell\">Tournament Name</th>
					<th class=\"center_cell\">Referee</th>
					<th class=\"center_cell\">Creator</th>
					<th class=\"center_cell\">Status</th>
					<th class=\"center_cell\">Type</th>
				</tr>";


		while ($stmt->fetch()) {
			$link1 = connect();
			$sql = "SELECT id, name, engine_id, user_id, status, private FROM tournaments WHERE id = (?)";
			$stmt1 = $link1->prepare($sql);
			$stmt1->bind_param("i", $tournament_id);
			$stmt1->execute();
			$stmt1->store_result();
			$num_rows = $stmt1->num_rows;
			$stmt1->bind_result($id, $name, $engine_id, $user_id, $status, $private);

			if (!$stmt1) {
				die("Joined tournaments cannot be displayed because you have not joined any tournaments.");
			}

			if ($num_rows == 0) {
				/** TODO : no joined tournaments **/
			}

			while ($stmt1->fetch()) {
				$str .= "<tr>";
				$str .= "<td class=\"center_cell\"><a href=\"view_tournament.php?id=". $id ."\">". $name ."</a></td>";
				$link2 = connect();
				$sql = "SELECT name FROM engines WHERE id = ?";
				$stmt2 = $link2->prepare($sql);
				$stmt2->bind_param("i", $engine_id);
				$stmt2->execute();
				$stmt2->bind_result($engine_name);
				$stmt2->fetch();

				$str .= "<td class=\"center_cell\">". $engine_name ."</td>";


				$user_name = getDisplayName($user_id);


				$str .= "<td class=\"center_cell\">". $user_name ."</td>";

				if ($status == 0) {
					$stat = "<p style=\"color:red\">Not Started";
				} else if ($status == 1) {
					$stat = "<p style=\"color:green\">Running";
				} else if($status == 2) {
					$stat = "<p style=\"color:orange\">Paused";
				} else if ($status == 3) {
					$stat = "<p style=\"color:green\">Completed!";
				} else {
					$stat = "ERROR";
				}

				if ($private == 0) {
					$private = " <p style=\"color:green\">Public";
				} else {
					$private = " <p style=\"color:orange\">Private";
				}
				if ($admin) {
					$buttons = "<td><a class=\"btn btn-primary btn-full\" href=\"tournament_ranking.php?id=".$tournament_id."\">Rankings</a></td><td><a class=\"btn btn-warning btn-full\" href=\"manage_tournament.php?id=". $tournament_id ."\">Manage</a></td><td class=\"center_cell\"><a class=\"btn btn-danger btn-full\" href=\"leave_tournament.php?id=". $id ."\">Leave</a></td>";
				} else {
					$buttons = "<td><a class=\"btn btn-primary btn-full\" href=\"tournament_ranking.php?id=".$tournament_id."\">Rankings</a></td><td><a class=\"btn btn-warning btn-full\" disabled>Manage</a></td><td class=\"center_cell\"><a class=\"btn btn-danger btn-full\" href=\"leave_tournament.php?id=". $id ."\">Leave</a></td>";
				}

				$str .= "<td class=\"center_cell\">$stat</td><td class=\"center_cell\">$private</td>$buttons</tr>";
			}

		}

		$str .= "</table>
			</div>
			<div class=\"col-md-2\"></div>
			</div>";
			echo $str;

	}

?>