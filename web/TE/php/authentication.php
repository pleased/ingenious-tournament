<?php
	include "connect.php";

	/**
     * Standard login authentication.
     *
     * @param email - the email of the user logging in.
     * @param pass - password of the user logging in.
    */

	function authenticate_login($email, $pass) {
		$correct_email = email_is_in_db($email);

		$correct_pass = pass_is_valid($pass, $email);

		echo $correct_email . "<br>";
		echo $correct_pass . "<br>";

		if ($correct_email && $correct_pass) {

			if(!isset($_SESSION)) {
    			session_start();
			}

			$_SESSION["user_email"] = $email;
			header('Location: '.'/TE/dashboard.php');
			exit();
		} else {
			if (!$correct_email || !$correct_pass) {
				$error = "<strong>Wrong credentials! You have submitted an incorrect email or password.</strong>";
				header('Location: '.'/TE/login.php?error='.$error);
				exit();
			}
		}
	}

	/**
     * Google login authentication.
     *
     * @param code - Google authentication code
     *
    */
	function authenticate_google_login($code) {

		if (session_status() !== PHP_SESSION_ACTIVE) {
			session_start();
		}

		require_once ('TE/vendor/autoload.php');
		$client_id = '368593521306-0529lu50jg199mto8i8nhsod8p73ruhd.apps.googleusercontent.com';
		$client_secret = '7wjvj6un20l_J32tDkYhmX2g';
		$redirect_uri = 'http://bladevm10.cs.sun.ac.za/index.php/google-login-request/';

		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->setScopes(array(
		"https://www.googleapis.com/auth/userinfo.email",
		));


		$token = $client->fetchAccessTokenWithAuthCode($code);
		$client->setAccessToken($token['access_token']);
		$_SESSION['access_token'] = $token['access_token'];

		$plus = new Google_Service_Plus($client);

		$person = $plus->people->get("me");
		$name = $person["displayName"];
		$email = $person["emails"][0]["value"];

		$link = connect();
		$sql = "SELECT * FROM users WHERE email = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows != 0) {
			$_SESSION['user_email'] = $email;
			$success = "<strong>Welcome back $name</strong>";
			header("Location: /TE/dashboard.php?success=" . $success);
			exit;
		} else {
			$link = connect();
			$sql = "INSERT INTO users (email, password) VALUES (?,?)";
			$pass = md5("YOLO");
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ss", $email, $pass);
			$res = $stmt->execute();
			if (!$res) {
				$error = "<strong>Error logging in, please contact an administrator</strong>";
				$error = urlencode($error);
				header("Location: /TE/login.php?error=" . $error);
				exit;
			} else {
				$_SESSION['user_email'] = $email;
				$success = "<strong>Welcome to the Tournament Engine</strong>";
				header("Location: /TE/dashboard.php?success=" . $success);
				exit;
			}
		}
	}

	/**
     * Checks if the password is valid.
     *
     * @param email - the email of the user logging in.
     * @param pass - password of the user logging in.
     * @return true - if password matches that in the database.
     * @return false - if password does not match that in the database.
    */
	function pass_is_valid($pass, $email) {
		$sql = "SELECT password FROM users WHERE email = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->bind_result($db_pass);
		$stmt->fetch();

		if ($db_pass == md5($pass)) {
			return True;
		} else {
			return False;
		}
	}

	/**
     * Checks if email is in the database.
     *
     * @param email - the user's email.
     * @return true - the email is in the database.
     * @return false - the email is not in the database.
    */
	function email_is_in_db($email) {
		$sql = "SELECT password FROM users WHERE email = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$stmt->fetch();
		if ($num_rows > 0) {
			return True;
		} else {
			return False;
		}
	}

	function debug_to_console($data) {

		if ( is_array( $data ) ) {
			$output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
		} else {
			$output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
		}

		echo $output;
	}

?>