<?php
	include("connect.php");
	include("errors.php");
	$str = "<div class=\"row\">
		<div class=\"col-md-2\"></div>
		<div class=\"col-md-8\">
			<h2>My Invitations</h2>
		</div>
		<div class=\"col-md-2\"></div>
	</div>";
	if (!isset($_SESSION)) {
		session_start();
	}

	$id = getUserId($_SESSION['user_email']);
	$email = $_SESSION['user_email'];

	$link = connect();
	$sql = "SELECT id,tournament_id FROM tournament_invitations WHERE (user_id = ? OR email = ?) AND status = ?";
	$stmt = $link->prepare($sql);

	$status = 0;

	$stmt->bind_param("isi", $id, $email, $status);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($invite_id, $tournament_id);

	if ($num_rows == 0) {
		$str .= "<div class=\"row\">
			<div class=\"col-md-2\"></div>
			<div class=\"col-md-8\">
				<b>No Tournament Invitations.</b>
			</div>
			<div class=\"col-md-2\"></div>
		</div>";
		echo $str;
	} else {
		echo $str;
		$str = "<div class=\"row\">
		<div class=\"col-md-2\"></div>
		<div class=\"col-md-8\">
			<table class=\"tournament_table\">
				<tr>
					<th class=\"center_cell\">Tournament Name</th>
					<th class=\"center_cell\">Referee</th>
					<th class=\"center_cell\">Creator</th>
					<th class=\"center_cell\">Tournament Status</th>
					<th class=\"center_cell\">Type</th>
				</tr>";
		while ($stmt->fetch()) {
			$link1 = connect();
			$sql = "SELECT id, name, engine_id, user_id, status, private FROM tournaments WHERE id = (?)";
			$stmt1 = $link1->prepare($sql);
			$stmt1->bind_param("i", $tournament_id);
			$stmt1->execute();
			$stmt1->store_result();
			$num_rows = $stmt1->num_rows;
			$stmt1->bind_result($id, $name, $engine_id, $user_id, $status, $private);

			if (!$stmt1) {
				die("Joined tournaments cannot be displayed because you have not joined any tournaments.");
			}

			if ($num_rows == 0) {
				/** TODO : no joined tournaments **/
			}

			while ($stmt1->fetch()) {
				$str .= "<tr>";
				$str .= "<td class=\"center_cell\">$name</td>";
				$link2 = connect();
				$sql = "SELECT name FROM engines WHERE id = ?";
				$stmt2 = $link2->prepare($sql);
				$stmt2->bind_param("i", $engine_id);
				$stmt2->execute();
				$stmt2->bind_result($engine_name);
				$stmt2->fetch();

				$str .= "<td class=\"center_cell\">". $engine_name ."</td>";


				$user_name = getDisplayName($user_id);


				$str .= "<td class=\"center_cell\">". $user_name."</td>";

				if ($status == 0) {
					$stat = "<p style=\"color:red\">Not Started";
				} else if ($status == 1) {
					$stat = "<p style=\"color:green\">Running";
				} else if($status == 2) {
					$stat = "<p style=\"color:orange\">Paused";
				} else if ($status == 3) {
					$stat = "<p style=\"color:green\">Completed!";
				} else {
					$stat = "ERROR";
				}

				if ($private == 0) {
					$private = " <p style=\"color:green\">Public";
				} else {
					$private = " <p style=\"color:orange\">Private";
				}

				$buttons = "<td><a class=\"btn btn-success btn-full\" href=\"accept_invite.php?invite_id=$invite_id\">Accept</a></td><td class=\"center_cell\"><a class=\"btn btn-danger btn-full\" href=\"decline_invite.php?invite_id=$invite_id\">Decline</a></td>";

				$str .= "<td class=\"center_cell\">$stat</td><td class=\"center_cell\">$private</td>$buttons</tr>";
			}

		}

		$str .= "</table>
			</div>
			<div class=\"col-md-2\"></div>
			</div>";
			echo $str;
		}

?>