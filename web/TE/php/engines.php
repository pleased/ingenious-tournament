<?php

	/**
     * Add a engine to the filesystem and the database
     * @param engine_name - Name of the engine to add
     * @param user_id - Id of the user adding the engine
     * @return Redirects to the appropriate page
    */
	function add_engine($engine_name, $user_id, $classname, $public) {

		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/engine_uploads/";
		$target_file = $target_dir . $engine_name . ".jar";
		$uploadOk = 1;
		$FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(isset($_FILES["file"])) {
			if ($target_file == ($target_dir.".jar")) {
			    $error = "<strong>Please enter a name.</strong>";
				header('Location: '.'/TE/add_engine.php?error='.$error);
				exit;
			} else if (file_exists($target_file)) {
			    $error = "<strong>Sorry, File already exists</strong>";
				header('Location: '.'/TE/add_engine.php?error='.$error);
				exit;
			} else if ($_FILES["file"]["size"] > 500000000) {
			    $error = "<strong>Sorry, your file is too large</strong>";
				header('Location: '.'/TE/add_engine.php?error='.$error);
				exit;
			}

		    if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Referee could not be uploaded, please contact an adminstrator</strong>";
				header('Location: '.'/TE/add_engine.php?error='.$error);
				exit;
			}
		} else {
			$error = "<strong>No file specified..</strong>";
			header('Location: '.'/TE/add_engine.php?error='.$error);
			exit;
		}

		$link = connect();
		$sql = "INSERT INTO engines (user_id, name, file, public, path_to_ref) VALUES (?, ?, ?, ?, ?)";
		$stmt = $link->prepare($sql);
		if ($public == 'on') {
			$public = 1;
		} else {
			$public = 0;
		}
		$engine_name = strip_tags($engine_name);
		$stmt->bind_param("issis", $user_id, $engine_name, $target_file, $public, $classname);
		if ($stmt->execute()) {
			$success = "<strong>Referee \"$engine_name\" successfully added</strong>";
			header('Location: '.'/TE/engines.php?success='.$success);
			exit;
		} else {
			$error = "<strong>Referee could not be added</strong>";
			header('Location: '.'/TE/add_engine.php?error='.$error);
			exit;
		}
	}


	/**
     * Delete a engine from the filesystem and the database
     * @param engine_name - Name of the engine to delete
     * @param user_id - Id of the user deleting the engine
     * @return Redirects to the appropriate page
    */
	function delete_engine($engine_id, $user_id) {
		if (tournamentUsesEngine($engine_id)) {
			$error = "<strong>Could not remove the Referee because a tournament is using the referee</strong>";
			header('Location: '.'/TE/engines.php?error='.$error);
			exit;
		}

		//Get engine dir
		$link = connect();
		$sql = "SELECT file FROM engines WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $engine_id);
		$stmt->execute();
		$stmt->bind_result($file_name);
		$stmt->fetch();

		//Delete scheduler entry
		$link = connect();
		$sql = "DELETE FROM engines WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $engine_id);
		$res = $stmt->execute();

		if (!$res) {
			$error = "<strong>Could not remove the referee, please contact the adminstrator</strong>";
			header('Location: '.'/TE/engines.php?error='.$error);
			exit;
		} else {
			unlink($file_name);
			$success = "<strong>Referee was successfully deleted</strong>";
			header('Location: '.'/TE/engines.php?success='.$success);
			exit;
		}

	}

?>