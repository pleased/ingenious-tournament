<?php
	include "connect.php";
	include "errors.php";

	$str = "<div class=\"row\">
	<div class=\"col-md-2\"></div>
	<div class=\"col-md-8\">
		<h2>Latest Match Results</h2>
	</div>
	<div class=\"col-md-2\"></div>
</div>";

	//Get user ID for relating to db
	$link = connect();
	$sql = "SELECT id FROM users WHERE email = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("s", $_SESSION['user_email']);
	$stmt->execute();
	$stmt->bind_result($id);
	$stmt->fetch();


	//Get match IDs that contain the user ID. i.e The user participated.
	$link = connect();
	$sql = "SELECT id, tournament_id, num_players, player_ids FROM (
  SELECT * FROM matches WHERE status = ? AND user_ids LIKE (?) AND ardhived = 0 ORDER BY id DESC LIMIT 3
) as r ORDER BY id;";
	$stmt = $link->prepare($sql);
	$like_id = "%".$id."%";
	$a = 2;
	$stmt->bind_param("is", $a, $like_id);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($match_id, $tournament_id, $num_players, $playerids);
	$rows_count = 0;

	if (!$stmt) {
		die ("Matches cannot be displayed.");
	}

	if ($num_rows == 0) {
		$str .= "<div class=\"row\">
			<div class=\"col-md-2\"></div>
			<div class=\"col-md-8\">
				<b>No Latest Matches</b>
			</div>
			<div class=\"col-md-2\"></div>
		</div>";
		echo $str;
	} else {
		echo $str;
		$str = "<div class=\"row\">
		<div class=\"col-md-2\"></div>
		<div class=\"col-md-8\">
			<table class=\"tournament_table\">
				<tr>
					<th class=\"center_cell\">Tournament Name</th>
					<th class=\"center_cell\">Players</th>
					<th class=\"center_cell\">Winner</th>
				</tr>";


		while ($stmt->fetch()) {
			$rows_count++;
			$player_ids = explode(",",$playerids);
			$i = 0;
			$player_names = array();
			$link = connect();

			//Get player names
			$sql = "SELECT name FROM players WHERE id = (?);";
			$stmt2 = $link->prepare($sql);
			foreach ($player_ids as $player_id) {
				$stmt2->bind_param("i", $player_id);
				$stmt2->execute();
				$stmt2->bind_result($player_name);
				$stmt2->fetch();
				$player_names[$i] = $player_name;
				$i = $i +1;
			}
			$stmt2->close();

			//Get match results. Winner, 2nd, 3rd etc.
			$link = connect();
			$sql = "SELECT winner FROM match_result WHERE match_id = (?)";
			$stmt2 = $link->prepare($sql);
			$stmt2->bind_param("i", $match_id);
			$stmt2->execute();
			$stmt2->store_result();
			$num_rows = $stmt2->num_rows;
			$stmt2->bind_result($winner);
			$stmt2->fetch();


			//Get Winner name
			if ($winner == -1) {
				$winner_name = "DRAW";
			} else {
				$link = connect();
				$sql = "SELECT name FROM players WHERE id = (?);";
				$stmt2 = $link->prepare($sql);
				$stmt2->bind_param("i", $winner);
				$stmt2->execute();
				$stmt2->bind_result($winner_name);
				$stmt2->fetch();
			}

			//Get tournament name
			$tournament_name = getTournamentName($tournament_id);

			$str .= "<tr><td class=\"center_cell\"><a href=\"view_tournament.php?id=$tournament_id\">$tournament_name</a></td>";
			$str .= "<td class=\"center_cell\">";
			$i = 0;
			$str .= $player_names[$i] . "<br>";
			for ($i = 1; $i < count($player_names); $i++) {
				$str .= "<b class=\"center\">vs</b><br> " .  $player_names[$i] . "<br>";
			}
			$str .= "</p></td><td class=\"center_cell\">$winner_name</td>";
			$str .= "<td><a class=\"btn btn-primary btn-full\" href=\"match.php?id=". $match_id ."\">Download Log</a></td></tr>";


			if ($rows_count >= 3) {
				if ($stmt->fetch()) {
					$hasMore = true;
				} else {
					$hasMore = false;
				}
				break;
			}
		}
		$str .= "</table>";

		if ($hasMore ==  true) {
			$str .= "<a class=\"pull-right\" href=\"#view_more\">View more</a></div>
				<div class=\"col-md-2\"></div>
				</div>";
			echo $str;
		} else {
			$str .= "</div>
			<div class=\"col-md-2\"></div>
			</div>";
			echo $str;
		}
	}
?>