<?php

	include "connect.php";

	/**
     * Add a scheduler to the filesystem and the database
     * @param scheduler_name - Name of the scheduler to add
     * @param user_id - Id of the user adding the scheduler
     * @return Redirects to the appropriate page
    */
	function add_scheduler($scheduler_name, $user_id) {

		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/scheduler_uploads/";
		$link = connect();
		$sql = "SELECT name FROM schedulers WHERE 1";
		$stmt = $link->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($s_name);

		while ($stmt->fetch()) {
			if ($s_name == $scheduler_name) {
				$error = "<strong>$scheduler_name already exists. Please choose a unique name</strong>";
				header('Location: '.'/TE/add_scheduler.php?error='.$error);
				exit;
			}
		}


		 $target_file = $target_dir . $scheduler_name . ".jar";
		 $uploadOk = 1;
		 $FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(isset($_FILES["file"])) {
			if ($target_file == ($target_dir.".jar")) {
			    $error = "<strong>Please enter a name.</strong>";
				header('Location: '.'/TE/add_scheduler.php?error='.$error);
				exit;
			} else if (file_exists($target_file)) {
			    $error = "<strong>Sorry, File already exists</strong>";
				header('Location: '.'/TE/add_scheduler.php?error='.$error);
				exit;
			} else if ($_FILES["file"]["size"] > 5000000) {
			    $error = "<strong>Sorry, your file is too large</strong>";
				header('Location: '.'/TE/add_scheduler.php?error='.$error);
				exit;
			}

		    if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Scheduler could not be uploaded, please contact an adminstrator</strong>";
				header('Location: '.'/TE/add_scheduler.php?error='.$error);
				exit;
			}
		} else {
			$error = "<strong>No file specified..</strong>";
			header('Location: '.'/TE/add_scheduler.php?error='.$error);
			exit;
		}


		$link = connect();
		$sql = "INSERT INTO schedulers (user_id, name, file) VALUES (?, ?, ?)";
		$stmt = $link->prepare($sql);
		$file = $target_file;
		$scheduler_name = strip_tags($scheduler_name);
		$stmt->bind_param("iss", $user_id, $scheduler_name, $file);
		if ($stmt->execute()) {
			$success = "<strong>Scheduler \"$scheduler_name\" successfully added</strong>";
			header('Location: '.'/TE/schedulers.php?success='.$success);
			exit;
		} else {
			$error = "<strong>Scheduler could not be added</strong>";
			header('Location: '.'/TE/add_scheduler.php?error='.$error);
			exit;
		}
	}


	/**
     * Delete a scheduler from the filesystem and the database
     * @param scheduler_id - ID of the scheduler to delete
     * @param user_id - Id of the user deleting the scheduler
     * @return Redirects to the appropriate page
    */
	function delete_scheduler($scheduler_id, $user_id) {
		if (tournamentUsesScheduler($scheduler_id)) {
			$error = "<strong>Could not remove the scheduler because it is currently being used in a tournament.</strong>";
			header('Location: '.'/TE/schedulers.php?error='.$error);
			exit;
		}


		//Get scheduler dir
		$link = connect();
		$sql = "SELECT file FROM schedulers WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $scheduler_id);
		$stmt->execute();
		$stmt->bind_result($file_name);
		$stmt->fetch();

		//Delete scheduler entry
		$link = connect();
		$sql = "DELETE FROM schedulers WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $scheduler_id);
		$res = $stmt->execute();

		if (!$res) {
			$error = "<strong>Could not remove the scheduler, please contact the adminstrator</strong>";
			header('Location: '.'/TE/schedulers.php?error='.$error);
			exit;
		} else {
			unlink($file_name);
			$success = "<strong>Scheduler was successfully deleted</strong>";
			header('Location: '.'/TE/schedulers.php?success='.$success);
			exit;
		}

	}

	function update_scheduler($scheduler_name, $scheduler_id, $user_id) {
		if (tournamentUsesScheduler($scheduler_id)) {
			$error = "<strong>Could not remove the scheduler because it is currently being used in a tournament.</strong>";
			header('Location: '.'/TE/schedulers.php?error='.$error);
			exit;
		}

		$link = connect();
		$sql = "SELECT name, file FROM schedulers WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $scheduler_id);
		$stmt->execute();
		$stmt->bind_result($old_name, $old_target_file);
		$stmt->fetch();


		$link = connect();
		$sql = "SELECT name FROM schedulers WHERE id <> ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $scheduler_id);
		$stmt->execute();
		$stmt->bind_result($s_name);

		while ($stmt->fetch()) {
			if ($s_name == $scheduler_name) {
				$error = "<strong>$scheduler_name already exists. Please choose a unique name</strong>";
				header("Location: /TE/edit_scheduler.php?id=$scheduler_id&error=".$error);
				exit;
			}
		}
		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/scheduler_uploads/";
		$target_file = $target_dir . $scheduler_name . ".jar";
		$uploadOk = 1;
		$FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(file_exists($_FILES['file']['tmp_name'])) {
			unlink($old_target_file);
			if ($target_file == ($target_dir.".jar")) {
			    $error = "<strong>Please enter a name.</strong>";
				header("Location: /TE/edit_scheduler.php?id=$scheduler_id&error=".$error);
				exit;
			} else if ($_FILES["file"]["size"] > 5000000) {
			    $error = "<strong>Sorry, your file is too large</strong>";
				header("Location: /TE/edit_scheduler.php?id=$scheduler_id&error=".$error);
				exit;
			}
		    if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Scheduler could not be uploaded, please contact an adminstrator</strong>";
				header("Location: /TE/edit_scheduler.php?id=$scheduler_id&error=".$error);
				exit;
			}

			$link = connect();
			$sql = "UPDATE schedulers SET name = ?, file = ? WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ssi", $scheduler_name, $target_file, $scheduler_id);
			if (!$stmt->execute()) {
				$error = "<strong>Scheduler could not be updated</strong>";
				header("Location: /TE/edit_scheduler.php?id=$scheduler_id&error=".$error);
				exit;
			}
		} else {
			$link = connect();
			$sql = "UPDATE schedulers SET name = ? , file = ? WHERE id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ssi", $scheduler_name, $target_file, $scheduler_id);
			if (!rename($old_target_file, $target_file)) {
		        $error = "<strong>Scheduler could not be changed, please contact an adminstrator</strong>";
				header("Location: /TE/edit_scheduler.php?id=$scheduler_id&error=".$error);
				exit;
			}
			if (!$stmt->execute()) {
				$error = "<strong>Scheduler could not be updated</strong>";
				header("Location: /TE/edit_scheduler.php?id=$scheduler_id&error=".$error);
				exit;
			}
		}


		$success = "<strong>Scheduler \"$scheduler_name\" successfully updated</strong>";
		header("Location: /TE/edit_scheduler.php?id=$scheduler_id&success=".$success);
		exit;

	}

?>