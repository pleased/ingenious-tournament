<?php
/**
* Sends a message to the application server
* @param message
* @return True if the message was succesfully sent OR False otherwise
**/
function send_to_server($message) {

	$address = '127.0.0.1';
	$port = 8000;

	$socket = socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
	if (!socket_connect($socket, $address, $port)) {
		return FALSE;
	}

	$len = strlen($message);

	$status = socket_sendto($socket, $message, $len, 0, $address, $port);
	if($status !== FALSE) {
		socket_close($socket);
	    return TRUE;
	} else {
		socket_close($socket);
	    return FALSE;
	}
}

?>