<?php
	include("connect.php");
	include("errors.php");

	//Get user id
	$id = getUserId($_SESSION['user_email']);

	if (isset($_GET['id'])) {
		$tournament_id = $_GET['id'];
	} else {
		$tournament_id = -1;
	}

	if ($tournament_id == -1) {
		header('Location: '.'/TE/my_tournaments.php?error=<strong>Please select a tournament to view.</strong>');
	}

	$link = connect();
	$sql = "SELECT name, engine_id, ranker_id, scheduler_id, private, max_users, max_players, resources_id, status FROM tournaments WHERE id = ? AND archived = 0";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;

	if ($num_rows == 0) {
		header('Location: '.'/TE/my_tournaments.php?error=<strong>The tournament you tried to view does not exist.</strong>');
	}
	$stmt->bind_result($tournament_name, $engine_id, $ranker_id, $scheduler_id, $private, $max_users, $max_players, $resources_id, $status);
	$stmt->fetch();

	$str = "<div class=\"row\">
		<div class=\"col-md-3\"></div>
		<div class=\"col-md-6\">
			<h2>$tournament_name Information</h2>
		</div>
		<div class=\"col-md-3\"></div>
	</div>";



	$engine_name = getEngineName($engine_id);
	$scheduler_name = getSchedulerName($scheduler_id);
	$ranker_name = getRankerName($ranker_id);
	if ($resources_id == -1) {
		$resources = "No limitations";
	} else {
		$mem = getMemResources($resources_id);
		$cpu = getcpuResources($resources_id);
		$resources = "<b>Memory Limit</b> : $mem <br><b>CPUs</b> : $cpu";
	}

	$num_users = getNumTournamentUsers($tournament_id);
	$num_my_players = getNumUsersPlayers($tournament_id, $id);

	if ($status == 0) {
		$status = "<strong style=\"color:red\">Not Started</strong>";
	} else if ($status == 1) {
		$status = "<strong style=\"color:green\">Running</strong>";
	} else if($status == 2) {
		$status = "<strong style=\"color:orange\">Paused</strong>";
	} else if ($status == 3) {
		$status = "<strong style=\"color:green\">Completed!</strong>";
	} else {
		$status = "ERROR";
	}

	if ($private == 0) {
		$private = "<strong style=\"color:green\">Public Tournament</strong>";
	} else {
		$private = "<strong style=\"color:red\">Private Tournament</strong>";
	}

	$str .= "<div class=\"row\">
			<div class=\"col-md-3\"></div>
			<div class=\"col-md-6\">
			<table class=\"tournament_info\">
			<tr>
			<th>Tournament Name</th>
			<td class=\"text-center\">$tournament_name</td>
			</tr>
			<tr>
			<th>Referee</th>
			<td class=\"text-center\">$engine_name</td>
			</tr>
			<tr>
			<th>Scheduler</th>
			<td class=\"text-center\">$scheduler_name</td>
			</tr>
			<tr>
			<th>Ranker</th>
			<td class=\"text-center\">$ranker_name</td>
			</tr>
			<tr>
			<th>Users</th>
			<td class=\"text-center\">$num_users / $max_users</td>
			</tr>
			<tr>
			<th>My Players</th>
			<td class=\"text-center\">$num_my_players / $max_players</td>
			</tr>
			<tr>
			<th>Resources</th>
			<td class=\"text-center\">$resources</td>
			</tr>
			<tr>
			<th>Status</th>
			<td class=\"text-center\">$status</td>
			</tr>
			<tr>
			<th>Tournament Type</th>
			<td class=\"text-center\">$private</td>
			</tr>
			</table>
			</div>
			<div class=\"col-md-3\"></div>
		</div>";


	echo $str;

?>