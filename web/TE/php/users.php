<?php
	include "connect.php";


	/**
     * Delete a user from a tournament
     * @param admin_id - user requesting delete
     * @param user_id - user id to delete
     * @param tournament_id - tournament to delete from
     * @return Redirects to the appropriate page
    */
	function delete_user($admin_id, $user_id, $tournament_id) {

		//Check admin of tournament
		$isAdmin =isUserAdmin($admin_id, $tournament_id);
		if (!$isAdmin) {
			header('Location: '.'/TE/my_tournaments.php?error=<strong>You are not an admin of the tournament.</strong>');
			exit();
		}

		if ($admin_id == $user_id) {
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>You cannot kick yourself, please leave the tournament</strong>");
			exit();
		}


		//Check if trying to kick creator
		$creator = getTournamentCreator($tournament_id);

		if ($creator == $user_id) {
			header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>You cannot kick the tournament creator</strong>");
			exit();
		}

		$sql = "SELECT * FROM tournament_players WHERE tournament_id = ? AND user_id = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ii", $tournament_id, $user_id);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows > 0) {
			header('Location: '.'/TE/manage_tournament.php?id='. $tournament_id .'&error=<strong>The user you are trying to kick still has players in the tournament. Please kick the users players before kicking the user.</strong>');
			exit();
		}


		$sql = "DELETE FROM tournament_users WHERE tournament_id = ? and user_id = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ii", $tournament_id, $user_id);
		$res = $stmt->execute();

		if (!$res) {
			header('Location: '.'/TE/manage_tournament.php?id='. $tournament_id .'&error=<strong>The user could not be kicked from the tournament, please contact an administrator.</strong>');
			exit();
		}

		header('Location: '.'/TE/manage_tournament.php?id='. $tournament_id .'&success=<strong>The user was successfully removed from the tournament!</strong>');
		exit();


	}

	/**
     * Set a users display name
     * @param dname - Display name to set to
     * @param user_id - Id of user to set the display name
     * @return Redirects to the appropriate page
    */
	function set_display_name($dname, $user_id) {
		//Check that name does not exist..
		$link = connect();
		$sql = "SELECT display_name FROM users WHERE 1=1";
		$stmt = $link->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($display_name);

		while ($stmt->fetch()) {
			if ($display_name == $dname) {
				header("Location: /TE/profile.php?error=<strong>The display name \"$dname\" already exists</strong>");
				exit;
			}
		}

		//Set the display name
		$link = connect();
		$sql = "UPDATE users SET display_name = ? WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("si", $dname, $user_id);
		if (!$stmt->execute()) {
			header("Location: /TE/profile.php?error=<strong>Could not set display name, please contact an administrator</strong>");
				exit;
		}

		header("Location: /TE/dashboard.php?success=<strong>Welcome $dname!</strong>");
		exit;
	}



?>