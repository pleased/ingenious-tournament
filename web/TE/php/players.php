<?php
	include "connect.php";
	include "socket.php";

	function add_player_to_tournament($user_id, $player_name, $tournament_id) {

		//Get Player id from name
		$player_id = getPlayerId($player_name);


		//Check user_id is in tournament
		if (!isUserInTournament($user_id, $tournament_id)) {
			$tournament_name = getTournamentName($tournament_id);
			header("Location: "."/TE/my_tournaments.php?error=<strong>You have not joined $tournament_name</strong>");
		}

		//Get Tournament info
		$link = connect();
		$sql = "SELECT name, max_users, max_players, status FROM tournaments WHERE id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $tournament_id);
		$stmt->execute();
		$stmt->bind_result($tournament_name, $max_users, $max_players, $status);
		$stmt->fetch();

		//Check that there the user is allowed to submit more players for tournament
		$num_players = getNumUsersPlayers($tournament_id, $user_id);
		if ($num_players == $max_players) {
			header('Location: '.'/TE/view_tournament.php?id='.$tournament_id.'&error=<strong>You have already submitted the maximum number of players for this tournament. Please remove one should you wish to suubmit another player.</strong>');
			exit;
		}

		/*echo $player_name . "<br>";
		echo $player_id . "<br>";
		echo $tournament_id . "<br>";
		echo $user_id . "<br>";*/

		//Check that the player isnt already submitted.
		$link = connect();
		$sql = "SELECT user_id FROM tournament_players WHERE tournament_id = ? AND player_id = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ii", $tournament_id, $player_id);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		echo $num_rows;
		if ($num_rows >= 1) {
			header('Location: '.'/TE/view_tournament.php?id='.$tournament_id.'&error=<strong>The player you are trying to submit already exists in the tournament. Is this a problem ? Click here to report a problem.</strong>');
			exit();
		}
		close($link);

		//Check if tournament is running paused or terminated.

		if ($status == 1 || $status == 2) {
			$message = "Add_player $player_id $tournament_id\n";
			send_to_server($message);
		}

		$link = connect();
		$sql = "INSERT INTO tournament_players (tournament_id, player_id, user_id, rating, status, active_count, scheduled_count) VALUES (?,?,?,?,?,?,?)";
		$stmt = $link->prepare($sql);
		$rating = 0;
		$status = 0;
		$a_count = 0;
		$s_count = 0;
		$stmt->bind_param("iiiiiii", $tournament_id, $player_id, $user_id, $rating, $status, $a_count, $s_count);
		$res = $stmt->execute();
		if (!$res) {
			$error = "error=<strong>Addition of \"$player_name\" went wrong, please contact an administrator.</strong>";
			header("Location: /TE/view_tournament.php?id=$tournament_id&$error");
		} else {
			$success = "success=<strong>Addition of \"$player_name\" completed successfully</strong>";
			header('Location: '.'/TE/view_tournament.php?id='.$tournament_id.'&'.$success);
		}

	}

	function add_player($player_name, $user_id, $class_name) {

		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/player_uploads/";


		//Check that name is unique
		$sql = "SELECT name FROM players";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($names);
		while ($stmt->fetch()) {
			if ($names == $player_name) {
				header("Location: "."/TE/add_player.php?error=<strong>Player with that name exists already. Please choose a different name</strong>");
				exit();
			}
		}


		 $target_file = $target_dir . $player_name . ".jar";
		 $uploadOk = 1;
		 $FileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file is valid
		if(isset($_FILES["file"])) {
			if ($target_file == ($target_dir.".jar")) {
			    $error = "<strong>Please enter a name.</strong>";
				header('Location: '.'/TE/add_player.php?error='.$error);
				exit;
			} else if (file_exists($target_file)) {
			    $error = "<strong>Sorry, File already exists</strong>";
				header('Location: '.'/TE/add_player.php?error='.$error);
				exit;
			} else if ($_FILES["file"]["size"] > 5000000) {
			    $error = "<strong>Sorry, your file is too large</strong>";
				header('Location: '.'/TE/add_player.php?error='.$error);
				exit;
			}

		    if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		        $error = "<strong>Player could not be uploaded, please contact an adminstrator</strong>";
				header('Location: '.'/TE/add_player.php?error='.$error);
				exit;
			}
		} else {
			$error = "<strong>No file specified..</strong>";
			header('Location: '.'/TE/add_player.php?error='.$error);
			exit;
		}


			$sql = "INSERT INTO players (user_id, status, name, classname, dir) VALUES (?,?,?,?,?)";
			$link = connect();
			$stmt = $link->prepare($sql);
			$status = 0;
			$player_name = strip_tags($player_name);
			/* TODO : Add directory for player */
			$dir = $target_file;
			$stmt->bind_param("iisss", $user_id, $status, $player_name, $class_name, $dir);
			$res = $stmt->execute();

			if (!$res) {
				header("Location: "."/TE/add_player.php?error=<strong>Unable to add the player to the tournament. Please contact an administrator using the contact form.</strong>");
				exit();
			} else {

				header("Location: "."/TE/my_players.php?success=<strong>\"$player_name\" has been successfully added</strong>");
				exit();
			}
		}
	//}


	function delete_player_from_tournament($user_id, $player_id, $tournament_id) {

		$user_id = getUserId($_SESSION['user_email']);
		$owner_id = getPlayerOwnerId($player_id);
		$isAdmin = isUserAdmin($user_id, $tournament_id);

		if ($owner_id != $user_id) {
			if (!$isAdmin) {
				header("Location: /TE/my_tournaments.php?success=<strong>You are not an admin of the tournament</strong>");
				exit();
			}
			/* Not own player, but admin */
			$status = getTournamentStatus($tournament_id);

			//Get player name
			$player_name = getPlayerName($player_id);

			if ($status != 0) {
				$message = "Remove_player $player_id $tournament_id\n";
				if (!send_to_server($message)) {
					header("Location: "."/TE/manage_tournament.php?id=$tournament_id&error=<strong>\"$player_name\" could not be removed</strong>");
					exit();
				}
			}

			//Delete the player entry in the tournament players table
			$sql = "DELETE FROM tournament_players WHERE tournament_id = (?) AND player_id = (?)";
			$link = connect();
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ii", $tournament_id, $player_id);
			$res = $stmt->execute();
			if (!$res) {
				header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>\"$player_name\" could not be removed.. Please contact an administrator</strong>");
				exit();
			} else {
				header("Location: /TE/manage_tournament.php?id=$tournament_id&success=<strong>\"$player_name\" has been successfully removed</strong>");
				exit();
			}
		} else {
			$status = getTournamentStatus($tournament_id);

			//Get player name
			$player_name = getPlayerName($player_id);

			if ($status != 0) {
				$message = "Remove_player $player_id $tournament_id\n";
				if (!send_to_server($message)) {
					header("Location: "."/TE/view_tournament.php?id=$tournament_id&error=<strong>\"$player_name\" could not be removed</strong>");
					exit;
				}
			}

			//Delete the player entry in the tournament players table
			$sql = "DELETE FROM tournament_players WHERE tournament_id = (?) AND user_id = (?) AND player_id = (?)";
			$link = connect();
			$stmt = $link->prepare($sql);
			$stmt->bind_param("iii", $tournament_id, $user_id, $player_id);
			$res = $stmt->execute();
			if (!$res) {
				header("Location: "."/TE/view_tournament.php?id=$tournament_id&error=<strong>\"$player_name\" could not be removed.. Please contact an administrator</strong>");
				exit;
			} else {
				header("Location: "."/TE/view_tournament.php?id=$tournament_id&success=<strong>\"$player_name\" has been successfully removed</strong>");
				exit;
			}
		}
	}

	function delete_player($player_id, $user_id) {
		//Check the player exists
		$sql = "SELECT tournament_player_id FROM tournament_players WHERE user_id = ? AND player_id = ?";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ii", $user_id, $player_id);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;

		if ($num_rows != 0) {
			header("Location: /TE/my_players.php?error=<strong>The player is in a tournament, plaease remove the player from thier tournaments before you can delete them.</strong>");
			exit();
		}

		$player_name = getPlayerName($player_id);
		$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/TE/uploads/player_uploads/";
		$target_file = $target_dir . $player_name . ".jar";

		if (!unlink($target_file)) {
			header("Location: /TE/my_players.php?error=<strong>The player file could not be removed, please contact an administrator</strong>");
			exit();
		}

		//Delete the player entry in the tournament players table
		$sql = "DELETE FROM players WHERE user_id = (?) AND id = (?)";
		$link = connect();
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ii", $user_id, $player_id);
		$res = $stmt->execute();
		if (!$res) {
			header("Location: /TE/my_players.php?error=<strong>The player could not be removed, please contact an administrator</strong>");
			exit();
		} else {
			header("Location: /TE/my_players.php?success=<strong>\"$player_name\" has been successfully removed from the system</strong>");
			exit();
		}
	}
?>