<?php
	include("connect.php");
	include("errors.php");

	if (isset($player_id)) {
		$player_name = getPlayerName($player_id);
		$str = "<div class=\"row\">
		<div class=\"col-md-3\"></div>
		<div class=\"col-md-6\">
			<h2>$player_name Tournaments</h2>
		</div>
		<div class=\"col-md-3\"></div>
	</div>";

		if (!isset($_SESSION)) {
			session_start();
		}

		$id = getUserId($_SESSION['user_email']);
		$email = $_SESSION['user_email'];

		//Get tournaments the player is participating in.
		$link = connect();
		$sql = "SELECT tournament_id, active_count, scheduled_count, rating FROM tournament_players WHERE player_id = ? AND archived = 0";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("i", $player_id);
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$stmt->bind_result($tournament_id, $active_count, $scheduled_count, $rating);

		if ($num_rows == 0) {
			$error = "<div class=\"row\">
					<div class=\"col-md-3\"></div>
					<div class=\"col-md-6\">
						<strong>\"$player_name\" is not participating in any tournaments.</strong>
					</div>
					<div class=\"col-md-3\"></div>
				</div>";
			echo $str;
			echo $error;
			exit();
		}

		$str .= "<div class=\"row\">
			<div class=\"col-md-3\"></div>
			<div class=\"col-md-6\">
				<table class=\"tournament_table\">
					<tr>
						<th class=\"center_cell\">Tournament Name</th>
						<th class=\"center_cell\">Referee</th>
						<th class=\"center_cell\">Active Matches</th>
						<th class=\"center_cell\">Scheduled Matches</th>
						<th class=\"center_cell\">Rating</th>
					</tr>";

		while ($stmt->fetch()) {
			$tournament_name = getTournamentName($tournament_id);
			$engine_name = getTournamentEngineName($tournament_id);
			$active = "<strong style=\"color: red\">$active_count active</strong>";
			$scheduled = "<strong style=\"color: orange\">$scheduled_count scheduled</strong>";
			$str .= "<tr><td class=\"center_cell\">$tournament_name</td><td class=\"center_cell\">$engine_name</td><td class=\"center_cell\">$active</td><td class=\"center_cell\">$scheduled</td><td class=\"center_cell\">$rating</td></tr>";
		}

		$str .= "</table></div><div class=\"col-md-3\"></div>";
		echo $str;
	} else {
		$error = "<strong>No player specified. Please choose a player to view.</strong>";
		header('Location: '.'/TE/my_players.php?error='.$error);
	}

?>