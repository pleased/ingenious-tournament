<?php
	include "php/connect.php";
	include "php/sessions.php";
	include "php/errors.php";
	include "php/socket.php";

	//Get user id
	$user_id = getUserId($_SESSION['user_email']);

	//Check if user is admin
	$isAdmin = isUserAdmin($user_id, $tournament_id);

	if (!$isAdmin) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>You do not have sufficient rights to start the tournament.</strong>");
		exit();
	}

	//Check if tournament is already started
	$status = getTournamentStatus($tournament_id);

	$tournament_name = getTournamentName($tournament_id);

	/**
	* Get all logs ascociated with tournament
	**/
	$file_names = Array();
	$match_counter = 1;

	$link = connect();
	$sql = "SELECT id FROM matches WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->bind_result($match_id);

	while ($stmt->fetch()) {
		$link1 = connect();
		$sql1 = "SELECT log_dir FROM match_log_files WHERE match_id = ?";
		$stmt1 = $link1->prepare($sql1);
		$stmt1->bind_param("i", $match_id);
		$stmt1->execute();
		$stmt1->bind_result($log_dir);
		while ($stmt1->fetch()) {
			$archive_log_dir = "/var/www/html/TE/archives/Tournaments/$tournament_name/matches/$match_counter/";
			create_dirs($archive_log_dir);
			copy($log_dir, $archive_log_dir . basename($log_dir));
		}
		$match_counter++;
	}

	//Archive players

	$link = connect();
	$sql = "SELECT player_id, rating, user_id FROM tournament_players WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->bind_result($player_id, $rating, $owner_id);

	$score_array = Array();

	while ($stmt->fetch()) {
		$player_name = getPlayerName($player_id);
		$ownerName = getDisplayName($owner_id);
		$player_file = getPlayerFile($player_id);
		$player_classname = getPlayerClassname($player_id);

		$row_array['player_id'] = $player_id;
		$row_array['owner_id'] = $owner_id;
		$row_array['owner_name'] = $ownerName;
		$row_array['player_name'] = $player_name;
		$row_array['player_rating'] = $rating;
		$row_array['classname'] = $player_classname;


		$file = "/var/www/html/TE/archives/Tournaments/$tournament_name/Players/$ownerName" . "_" . "$player_name.jar";
		echo $player_file . "<br>" .  $file . "<br>";
		create_dirs(dirname($file));
		array_push($score_array, $row_array);
		copy($player_file, $file);

	}
	create_dirs("/var/www/html/TE/archives/Tournaments/$tournament_name/config/");
	$fp = fopen("/var/www/html/TE/archives/Tournaments/$tournament_name/player_scores.json", 'w');
	fwrite($fp, json_encode($score_array));
	fclose($fp);


	//archives json config for tournament
	copy("/var/www/html/TE/uploads/config_uploads/tournament_".$tournament_id . ".json", "/var/www/html/TE/archives/config/tournament_".$tournament_id . ".json");


	// Set archived = 1 on all relevant tables
	//Tournament

	$link = connect();
	$sql = "UPDATE tournaments SET archived = 1 WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	if ($stmt->execute()) {
		//ERROR
	}


	/**
		Delete tournament users from database
	*/
	$link = connect();
	$sql = "UPDATE tournament_users SET archived = 1 WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The tournament could not be deleted..</strong>");
		exit();
	}

	//Matches

	$sql = "UPDATE matches
			JOIN match_log_files ON matches.id = match_log_files.match_id
			JOIN match_result ON matches.id = match_result.match_id
			SET matches.archived = 1, match_log_files.archived = 1, match_result.archived = 1
			WHERE matches.tournament_id = ?";

	$link = connect();
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The tournament could not be deleted..</strong>");
		exit();
	}

	/**
		Delete tournament players from database
	*/
	$link = connect();
	$sql = "UPDATE tournament_players SET archived = 1 WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/manage_tournament.php?id=$tournament_id&error=<strong>The tournament could not be deleted..</strong>");
		exit();
	}

	header("Location: /TE/my_tournaments.php?success=<strong>The tournament has been archived successfully</strong>");
		exit();


	function create_dirs($dir) {
		$dirs = explode("/", $dir);
		$folder = "";
		foreach ($dirs as $key => $val) {
			$folder .= $val . "/";
			if (!file_exists($folder)) {
				mkdir($folder);
			}
		}
	}
?>