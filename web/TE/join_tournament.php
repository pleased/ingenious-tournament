<?php

	include_once("php/connect.php");
    include "php/sessions.php";


	if (isset($_GET)) {
		if (isset($_GET['id'])) {
			$tournament_id = $_GET['id'];
		} else {
			header('Location: '.'/TE/public_tournaments.php');
		}
	}

	//Get user id
	$user_id = getUserId($_SESSION['user_email']);

	//Get Number of players in tournament
	$link = connect();
	$sql = "SELECT * FROM tournament_users WHERE tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->store_result();
	$num_players = $stmt->num_rows;

	//Check if user is already in tournament.
	$link = connect();
	$sql = "SELECT tournament_id FROM tournament_users WHERE user_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $user_id);
	$stmt->execute();
	$stmt->bind_result($ids);
	while ($stmt->fetch()) {
		if ($ids == $tournament_id) {
			header('Location: '.'/TE/public_tournaments.php?error=<strong>You are already in the tournament. <br> Please join another tournament</strong>');
			exit();
		}
	}
	//Check if the user created the tournament
	$link = connect();
	$sql = "SELECT user_id FROM tournaments WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->bind_result($ids);
	$stmt->fetch();

	if ($ids == $user_id) {
		header('Location: '.'/TE/public_tournaments.php?error=<strong>You are already in the tournament. <br> Please join another tournament</strong>');
		exit();
	}

	//Get Number of players in tournament
	$link = connect();
	$sql = "SELECT max_users FROM tournaments WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->bind_result($max_users);
	$stmt->fetch();


	if ($num_players >= $max_users) {
		$error = "<strong>The tournament is full</strong>";
		header('Location: '.'/TE/public_tournaments.php?error='.$error);
	} else {
		//Insert the player into tournament
		$link = connect();
		$sql = "INSERT INTO tournament_users (tournament_id, user_id, admin) VALUES (?, ?, ?)";
		$stmt = $link->prepare($sql);
		$admin = 0;
		$stmt->bind_param("iii", $tournament_id, $user_id, $admin);
		if ($stmt->execute()) {
			$success = "<strong>You successfully joined the tournament</strong>";
			header('Location: '.'/TE/public_tournaments.php?success='.$success);
		} else {
			$error = "<strong>You could not be added to the tournament</strong>";
			header('Location: '.'/TE/public_tournaments.php?error='.$error);
		}
	}

?>