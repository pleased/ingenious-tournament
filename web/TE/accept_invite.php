<?php
	include "php/connect.php";
	include "php/sessions.php";

	if (isset($_GET)) {
		if (isset($_GET['invite_id'])) {
			$invite_id = $_GET['invite_id'];
		} else {
			header('Location: /TE/my_invitations.php?error=<strong>Could not accept the invite, since no invite has been selected.</strong>');
		}

	} else {
		header('Location: /TE/my_tournaments.php?error=<strong>Could not accept the invite since no invite was selected.</strong>');
	}

	$user_id = getUserId($_SESSION['user_email']);

	$link = connect();
	$sql = "SELECT tournament_id, status FROM tournament_invitations WHERE id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $invite_id);
	$stmt->execute();
	$stmt->bind_result($tournament_id, $status);
	$stmt->fetch();

	//Get Current number of players
	$num_users = getNumTournamentUsers($tournament_id);
	$max_users = getMaxTournamentUsers($tournament_id);
	if ($num_users == $max_users) {
		header("Location: /TE/my_invitations.php?error=<strong>The tournament you are trying to join is already at maximum capacity</strong>");
	}

	//Check that the user isnt already in the tournament.
	if (isUserInTournament($user_id, $tournament_id)) {
		header("Location: /TE/my_invitations.php?error=<strong>You are already in the tournament you trying to join.</strong>");
		exit();
	}

	//Check status of invitation.
	if ($status != 0) {
		header("Location: /TE/my_invitations.php?error=<strong>Your invitation has already been used.</strong>");
		exit();
	}

	//Maybe more check ?

	$tournament_name = getTournamentName($tournament_id);

	$link = connect();
	$sql = "UPDATE tournament_invitations SET status= ? WHERE id = ?";
	$status = 1;
	$stmt = $link->prepare($sql);
	$stmt->bind_param("ii", $status, $invite_id);
	$res = $stmt->execute();

	if (!$res) {
		header("Location: /TE/my_invitations.php?id=$tournament_id&error=<strong>Your invitation could not be accepted. Please contact an administrator.</strong>");
		exit();
	} else {
		$link = connect();
		$sql = "INSERT INTO tournament_users (tournament_id, user_id, admin) VALUES (?, ?, ?)";
		$stmt = $link->prepare($sql);
		$admin = 0;
		$stmt->bind_param("iii", $tournament_id, $user_id, $admin);
		if ($stmt->execute()) {
			$success = "<strong>You successfully accepted the invitation and joined the tournament</strong>";
			header('Location: '.'/TE/my_invitations.php?success='.$success);
		} else {
			$error = "<strong>You could not be added to the tournament</strong>";
			header('Location: '.'/TE/public_tournaments.php?error='.$error);
		}
	}


?>