<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>My Tournaments</title>
		<link href="css/default.css" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- jQuery library -->
		<script src="http://code.jquery.com/jquery-3.2.1.min.js"
  				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  				crossorigin="anonymous">
  		</script>
		<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  				integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  				crossorigin="anonymous">
		</script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script src="js/pagination.js"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li ><a href="dashboard.php">Home <span class="sr-only">(	current)</span></a></li>
								<li class="dropdown active">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							    <li><a href="engines.php">Referees</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>

		<?php
			include "php/connect.php";
            include "php/errors.php";

            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }
        ?>
		<div id="my_tournaments"></div>
		<br>
		<div id="upcoming_matches"></div>
		<div id="ongoing_matches"></div>
		<div id="latest_results"></div>


		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde.</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
	<script src="js/control.js"></script>
</html>
