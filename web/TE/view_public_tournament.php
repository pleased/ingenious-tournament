<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Public Tournament Information</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/default.css" rel="stylesheet">
		<link rel="stylesheet" href="css/w3.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="login.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="active"><a href="tournaments.php">Tournaments</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Login</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>

		<?php
			include("php/connect.php");
			$tournament_id = $_GET['id'];
			$tournament_name = getTournamentName($tournament_id);
			$str = "<div class=\"row\">
					<div class=\"col-md-3\"></div>
					<div class=\"col-md-6\">
						<h2>$tournament_name Information</h2>
					</div>
					<div class=\"col-md-3\"></div>
				</div>";


			$link = connect();
			$sql = "SELECT status, engine_id, scheduler_id, ranker_id, resources_id, max_users FROM tournaments WHERE private = ? AND archived = 0 AND id = ?";
			$private = 0;
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ii", $private, $tournament_id);
			$stmt->execute();
			$stmt->bind_result($status, $engine_id, $scheduler_id, $ranker_id, $resources_id, $max_users);
			$stmt->fetch();

			$engine_name = getEngineName($engine_id);
			$scheduler_name = getSchedulerName($scheduler_id);
			$ranker_name = getRankerName($ranker_id);
			$resources_name = getResourcesName($resources_id);
			$num_users = getNumTournamentUsers($tournament_id);



			if ($status == 0) {
				$status = "<strong style=\"color:red\">Not Started</strong>";
			} else if ($status == 1) {
				$status = "<strong style=\"color:green\">Running</strong>";
			} else if($status == 2) {
				$status = "<strong style=\"color:orange\">Paused</strong>";
			} else if ($status == 3) {
				$status = "<strong style=\"color:green\">Completed!</strong>";
			} else {
				$status = "ERROR";
			}

			if ($private == 0) {
				$private = "<strong style=\"color:green\">Public Tournament</strong>";
			} else {
				$private = "<strong style=\"color:red\">Private Tournament</strong>";
			}

			$str .= "<div class=\"row\">
					<div class=\"col-md-3\"></div>
					<div class=\"col-md-6\">
					<table class=\"tournament_info\">
					<tr>
					<th>Tournament Name</th>
					<td class=\"text-center\">$tournament_name</td>
					</tr>
					<tr>
					<th>Referee</th>
					<td class=\"text-center\">$engine_name</td>
					</tr>
					<tr>
					<th>Scheduler</th>
					<td class=\"text-center\">$scheduler_name</td>
					</tr>
					<tr>
					<th>Ranker</th>
					<td class=\"text-center\">$ranker_name</td>
					</tr>
					<tr>
					<th>Users</th>
					<td class=\"text-center\">$num_users / $max_users</td>
					</tr>
					<tr>
					<th>Resources</th>
					<td class=\"text-center\">$resources_name</td>
					</tr>
					<tr>
					<th>Status</th>
					<td class=\"text-center\">$status</td>
					</tr>
					<tr>
					<th>Tournament Type</th>
					<td class=\"text-center\">$private</td>
					</tr>
					</table>
					</div>
					<div class=\"col-md-3\"></div>
				</div>";


			echo $str;

		?>

	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
        <script src="js/control.js"></script>
	</footer>
</html>
