<?php

	include_once("php/connect.php");
	include "php/sessions.php";


	if (isset($_GET)) {
		if (isset($_GET['id'])) {
			$tournament_id = $_GET['id'];
		} else {
			header('Location: '.'/TE/my_tournaments.php');
		}
	}

	//Get user id
	$user_id = getUserId($_SESSION['user_email']);

	//get users players to check if they are busy
	$sql = "SELECT status, player_id FROM tournament_players WHERE user_id = ?";
	$link = connect();
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $user_id);
	$stmt->execute();
	$stmt->bind_result($status, $player_id);

	while ($stmt->fetch()) {
		if ($status == 1) {
			$player_name = getPlayerName($player_id);
			$error = "<strong>You can not leave the tournament because your player \"$player_name\" is still in a match!</strong>";
			header('Location: '.'/TE/my_tournaments.php?error='.$error);
			exit();
		}
	}

	//Number of admins in the tournament
	$link = connect();
	$sql = "SELECT COUNT(tournament_id) FROM tournament_users WHERE tournament_id = ? AND admin = 1";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("i", $tournament_id);
	$stmt->execute();
	$stmt->bind_result($num_users);
	$stmt->fetch();

	//Check creator
	$link = connect();
	$sql = "SELECT user_id FROM tournaments WHERE id = ?";
	$stmt1 = $link->prepare($sql);
	$stmt1->bind_param("i", $tournament_id);
	$stmt1->execute();
	$stmt1->bind_result($admin_id);
	$stmt1->fetch();

	if ($admin_id == $user_id || $num_users == 0) {
		$error = "<strong>You can not leave the tournament because you are the last admin in the tournament.<br>Please delete the tournament should you wish to</strong>";
			header('Location: '.'/TE/my_tournaments.php?error='.$error);
			exit();
	}


	//Remove user from tournament
	$link = connect();
	$sql = "DELETE FROM tournament_users WHERE user_id = ? AND tournament_id = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param("ii", $user_id, $tournament_id);
	if ($stmt->execute()) {
		//Deactivate the players
		$link = connect();
		$sql = "UPDATE tournament_players SET status= ? WHERE user_id = ? AND tournament_id = ?";
		$stmt = $link->prepare($sql);
		$status = 2;
		$stmt->bind_param("iii", $status, $user_id, $tournament_id);
		if ($stmt->execute()) {
			$tournament_name = getTournamentName($tournament_id);
			$success = "<strong>Successfully left \"$tournament_name\"</strong>";
			header('Location: '.'/TE/my_tournaments.php?success='.$success);
			exit();
		} else {
			$error = "<strong>Unable to leave the tournament..</strong>";
			header('Location: '.'/TE/my_tournaments.php?error='.$error);
			exit();
		}
	} else {
		$error = "<strong>Unable to leave the tournament..</strong>";
			header('Location: '.'/TE/my_tournaments.php?error='.$error);
			exit();
	}

?>