<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Add Scheduler</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/default.css" rel="stylesheet">
		<link rel="stylesheet" href="css/w3.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li ><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>

							    <li><a href="engines.php">Referees</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li><a href="rankers.php">Rankers</a></li>
								<li class="active"><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
		<?php
			include "php/connect.php";
            include "php/errors.php";

            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }
        ?>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 login-wrapper">
				<b id="sign-up-header">Add Scheduler</b>
				<form action="/index.php/add_scheduler" method="post" enctype="multipart/form-data">
					<br>
					<?php
						//Get user id
						$id = getUserId($_SESSION['user_email']);
						echo "<input class=\"form-control\" type=\"hidden\" name=\"id\" value=\"$id\">";
					?>
					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Scheduler Name:</div><div class="col-md-6"> <input class="form-control" type="name" name="name"></div>
					</div>

					<div class="row">
						<div class="col-md-1"></div><div class="input-label col-md-4 form-group">Scheduler File:</div><div class="col-md-6"> <input class="form-control" type="file" name="file"></div>
					</div>

					<br>

  					<div class="row">
  					<div class="col-md-5"></div>
  					<div class="col-md-4">
  						<input type="submit" class="btn btn-primary button-overwrite" value="Add Scheduler">
  					</div>
  					<br><br>
  					<hr class="seperator">
  					<br><br>
				</form>
				<br>
			</div>
		</div>

		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>
