<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>My Rankers</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/default.css" rel="stylesheet">
		<link rel="stylesheet" href="css/w3.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
	</head>

	<body class="container">
		<div class="row">
			<div class="col-md-2 col-md-push-1">
				<!--<img class="logo-image" src="logo.png">-->
			</div>
			<div class="col-md-9">
				<h1 class="header-text">Tournament Engine for the Ingenious Framework</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<nav class="navbar">
					<div class="navbar-settings">
						<div class="navbar-header ">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="dashboard.php">Home <span class="sr-only">(current)</span></a></li>
								<li class="dropdown">
							        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tournaments
							        <span class="caret"></span></a>
							        <ul class="dropdown-menu">
							          <li><a href="create_tournament.php">Create Tournament</a></li>
							          <li><a href="my_tournaments.php">My Tournaments</a></li>
							          <li><a href="public_tournaments.php">Public Tournaments</a></li>
							          <li><a href="my_invitations.php">My Tournament Invitations</a></li>
							          <li><a href="archived_tournaments.php">Archived Tournaments</a></li>
							        </ul>
							      </li>
							    <li><a href="engines.php">Referees</a></li>
							    <li><a href="my_players.php">My Players</a></li>
								<li class="active"><a href="rankers.php">Rankers</a></li>
								<li><a href="schedulers.php">Schedulers</a></li>
								<li><a href="rankings.php">Rankings</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
						        <li><a href="/index.php/logout/">Logout</a></li>
						      </ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
		<?php
			include "php/connect.php";
            include "php/errors.php";

            if (isset($error)) {
            	echo $error;
            } else if (isset($success)) {
            	echo $success;
            }
        ?>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h2>My Rankers</h2>
			</div>
			<div class="col-md-3"></div>
		</div>
		<?php
			//Get id of user logged in.
			$user_id = getUserId($_SESSION['user_email']);

			$link = connect();
			$sql = "SELECT id, name FROM rankers WHERE user_id = ?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("i", $user_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows();
			$stmt->bind_result($ranker_id, $ranker_name);


			if ($num_rows == 0) {
				$str = "<div class=\"row\">
						<div class=\"col-md-3\"></div>
						<div class=\"col-md-6\">
							<strong>You currently have no rankers, add a ranker using the button below.</strong>
						</div>
						<div class=\"col-md-3\"></div>
					</div>";
				echo $str;
			} else {
				$str = "<div class=\"row\">
				<div class=\"col-md-3\"></div>
				<div class=\"col-md-6\">
					<table class=\"tournament_table\">
						<tr>
							<th>Ranker Name</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>";

				while ($stmt->fetch()) {
					$str .= "<tr><td>". $ranker_name . "</td>";
					$str .= "<td><a class=\"btn btn-warning btn-full\" href=\"edit_ranker.php?id=$ranker_id\">Edit</a></td>";
					$form1 = "<form method=\"POST\" action=\"/index.php/delete_ranker\">
							<input type=\"hidden\" value=$ranker_id name=\"ranker_id\">
							<input type=\"hidden\" value=$user_id name=\"user_id\">
							<input type=\"submit\" class=\"btn btn-danger btn-full\" value=\"Delete Ranker\">
							</form>";
					$str .= "<td>$form1</td>
						</tr>";
				}
				$str .= "</table>";
				echo $str;
			}

			echo "<br><br>
				<a class=\"btn btn-primary btn-overwrite\" href=\"add_ranker.php\">Add Ranker</a>
				</div>
				<div class=\"col-md-3\"></div>
			</div>";

		?>

		<div class="row"><div class="col-md-3 col-md-offset-12"><br></div></div>
	</body>
	<footer>
        <div class="row footer-wrapper">
        	<div class="col-md-3 col-md-offset-12"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-2">Copyright 2017 Henry Linde.</div>
        	<!--<div class="col-md-1"><img class="logo-image-small" src="logo.png"></div>-->
        	<div class="col-md-3">Tournament Engine</div>
        	<br>
        </div>
	</footer>
</html>
