To start of the Tournament Engine requires docker ce. 
Please ensure that running docker --version yields the following version:
Docker version 17.09.0-ce

Firstly, the system image needs to be built.

To build the dockerfile in the root directory with
"docker build -t web_te ."
Sudo rights might be required.


Once completed the Tournament Engine is ready for use
Start the engine by running ./start_te.sh

Once it says successfully started Tournament Engine you can access the website through localhost:80

The installation comes with default schedulers and rankers located at their respective folders shown below

Schedulers:		- 2 Player Round Robin Scheduler 	/engine/TE/2PlayerRoundRobin.jar
				- 1 player Round Robin Scheduler 	/engine/TE/1PlayerRoundRobin.jar
				- Knockout Scheduler 			 	/engine/TE/KnockoutScheduler.jar
Rankers: 		- Elo Ranker 					 	/engine/TE/EloRanker.jar
		  		- Score Ranker 					 	/engine/TE/ScoreRanker.jar


Default Referees will also be available to use pre-installed on the system.
The following referees are available
Domineering		-	/engine/TE/Domineering.jar
MNK				-	/engine/TE/MNK.jar
LOA				-	/engine/TE/LOA.jar
TicTacToe		-	/engine/TE/TicTacToe.jar

To implement your own referee visit https://bitbucket.org/skroon/ingenious-framework and clone the repo and further instructions will be present to show how to implelement a new game.


******************************************** SCHEDULER ***********************************************
To implement your own scheduler extend the Scheduler class located in /engine/TE/src/main/java/components/Scheduler.java

Once the scheduler starts the participating players are read into the players HashMap containing the players ID's as the key and value being their rating in the tournament. The scheduler then calls generateMatches() which you as the developer need to implement. This method is the starting point for generating matches and any matches that are generated need to be added to the matches variable. The matches variable is an ArrayList<Match>. The Match is represented by an ArrayList<Integer> of player ID's in the match.

It is important to note that generateMatches gets called automatically on a few occations.
Once the scheduler is started and should a player be added to the tournament.

Default methods and data structures are provided to do the communication with the server. API has been implemented for schedulers detailed below

Scheduler Data:
players - HashMap<ID,rating>
matches - ArrayList<ArrayList<ID>>
num_players - int

Scheduler API:
sendOutstandingMatches() -Sends the newly generated matches to the tournament.
getWinner(String result) - Takes a result received from the tournament and returns the winner ID
getPlayerInMatch() - Takes the result received from the tournament and returns the participating player ids in an ArrayList

*******************************************************************************************

************************************************ RANKER **********************************************
To implement your own ranker extend the Ranker class located in /engine/TE/src/main/java/components/Ranker.java

The ranker constructor takes two parameters. The first is the port for communication which you need not worry about. The second is the initial rating players will receieve. The default initial rating is 0.

Once the ranker is started the sendAllRatings method is called. This sends the initialized ratings of all participating players to the tournament. Once a new result is received from the tournament, the result is parsed into a HashMap<Integer, Double>. The key value is the player ID and the value is the score achieved in the match. The calculateNewRatings is then called with this data as a parameter for calculating the new ratings of the players in the match.

Default methods and data structures are provided to do the communication with the server. API has been implemented for rankers detailed below

Ranker Data:
players - HashMap<Integer, Double>. The key is the player ID and the value is the player rating.

Ranker API:
setInitialrating(double rating) - This is used to set the initial rating of players. (Important to note that setting the initial rating through the constructor will ensure the initial players in the tournament are initialized to the rating. Setting te initial rating using the provided method ensures that new players that are added will be initialized to the rating)
sendAllRatings - This sends all the participating players ratings to the tournament.
sendRating(playerID, playerRating) - this sends an individual player rating to the tournament.
setPlayerRating(playerID, playerRating) - This sets the specific players rating.

*******************************************************************************************