package src.main.java.constants;

public class Database {
	
	public static final int DB_PORT = 3306;
	public static final String DB_USER = "cs-games";
	public static final String DB_PASS = "cs-games1234";
	public static final String DB_NAME = "TE";
	public static final String DB_URI = "jdbc:mysql://localhost:3306/";
	public static final String DB_URL = DB_URI + DB_NAME + "?user=" + DB_USER + "&password=" + DB_PASS;
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; 
}
