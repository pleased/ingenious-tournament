package src.main.java.constants;

public class Tournament {
	
	//Tournament Logs
	public static final String TOURNAMENT_LOG_DIR = System.getProperty("user.home") + "/TEResources/Logs/Engine/";
	public static final String MEDIATOR_LOG_DIR = System.getProperty("user.home") + "/TEResources/Logs/Engine/Mediators/";
	public static final String MEDIATOR_LOG_NAME = "Mediator_";
	
	public static String getMediatorLogName(int id) {
		return MEDIATOR_LOG_NAME + id + ".log";
	}
	
	public static String getTournamentLogName(int id) {
		return "Tournament_" + id + ".log"; 
	}
	
	//Tournament status constants
	public static final int TOURNAMENT_STOPPED = 0;
	public static final int TOURNAMENT_RUNNING = 1;
	public static final int TOURNAMENT_PAUSED = 2;
	public static final int TOURNAMENT_DONE = 3;
	public static final int TOURNAMENT_COMPLETE = 4;
	
	//Tournament error strikes allowed
	public static final int MATCH_STRIKES_ALLOWED = 3;
	public static final int ENGINE_STRIKES_ALLOWED = 3;
	public static final int PLAYER_STRIKES_ALLOWED = 1;
	
	//Tournament max constants
	public static final int MAX_RUNNING_MATCHES = 1;
	
	//Tournament match status constants
	public static final int MATCH_SCHEDULED = 0;
	public static final int MATCH_RUNNING = 1;
	public static final int MATCH_COMPLETE = 2;
	
	//Tournament player status constants
	public static final int PLAYER_IDLE = 0;
	
	
}
