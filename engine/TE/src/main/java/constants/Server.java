package src.main.java.constants;

public class Server {
	public static final int SERVER_SLEEP_DURATION = 500;
	public static final String ENGINE_LOG_DIR = System.getProperty("user.home") + "/TEResources/Logs/Engine/";
	public static final String ENGINE_LOG_NAME = "TournamentEngineServer.log";
}
