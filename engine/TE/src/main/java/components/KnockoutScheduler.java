package src.main.java.components;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class KnockoutScheduler extends Scheduler {
	
	public int roundNr;
	public ArrayList<Integer> byeRound;
	public HashMap<Integer, Double> roundPlayers;
	public int matchCount;
	
	public KnockoutScheduler(String _port, int num_players) throws UnknownHostException, IOException {
		super(_port, num_players);
		roundNr = 0;
		byeRound = new ArrayList<>(num_players-1);
		roundPlayers = new HashMap<>();
		matchCount = 0;
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		KnockoutScheduler ks = new KnockoutScheduler(args[0], 2);
	}

	@Override
	public void generateMatches() {
		HashMap<Integer, Double> hs;
		int playerid;
		ArrayList<Integer> a;
		Random r;
		matchCount = 0;
		if (roundNr == 0) {
			a = new ArrayList<>();
			hs = new HashMap<Integer, Double>(players);
			r = new Random();
			roundPlayers = new HashMap<>(players);
			byeRound = new ArrayList<>(num_players-1);
		} else {
			a = new ArrayList<>();
			for (Integer i : byeRound) {
				roundPlayers.put(i, 0.0);
			}
			byeRound = new ArrayList<>();
			hs = new HashMap<Integer, Double>(roundPlayers);
			r = new Random();
		}
		
		if (roundPlayers.size() < num_players) {
			sendTerminatedMessage();
		}
		
		while ((hs.size() % num_players) != 0) {
			playerid = getRandomPlayer(hs, r);
			hs.remove(playerid);
			byeRound.add(playerid);
		}
		System.out.println((hs.size()) % num_players);
		System.out.println((hs.size()));
		System.out.println((byeRound.size()));
		System.out.println("===========Bye players==============");
		for (Integer x : byeRound) {
			System.out.println(x);
		}
		System.out.println("====================================");
		System.out.println("==============Matches==================");
		while ((hs.size() % num_players) == 0) {
			a = new ArrayList<>();
			for (int i = 0; i < num_players; i++) {
				playerid = getRandomPlayer(hs, r);
				hs.remove(playerid);
				a.add(playerid);
			}
			for (Integer x : a) {
				System.out.print(x + " vs ");
			}
			System.out.println();
			matches.add(a);	
			matchCount++;
			
			if (hs.size() == 0) {
				break;
			}
		}
		sendOutstandingMatches();
	}
	
	public static int getRandomPlayer(HashMap<Integer, Double> players, Random r) {
		Object[] b = players.keySet().toArray();
		return (int) b[r.nextInt(b.length)];
	}

	@Override
	public void new_match_result(String res) {
		//Get winner and loser, and remove loser from players hashmap
		int id = getWinner(res);
		ArrayList<Integer> e = new ArrayList<>();
		e = getPlayersInMatch(res);
		if (id == -1) {
			matches.add(e);
			sendOutstandingMatches();
		} else {
			for (Integer i : e) {
				if (i != id) {
					roundPlayers.remove(i);
				}
			}
			matchCount--;
			if (matchCount == 0) {
				roundNr++;
				generateMatches();
			}
		}
	}
	
}