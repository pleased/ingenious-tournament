package src.main.java.components;
import src.main.java.database.DatabaseManager;

public class Player {
	private int id;
	private int rating;
	private String dir;
	private int tournament_id;
	
	public Player(int id, int tournament_id) {
		this.id = id;
		this.tournament_id = tournament_id;
		this.rating = DatabaseManager.getPlayerRating(this.id, this.tournament_id);
		this.dir = DatabaseManager.getPlayerDir(this.id);
	}
	
	public int getID() {
		return this.id;
	}
	
	public int getRating() {
		return this.rating;
	}
	
}
