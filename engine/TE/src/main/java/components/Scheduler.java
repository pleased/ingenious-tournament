package src.main.java.components;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class Scheduler implements Runnable {
	
	protected ObjectOutputStream os;
	protected ObjectInputStream is;
	protected HashMap<Integer, Double> players = new HashMap<>();
	protected HashMap<Integer, Double> deactivatedPlayers = new HashMap<>();
	protected ArrayList<ArrayList<Integer>> matches = new ArrayList<ArrayList<Integer>>();
	protected ArrayList<ArrayList<Integer>> matches_sent = new ArrayList<ArrayList<Integer>>();
	protected int num_players;
	
	
	public Scheduler(String _port, int num_players) throws UnknownHostException, IOException {
		System.out.println("Scheduler started..");
		
		int port = Integer.parseInt(_port);
		Socket socket;
		socket = new Socket("localhost", port);
		os = new ObjectOutputStream(socket.getOutputStream());
		is = new ObjectInputStream(socket.getInputStream());
		this.num_players = num_players;
		//this.run();
		
	}
	
	/**
	 * Initializes the scheduler with the players in the tournament
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void init_scheduler() throws ClassNotFoundException, IOException {
		ArrayList<Integer> init_players;
		init_players = (ArrayList<Integer>)is.readObject();
		int j = 0;
		for (int i : init_players) {
			players.put(i, 0.0);
			j++;
		}
	}
	
	/**
	 * Reads the next command from the tournament server socket.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected String getCommand() throws ClassNotFoundException, IOException {
		return (String)is.readObject();
	}
	
	/**
	 * Read and integer ID from the socket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected int readID() throws ClassNotFoundException, IOException {
		return (int)is.readObject();
	}
	
	/**
	 * Reads the id from the socket and adds/reactivates the player
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void add_player(int id) throws ClassNotFoundException, IOException {
		System.out.println(id + " just got added to the tournament");
		
		if (deactivatedPlayers.containsKey(id)) {
			System.out.println("Contains!!!");
			players.put(id, deactivatedPlayers.get(id));
			return;
		}
		
		if (!players.containsKey(id)) {
			players.put(id, 0.0);
		}
	}
	
	/**
	 * Reads the id from the socket and deactivates the player
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void remove_player() throws ClassNotFoundException, IOException {
		int id = readID();
		if (players.containsKey(id)) {
			double rating = players.get(id);
			deactivatedPlayers.put(id, rating);
			players.remove(id);
		} else {
			deactivatedPlayers.put(id, 0.0);
		}
	}
	
	/**
	 * Sends all the matches stored in Scheduler.matches
	 */
	protected void sendOutstandingMatches() {
		System.out.println(matches.size() + " matches generated");
		while (matches.size() != 0) {
			System.out.println("Writing next match");
			try {
				if (!matches_sent.contains(matches.get(0))) {
					os.writeObject("next_match");
					matches_sent.add(matches.get(0));
					os.flush();
					os.writeObject(matches.remove(0));
					os.flush();
				} else {
					matches.remove(0);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Done writing match");
			System.out.println("Matches remaining in q : " + matches.size());
		}
	}
	
	protected void sendTerminatedMessage() {
		try {
			os.writeObject("done");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Takes the result returned from the mediator and determines the winner
	 * Only works with multiplayer matches
	 * @param result
	 * @return Id of winner OR -1 if no winner
	 */
	protected int getWinner(String res) {
		String[] split = res.split(",");
		int player_id;
		double score;
		int winner_id = -1;
		double max_score = Double.MIN_VALUE;
		boolean draw = false;
		
		for (String s : split) {
			String[] temp = s.split(" ");
			player_id = Integer.parseInt(temp[0]);
			score = Double.parseDouble(temp[1]);
			if (score > max_score) {
				max_score = score;
				winner_id = player_id;
			} else if (score == max_score) {
				draw = true;
			}
		}
		
		if (draw) {
			return -1;
		}
		
		return winner_id;
	}
	
	/**
	 * Returns a list of player ids that participated in the match result
	 * @param match result
	 * @return list of player ids
	 */
	protected ArrayList<Integer> getPlayersInMatch(String res) {
		ArrayList<Integer> a = new ArrayList<>();
		String[] split = res.split(",");
		int player_id;
		for (String s : split) {
			String[] ss = s.split(" ");
			player_id = Integer.parseInt(ss[0]);
			a.add(player_id);
		}
		
		return a;
		
	}
	
	
	/**
	 * 
	 * 
	 */
	public abstract void generateMatches();
	
	/**
	 * 
	 * @param result
	 */
	public abstract void new_match_result(String res);
	
	
	@Override
	public void run() {
		try {
			String s;
			boolean running = true;
			while (running) {
				s = getCommand();
				System.out.println(s);
				switch (s) {
				case "init" : {
					init_scheduler();
					generateMatches();
					break;
				}
				case "add_player" : {
					int id = readID();
					add_player(id);
					generateMatches();
					break;
				}
				case "remove_player" : {
					remove_player();
					break;
				}
				case "new_result" : {
					String res = (String)is.readObject();
					new_match_result(res);
					break;
				}
				default : {
					break;
				}
			}

			}
		} catch (ClassNotFoundException | IOException e ) {
			e.printStackTrace();
		}
	}
}
