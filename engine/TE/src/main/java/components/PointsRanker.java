package src.main.java.components;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;

public class PointsRanker extends Ranker {

	public PointsRanker(String _port) throws UnknownHostException, IOException {
		super(_port, 0);
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		PointsRanker ps = new PointsRanker(args[0]);
	}

	@Override
	public void calculateNewRatings(HashMap<Integer, Double> a) {
		for (Integer i : a.keySet()) {
			players.replace(i, players.get(i)+a.get(i));
		}
	}

}
