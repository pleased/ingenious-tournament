package src.main.java.components;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class Ranker implements Runnable {
	
	protected ObjectOutputStream os;
	protected ObjectInputStream is;
	protected HashMap<Integer, Double> players = new HashMap<>();
	protected HashMap<Integer, Double> deactivatedPlayers = new HashMap<>();
	protected double INITIAL_RATING = 0;
	
	
	public Ranker(String _port, double initial_rating) throws UnknownHostException, IOException {
		System.out.println("Ranker started..");
		
		int port = Integer.parseInt(_port);
		setInitialRating(initial_rating);
		System.out.println(port);
		Socket socket;
		socket = new Socket("localhost", port);
		os = new ObjectOutputStream(socket.getOutputStream());
		is = new ObjectInputStream(socket.getInputStream());
		
		this.run();
		
	}
	
	/**
	 * Initializes the ranker with the players in the tournament and sets their ratings according to the INITAL_RATINGS
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void init_ranker() throws ClassNotFoundException, IOException {
		ArrayList<Integer> init_players;
		init_players = (ArrayList<Integer>)is.readObject();
		for (int i : init_players) {
			players.put(i, INITIAL_RATING);
		}
	}
	
	/**
	 * Sets the initial ratings given to newly added players.
	 * @param rating
	 */
	protected void setInitialRating(double rating) {
		this.INITIAL_RATING = rating;
	}
	
	/**
	 * Read the command from the socket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected String getCommand() throws ClassNotFoundException, IOException {
		return (String)is.readObject();
	}
	
	/**
	 * Read an id from the socket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected int readID() throws ClassNotFoundException, IOException {
		return (int)is.readObject();
	}
	
	/**
	 * Reads the player_id from the socket and adds/reactivates the player
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void add_player() throws ClassNotFoundException, IOException {
		int id = readID();
		
		if (deactivatedPlayers.containsKey(id)) {
			players.put(id, deactivatedPlayers.get(id));
			sendRating(id, players.get(id));
			return;
		}
		
		if (!players.containsKey(id)) {
			players.put(id, INITIAL_RATING);
			sendRating(id, INITIAL_RATING);
		} else {
			sendRating(id, players.get(id));
		}
	}
	
	/**
	 * Reads the player id specified by the tournament server and deactivates the player.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void remove_player() throws ClassNotFoundException, IOException {
		int id = readID();
		if (players.containsKey(id)) {
			double rating = players.get(id);
			deactivatedPlayers.put(id, rating);
			players.remove(id);
		} else {
			deactivatedPlayers.put(id, INITIAL_RATING);
		}
	}
	
	/**
	 * Sends all ratings to the tournament server
	 * @throws IOException
	 */
	protected void sendAllRatings() throws IOException {
		os.writeObject("new_ratings");
		System.out.println("sending player ids");
		os.writeObject(players);
		os.flush();
	}
	
	/**
	 * Sends the player id with the rating to the tournament server
	 * @param player_id
	 * @param player_rating
	 * @throws IOException
	 */
	protected void sendRating(int player_id, double player_rating) throws IOException {
		os.writeObject("new_rating");
		System.out.println("sending player id");
		os.writeInt(player_id);
		System.out.println("sending rating");
		os.writeDouble(player_rating);
		os.flush();
	}
	
	/**
	 * Returns a map of player_ids to player scores obtained in a match
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected HashMap<Integer, Double> getResult() throws ClassNotFoundException, IOException {
		String res = (String)is.readObject();
		int id;
		double score;
		String[] ss;
		HashMap<Integer, Double> player_scores = new HashMap<>();
		
		String[] scores = res.split(",");
		for (String s : scores) {
			ss = s.split(" ");
			id = Integer.parseInt(ss[0]);
			score = Double.parseDouble(ss[1]);
			player_scores.put(id, score);
		}
		
		return player_scores;
	}
	
	/**
	 * Changes the specified players rating and sends the updated rating to the tournament server
	 * @param player_id
	 * @param player_rating
	 * @throws IOException
	 */
	protected void setPlayerRating(int player_id, double player_rating) throws IOException {
		this.players.replace(player_id, player_rating);
		sendRating(player_id, player_rating);
	}
	
	/**
	 * Takes a Map of player ids and their respective scores in the match and calculates the new ratings
	 * @param scores
	 */
	public abstract void calculateNewRatings(HashMap<Integer, Double> scores);
	
	
	
	@Override
	public void run() {
		try {
			String s;
			boolean running = true;
			while (running) {
				s = getCommand();
				System.out.println(s);
				
				switch (s) {
					case "init_players" : {
						init_ranker();
						sendAllRatings();
						break;
					}
	
					case "add_player" : {
						add_player();
						break;
					}
					case "remove_player" : {
						remove_player();
						break;
					}
					case "new_result" : {
						HashMap<Integer, Double> a = getResult();
						calculateNewRatings(a);
						break;
					}
					default : {
						break;
					}
				}

			}
		} catch (ClassNotFoundException | IOException e ) {
			e.printStackTrace();
		}
	}
}
