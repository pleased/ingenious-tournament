package src.main.java.components;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;

public class ScoreRanker extends Ranker {

	public ScoreRanker(String _port, double initial_rating) throws UnknownHostException, IOException {
		super(_port, initial_rating);
	}
	
	public static void main(String[] args) {
		try {
			ScoreRanker sr = new ScoreRanker(args[0], 0.0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void calculateNewRatings(HashMap<Integer, Double> scores) {
		for (int i : scores.keySet()) {
			try {
				setPlayerRating(i, players.get(i)+ scores.get(i));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
