package src.main.java.components;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;


public class EloRanker extends Ranker {
	
	public EloRanker(String _port) throws UnknownHostException, IOException {
		//super (port, inital_rating)
		super(_port, 100);
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		EloRanker er = new EloRanker(args[0]);
	}

	private HashMap<Integer, Double> getParticipatingIds(HashMap<Integer, Double> scores) {
		HashMap<Integer, Double> player_ratings = new HashMap<>();
		double rating;
		for (Integer i : scores.keySet()) {
			rating = players.get(i);
			player_ratings.put(i, rating);
		}
		return player_ratings;
	}
	
	public void calculateMultiplayerElo(HashMap<Integer, Double> scores, HashMap<Integer, Double> ratings) throws IOException  {
		
        if (ratings.size() == 0) {
            return;
        }
        
        // Calculate total Q
        double Q = 0.0;
        for (int user_id : ratings.keySet()) {
            Q += Math.pow(10.0, ((double) ratings.get(user_id) / 400));
        }

    	double rating;
        
        // Calculate new rating for each player
        for (int user_id : ratings.keySet()) {
        	
        	rating = ratings.get(user_id);
        	
            int K = getK(rating);
        	
            /**
             * Expected rating for a player
             * E = Q(i) / Q(total)
             * Q(i) = 10 ^ (R(i)/400)
             */
            double expected = Math.pow(10.0, ((double) rating / 400)) / Q;
                        
            double actualScore = scores.get(user_id);

            // new rating = rating + K * (actualScore - expected);
            double newRating = Math.round(rating + K * (actualScore - expected));
            System.out.println("New rating: " + newRating);

            // replace HashMap rating with new rating
            setPlayerRating(user_id, newRating);
        }
        return;
    }

	public int getK(double rating) {
        if (rating < 2000) {
            return 32;
        } else if (rating >= 2000 && rating < 2400) {
            return 24;
        } else {
            return 16;
        }
    }

	@Override
	public void calculateNewRatings(HashMap<Integer, Double> a) {
		HashMap<Integer, Double> ratings = getParticipatingIds(a);
		try {
			System.out.println("Calculating new elo rating");
			calculateMultiplayerElo(a, ratings);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
