package src.main.java.components;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class RoundRobinScheduler extends Scheduler{
	
	public RoundRobinScheduler(String _port, int num_players) throws UnknownHostException, IOException {
		super(_port, num_players);
	}


	public static void main(String[] args) throws UnknownHostException, IOException {
		RoundRobinScheduler rrs = new RoundRobinScheduler(args[0], 2);
		rrs.run();
	}

	private void getSubsets(List<Integer> superSet, int k, int idx, Set<Integer> current,List<Set<Integer>> solution) {
		
	    if (current.size() == k) {
	        solution.add(new HashSet<>(current));
	        return;
	    }
	    
	    if (idx == superSet.size()) return;
	    Integer x = superSet.get(idx);
	    current.add(x);
	    
	    getSubsets(superSet, k, idx+1, current, solution);
	    current.remove(x);
	    getSubsets(superSet, k, idx+1, current, solution);
	}

	public List<Set<Integer>> getSubsets(List<Integer> superSet, int k) {
	    List<Set<Integer>> res = new ArrayList<>();
	    getSubsets(superSet, k, 0, new HashSet<Integer>(), res);
	    return res;
	}
	
	@Override
	protected void add_player(int id) throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		super.add_player(id);
		//int id = readID();
		//players.put(id, 0.0);
		//generateMatches();
	}


	@Override
	public void generateMatches() {
		System.out.println("Generating matches with " + players.size() + " players ");
		Set<Integer> s = players.keySet();
		if (s.size() < num_players) {
			return;
		}
		
		List<Integer> ids = new ArrayList<>();
		for (Integer i : s) {
			ids.add(i);
		}
		
		List<Set<Integer>> ms = getSubsets(ids, num_players);
		ArrayList<Integer> match = new ArrayList<>();
		
		for (Set<Integer> m : ms) {
			match = new ArrayList<>();
			for (Integer k : m) {
				//Handle match
				match.add(k);
			}
			matches.add(match);
		}
		
		sendOutstandingMatches();
	
	}


	@Override
	public void new_match_result(String res) {
		System.out.println(res);
		
	}
	
}

