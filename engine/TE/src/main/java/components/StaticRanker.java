package src.main.java.components;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

public class StaticRanker {

	private static HashMap<Integer, Integer> players = new HashMap<Integer, Integer>();
	private static HashMap<Integer, Integer> deactivatedPlayers = new HashMap<Integer, Integer>();
	private static final int INITIAL_RATING = 0;
	private static ObjectInputStream is;
	private static ObjectOutputStream os;
	
	
	public static void main(String[] args) {
		System.out.println("Ranker started..");
		
		int port = Integer.parseInt(args[0]);
		System.out.println(port);
		Socket socket;
		try {
			socket = new Socket("localhost", port);
			os = new ObjectOutputStream(socket.getOutputStream());
			is = new ObjectInputStream(socket.getInputStream());
			boolean running = true;
			while (running) {
				String s = "";
				
				s = (String)is.readObject();
				System.out.println(s);
				
				switch (s) {
				case "init_players" : {
					ArrayList<Integer> init_players;
					ArrayList<Integer> init_ratings;
					init_players = (ArrayList<Integer>)is.readObject();
					init_ratings = (ArrayList<Integer>)is.readObject();
					int j = 0;
					for (int i : init_players) {
						players.put(i, init_ratings.get(j));
						System.out.println("Player id : " + i);
						j++;
					}
					sendAllRatings();
					break;
				}

				case "add_player" : {
					int id = (int)is.readObject();
					if (!players.containsKey(id)) {
						players.put(id, INITIAL_RATING);
						sendRating(id, INITIAL_RATING);
					} else {
						sendRating(id, 0);
					}
					break;
				}
				case "remove_player" : {
					/* TODO */
					int id = (int)is.readObject();
					if (players.containsKey(id)) {
						int rating = players.get(id);
						deactivatedPlayers.put(id, rating);
						players.remove(id);
					} else {
						deactivatedPlayers.put(id, INITIAL_RATING);
					}
					break;
				}
				case "new_result" : {
					String res = (String)is.readObject();
					System.out.println("result : " + res);
					String winner = calcNewRatings(res);
					System.out.println(winner);
					sendAllRatings();
					break;
				}
				default : {
					break;
				}
				}
			}
		} catch (UnknownHostException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private static void sendAllRatings() throws IOException {
		os.writeObject("new_ratings");
		System.out.println("sending player ratings");
		os.writeObject(players);
		os.flush();
	}
	
	private static void sendRating(int player_id, int player_rating) throws IOException {
		os.writeObject("new_rating");
		System.out.println("sending player id");
		os.writeInt(player_id);
		System.out.println("sending rating");
		os.writeInt(player_rating);
		System.out.println("FLUSH");
		os.flush();
	}
	
	public static String calcNewRatings(String res) {
		String[] scores = res.split(",");
		int id;
		double score;
		int change;
		String ret = "none";
		System.out.println("scores.length = " + scores.length);
		String[] split;
		for (int i = 0; i < scores.length; i++) {
			split = scores[i].split(" ");
			id = Integer.parseInt(split[0]);
			score = Double.parseDouble(split[1]);
			System.out.println("id : " + id);
			System.out.println("score : " + score);
			if (score == 1.0) {
				change = 50;
				ret = Integer.toString(id);
			} else if (score == 0.0) { 
				change = -50;
			} else {
				change = 0;
			}
			players.replace(id, players.get(id), players.get(id)+change);
		}
		
		return ret;
		
	}

}
