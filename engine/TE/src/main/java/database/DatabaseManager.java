package src.main.java.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import src.main.java.constants.Database;
import src.main.java.constants.Tournament;

public class DatabaseManager {
	static final String JDBC_DRIVER = Database.JDBC_DRIVER;
	static String DB_URL = Database.DB_URL;
	
	/**
	 * @return Connection to the db
	 */
	public static Connection getConnection(){
		Connection connect = null;
		try {
			Class.forName(JDBC_DRIVER);
			connect = DriverManager.getConnection(DB_URL);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connect;
	}
	
	/**
	 * @param tournament_id
	 * @return engine ID OR -1 if it failed
	 */
	public static int getEngineID(int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT engine_id FROM tournaments WHERE id = ?;";
		int engine_id = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				engine_id = res.getInt(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return engine_id;
	}
	
	/**
	 * @param tournament_id
	 * @return scheduler ID OR -1 if it failed
	 */
	public static int getSchedulerID(int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT scheduler_id FROM tournaments WHERE id = ?;";
		int sched_id = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				sched_id = res.getInt(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return sched_id;
	}
	
	
	/**
	 * @param tournament_id
	 * @return resource ID OR -1 if it failed
	 */
	public static int getResourceID(int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT resources_id FROM tournaments WHERE id = ?;";
		int res_id = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				res_id = res.getInt(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return res_id;
	}
	
	/**
	 * @param tournament_id
	 * @return ranker ID OR -1 if it failed
	 */
	public static int getRankerID(int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT ranker_id FROM tournaments WHERE id = ?;";
		int ranker_id = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				ranker_id = res.getInt(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ranker_id;
	}
	
	/**
	 * @param tournament_id
	 * @return player IDs separated by commas of tournament if successful OR an empty string if not
	 */
	public static String getPlayers(int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT player_id FROM tournament_players WHERE tournament_id = ?;";
		ArrayList<String> ids = new ArrayList<String>(); 
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				ids.add(res.getString(1));
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return String.join(",", ids);
	}
	
	/**
	 * @param tournament_id
	 * @return player ID's of match separated by commas if successful OR "none" if not
	 */
	public static String getPlayersInMatch(int match_id) {
		Connection connect = getConnection();
		String sql = "SELECT player_ids FROM matches WHERE id = ?;";
		String ids = "none";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				ids = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ids;
	}
	
	
	/**
	 * 
	 * @param match_id
	 * @return the number of players participating in the match
	 */
	public static int getNumPlayersInMatch(int match_id) {
		Connection connect = getConnection();
		String sql = "SELECT player_ids FROM matches WHERE id = ?;";
		String ids = "none";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				ids = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ids.split(",").length;
	}
	
	
	
	/**
	 * @param tournament_id
	 * @return tournament name if successful OR the empty string if not
	 */
	public static String getTournamentName(int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT name FROM tournaments WHERE id = ?;";
		String t_name = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				t_name = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return t_name;
	}
	
	
	public static String getTournamentConfig(int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT config FROM tournaments WHERE id = ?;";
		String config = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				config = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return config;
	}
	
	/**
	 * @param tournament_id
	 * @return scheduler name if successful OR the empty string if not
	 */
	public static String getSchedulerName(int scheduler_id) {
		Connection connect = getConnection();
		String sql = "SELECT name FROM schedulers WHERE id = ?;";
		String s_name = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, scheduler_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				s_name = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return s_name;
	}
	
	
	/**
	 * @param player_id
	 * @return player name if successful OR the empty string if not
	 */
	public static String getPlayerName(int player_id) {
		Connection connect = getConnection();
		String sql = "SELECT name FROM players WHERE id = ?;";
		String player_name = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, player_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				player_name = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return player_name;
	}
	
	/**
	 * @param ranker_id
	 * @return ranker directory if successful OR the empty string if not
	 */
	public static String getRankerDir(int ranker_id) {
		Connection connect = getConnection();
		String sql = "SELECT file FROM rankers WHERE id = ?;";
		String ranker_dir = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, ranker_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				ranker_dir = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ranker_dir;
	}
	
	/**
	 * @param ranker_id
	 * @return ranker name if successful OR the empty string if not
	 */
	public static String getRankerName(int ranker_id) {
		Connection connect = getConnection();
		String sql = "SELECT name FROM rankers WHERE id = ?;";
		String ranker_name = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, ranker_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				ranker_name = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ranker_name;
	}
	
	
	/**
	 * @param player_id
	 * @return player directory if successful OR the empty string if not
	 */
	public static String getPlayerDir(int player_id) {
		Connection connect = getConnection();
		String sql = "SELECT dir FROM players WHERE id = ?;";
		String player_dir = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, player_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				player_dir = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return player_dir;
	}
	
	/**
	 * 
	 * @param player_id
	 * @return Entry point to the player engine
	 */
	public static String getPlayerClassName(int player_id) {
		Connection connect = getConnection();
		String sql = "SELECT classname FROM players WHERE id = ?;";
		String player_class = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, player_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				player_class = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return player_class;
	}
	
	
	/**
	 * @param player_id
	 * @return player rating if successful OR -1 if not
	 */
	public static int getPlayerRating(int player_id, int tournament_id) {
		Connection connect = getConnection();
		String sql = "SELECT rating FROM tournament_players WHERE player_id = ? AND tournament_id = ?;";
		int player_rating = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, player_id);
			stmt.setInt(2, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				player_rating = res.getInt(1);
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return player_rating;
	}
	
	/**
	 * @param sched_id
	 * @return scheduler directory if successful OR the empty string if not
	 */
	public static String getSchedulerDir(int scheduler_id) {
		Connection connect = getConnection();
		String sql = "SELECT file FROM schedulers WHERE id = ?;";
		String sched_dir = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, scheduler_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				sched_dir = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return sched_dir;
	}
	
	/**
	 * @param engine_id
	 * @return engine directory if successful OR the empty string if not
	 */
	public static String getEngineDir(int engine_id) {
		Connection connect = getConnection();
		String sql = "SELECT file FROM engines WHERE id = ?;";
		String engine_dir = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, engine_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				engine_dir = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return engine_dir;
	}
	
	
	public static String getEnginePath(int engine_id) {
		Connection connect = getConnection();
		String sql = "SELECT path_to_ref FROM engines WHERE id = ?;";
		String engine_path = "";
		try {
			
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, engine_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				engine_path = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return engine_path;
	}
	
	/**
	 * 
	 * @param engine_id
	 * @return engine name corresponding to the provided engine id
	 */
	public static String getEngineName(int engine_id) {
		Connection connect = getConnection();
		String sql = "SELECT name FROM engines WHERE id = ?;";
		String engine_name = "";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, engine_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				engine_name = res.getString(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return engine_name;
	}
	
	/**
	 * 
	 * @param player id
	 * @return the user id of the player id supplied if successful OR -1 if not
	 */
	public static Integer getUser(Integer i) {
		Connection connect = getConnection();
		String sql = "SELECT user_id FROM players WHERE id = ?;";
		Integer user_id = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, i);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				user_id = res.getInt(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return user_id;
	}
	
	/**
	 * 
	 * @param match player ID's
	 * @return user ID's of players separated by commas, empty string if failed
	 */
	public static String getUsersInMatch(ArrayList<Integer> match) {
		ArrayList<String> user_ids = new ArrayList<String>(match.size());
		for (Integer i : match) {
			if (i != -1) {
				user_ids.add(Integer.toString(getUser(i)));
			}
		}
		return String.join(",", user_ids);
	}
	
	
	/**
	 * 
	 * @param tournament_id
	 * @param player_id
	 * @return tournament_player_id if successful OR -1 if not
	 */
	public static int getPlayerTournamentId(int tournament_id, int player_id) {
		Connection connect = getConnection();
		String sql = "SELECT tournament_player_id FROM tournament_players WHERE player_id = ? AND tournament_id = ?";
		Integer tournament_player_id = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, player_id);
			stmt.setInt(2, tournament_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				tournament_player_id = res.getInt(1);
			}
			connect.close();
			res.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return tournament_player_id;
	}
	
	
	/**
	 * 
	 * @param tournament_id
	 * @param player ID's
	 * @return match ID if successful OR -1 if not
	 */
	public static int addMatch(int tournament_id, ArrayList<Integer> player_ids) {
		Connection connect = getConnection();
		String sql = "INSERT INTO matches (tournament_id, num_players, player_ids, status, user_ids, result_id) VALUES (?, ?, ?, ?, ?, ?);";
		ArrayList<String> p_ids = new ArrayList<String>(player_ids.size());
		for (Integer i : player_ids) {
			p_ids.add(Integer.toString(i));
		}
		
		String ids = String.join(",", p_ids);
		
		String user_ids = getUsersInMatch(player_ids);
		int id = -1;
		int status = Tournament.MATCH_SCHEDULED;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_id);
			stmt.setInt(2, player_ids.size());
			stmt.setString(3, ids);
			stmt.setInt(4, status);
			stmt.setString(5, user_ids);
			stmt.setInt(6, -1);
			if (stmt.executeUpdate() != 1) {
				return -1;
			}
			sql = "SELECT LAST_INSERT_ID()";
			stmt = connect.prepareStatement(sql);
			
			ResultSet res = stmt.executeQuery(sql);
			res.next();
			id = res.getInt(1);
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return id;
	}
	
	/**
	 * 
	 * @param tournament_id
	 * @param player_ids
	 * @return true of successful OR false if not
	 */
	public static boolean RemoveMatch(int match_id) {
		Connection connect = getConnection();
		String sql = "DELETE matches,match_result,match_log_files "
				+ "FROM matches "
				+ "LEFT JOIN match_result ON matches.id = match_result.match_id "
				+ "LEFT JOIN match_log_files ON matches.id = match_log_files.match_id "
				+ "WHERE matches.id = ?";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 
	 * @param player_id
	 * @param rating
	 * @param tournament_id
	 * @return 1 if successful OR -1 if not
	 */
	public static boolean newRating(int player_id, double rating, int tournament_id) {
		Connection connect = getConnection();
		String sql = "UPDATE tournament_players SET rating = ? WHERE player_id = ? AND tournament_id = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setDouble(1, rating);
			stmt.setInt(2, player_id);
			stmt.setInt(3, tournament_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	
	/**
	 * @param match_id
	 * @return true if successful OR false if not
	 */
	public static boolean setMatchActive(int match_id) {
		Connection connect = getConnection();
		String sql = "UPDATE matches SET status = ? WHERE id = ?;";
		int status = Tournament.MATCH_RUNNING;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, status);
			stmt.setInt(2, match_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 
	 * @param match_id
	 * @param dir
	 * @return true if successful or false if not
	 */
	public static boolean playerHasLogFile(int match_id, int player_id, String dir) {
		Connection connect = getConnection();
		String sql = "SELECT match_id, player_id, log_dir FROM match_log_files WHERE match_id = ? AND player_id = ? AND log_dir = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_id);
			stmt.setInt(2, player_id);
			stmt.setString(3, dir);
			ResultSet res = stmt.executeQuery();
			if (!res.next()) {
				connect.close();
				return false;
			} else {
				connect.close();
				return true;
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 
	 * @param match_id
	 * @param dir
	 * @return true if successful or false if not
	 */
	public static boolean setPlayerLogDir(int match_id, int player_id, String dir) {
		Connection connect = getConnection();
		String sql = "INSERT INTO match_log_files (match_id, player_id, log_dir) VALUES (?,?,?);";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_id);
			stmt.setInt(2, player_id);
			stmt.setString(3, dir);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 
	 * @param match_id
	 * @param dir
	 * @return true if successful or false if not
	 */
	public static boolean removePlayerLogDir(int match_id) {
		Connection connect = getConnection();
		String sql = "DELETE FROM match_log_files WHERE match_id = ?";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param tournament_player_id
	 * @return the number of matches the player is participating in currently
	 */
	public static int getNumPlayerActiveMatches(int tournament_player_id) {
		Connection connect = getConnection();
		String sql = "SELECT active_count FROM tournament_players WHERE tournament_player_id = ?";
		int num_matches = 0;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_player_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				num_matches = res.getInt(1);
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return num_matches;
	}
	
	
	/**
	 * 
	 * @param tournament_player_id
	 * @return number of matches the user is scheduled in
	 */
	public static int getNumPlayerScheduledMatches(int tournament_player_id) {
		Connection connect = getConnection();
		String sql = "SELECT scheduled_count FROM tournament_players WHERE tournament_player_id = ?";
		int num_matches = 0;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, tournament_player_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				num_matches = res.getInt(1);
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return num_matches;
	}
	
	
	/**
	 * @param match_id
	 * @return true if successful OR false if not
	 */
	public static boolean setPlayersActive(int match_id, int tournament_id) {
		Connection connect = getConnection();
		String p_ids = getPlayersInMatch(match_id);
		if (p_ids.equals("none")) {
			//Discard match
			return false;
		}
		String[] ids = p_ids.split(",");
		String sql = "UPDATE tournament_players SET active_count = ? , scheduled_count = ? WHERE tournament_player_id = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			for (int i = 0; i < ids.length; i++) {
				int player_id = Integer.parseInt(ids[i]);
				int tournament_player_id = DatabaseManager.getPlayerTournamentId(tournament_id, player_id);
				int cur_active_count = DatabaseManager.getNumPlayerActiveMatches(tournament_player_id);
				int cur_sched_count = DatabaseManager.getNumPlayerScheduledMatches(tournament_player_id);
				stmt.setInt(1, cur_active_count+1);
				stmt.setInt(2, cur_sched_count-1);
				stmt.setInt(3, tournament_player_id);
				if (stmt.executeUpdate() != 1) {
					return false;
				}
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/** Deactivates the player from the specified tournament
	 * @param player_id, tournament_id
	 * @return true if successful OR false if not
	 */
	public static boolean deactivatePlayer(int player_id, int tournament_id) {
		Connection connect = getConnection();
		String sql = "UPDATE tournament_players SET status = ? WHERE tournament_id = ? AND player_id = ?;";
		int status = 4;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, status);
			stmt.setInt(2, tournament_id);
			stmt.setInt(3, player_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	
	/** Activates the player from the specified tournament
	 * @param player_id, tournament_id
	 * @return true if successful OR false if not
	 */
	public static boolean reactivatePlayer(int player_id, int tournament_id) {
		Connection connect = getConnection();
		String sql = "UPDATE tournament_players SET status = ? WHERE tournament_id = ? AND player_id = ?;";
		int status = 0;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, status);
			stmt.setInt(2, tournament_id);
			stmt.setInt(3, player_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	
	/**
	 * @param match_id
	 * @return true if successful OR false if not
	 */
	public synchronized static boolean setPlayerScheduled(int player_id, int tournament_id) {
		Connection connect = getConnection();
		String sql = "UPDATE tournament_players SET scheduled_count = ? WHERE tournament_player_id = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			int tournament_player_id = DatabaseManager.getPlayerTournamentId(tournament_id, player_id);
			int cur_count = DatabaseManager.getNumPlayerScheduledMatches(tournament_player_id);
			stmt.setInt(1, cur_count+1);
			stmt.setInt(2, tournament_player_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * @param match_id
	 * @return true if successful OR false if not
	 */
	public synchronized static boolean setPlayersScheduled(int match_id, int tournament_id) {
		Connection connect = getConnection();
		String p_ids = getPlayersInMatch(match_id);
		if (p_ids.equals("none")) {
			//Discard match
			return false;
		}
		String[] ids = p_ids.split(",");
		String sql = "UPDATE tournament_players SET scheduled_count = ? WHERE tournament_player_id = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			for (int i = 0; i < ids.length; i++) {
				int player_id = Integer.parseInt(ids[i]);
				int tournament_player_id = DatabaseManager.getPlayerTournamentId(tournament_id, player_id);
				int cur_count = DatabaseManager.getNumPlayerScheduledMatches(tournament_player_id);
				stmt.setInt(1, cur_count+1);
				stmt.setInt(2, tournament_player_id);
				if (stmt.executeUpdate() != 1) {
					return false;
				}
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	public synchronized static boolean setPlayerInactive(int player_id, int tournament_id) {
		Connection connect = getConnection();
		String sql = "UPDATE tournament_players SET active_count = ? WHERE tournament_player_id = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			int tournament_player_id = DatabaseManager.getPlayerTournamentId(tournament_id, player_id);
			int cur_count = DatabaseManager.getNumPlayerActiveMatches(tournament_player_id);
			stmt.setInt(1, cur_count-1);
			stmt.setInt(2, tournament_player_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	public synchronized static boolean setPlayersInactive(int match_id, int tournament_id) {
		Connection connect = getConnection();
		String p_ids = getPlayersInMatch(match_id);
		if (p_ids.equals("none")) {
			//Discard match
			return false;
		}
		String[] ids = p_ids.split(",");
		String sql = "UPDATE tournament_players SET active_count = ? WHERE tournament_player_id = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			for (int i = 0; i < ids.length; i++) {
				int player_id = Integer.parseInt(ids[i]);
				int tournament_player_id = DatabaseManager.getPlayerTournamentId(tournament_id, player_id);
				int cur_count = DatabaseManager.getNumPlayerActiveMatches(tournament_player_id);
				stmt.setInt(1, cur_count-1);
				stmt.setInt(2, tournament_player_id);
				if (stmt.executeUpdate() != 1) {
					return false;
				}
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 
	 * @param match_id
	 * @return true if successful OR false if not
	 */
	public static boolean setMatchDone(int match_id) {
		Connection connect = getConnection();
		String sql = "UPDATE matches SET status = ? WHERE id = ?;";
		int status = Tournament.MATCH_COMPLETE;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, status);
			stmt.setInt(2, match_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param match_id
	 * @param winner
	 * @return match result ID OR -1 if it failed to insert
	 */
	public static int newMatchResult(int match_id, int winner) {
		Connection connect = getConnection();
		String sql = "INSERT INTO match_result (match_id, result, winner) VALUES (?, ?, ?);";
		String result = "";
		int id = -1;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_id);
			stmt.setString(2, result);
			stmt.setInt(3, winner);
			if (stmt.executeUpdate() != 1) {
				return -1;
			}
			sql = "SELECT LAST_INSERT_ID()";
			stmt = connect.prepareStatement(sql);
			ResultSet res = stmt.executeQuery(sql);
			res.next();
			id = res.getInt(1);
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return id;
	}
	
	/**
	 * 
	 * @param match_id
	 * @param match_res
	 * @return true if successful OR false if not
	 */
	public static boolean setMatchResult(int match_id, int match_res) {
		Connection connect = getConnection();
		String sql = "UPDATE matches SET result_id = ? WHERE id = ?;";
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, match_res);
			stmt.setInt(2, match_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param tournament_id
	 * @return true if successful OR false if not
	 */
	public static boolean setTournamentDone(int tournament_id) {
		Connection connect = getConnection();
		String sql = "UPDATE tournaments SET status = ? WHERE id = ?;";
		int status = Tournament.TOURNAMENT_DONE;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, status);
			stmt.setInt(2, tournament_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param match_id
	 * @return true if successful OR false if not
	 */
	public static boolean setMatchUpcoming(int match_id) {
		Connection connect = getConnection();
		String sql = "UPDATE matches SET status = ? WHERE id = ?;";
		int status = Tournament.MATCH_SCHEDULED;
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, status);
			stmt.setInt(2, match_id);
			if (stmt.executeUpdate() != 1) {
				return false;
			}
			connect.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 
	 * @param resource_id
	 * @return resource limitations - String [] 0 : memory, 1 : cpu
	 */
	public static String[] getResources(int resource_id) {
		Connection connect = getConnection();
		String sql = "SELECT memory, cpu FROM resources WHERE id = ?;";
		String[] ret = new String [2];
		try {
			PreparedStatement stmt = connect.prepareStatement(sql);
			stmt.setInt(1, resource_id);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				ret[0] = res.getString(1);
				ret[1] = Double.toString(res.getDouble(2));
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	
	public static void main(String[] args) {
		String[] res = getResources(6);
		
	}

}
