package src.main.java.core;

import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.StreamCorruptedException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import src.main.java.components.Player;
import src.main.java.constants.Server;
import src.main.java.database.DatabaseManager;
import src.main.java.utils.ShellExec;

public class RankerWrapper extends Thread{
	
	private int id;
	private Tournament tournament;
	private int tournament_id;
	private String r_dir;
	private ServerSocket socket;
	private ObjectInputStream is;
	private ObjectOutputStream os;
	private boolean running = false;
	private int sleep;
	private static PrintStream ps;
	private Collection<Player> players = new ArrayList<>();
	

	public RankerWrapper (int id, Tournament tournament, String dir, Collection<Integer> players) throws FileNotFoundException {
		new File(System.getProperty("user.home") + "/TEResources/Logs/Engine/Rankers/").mkdirs();
		String dirr = System.getProperty("user.home") + "/TEResources/Logs/Engine/Rankers/RankerWrapper_" + tournament.getID() + ".log";
		File log = new File(dirr);
		ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(log)), true);
		this.id = id;
		this.tournament = tournament;
		this.tournament_id = tournament.getID();
		this.r_dir = dir;
		sleep = Server.SERVER_SLEEP_DURATION;
		for (Integer i : players) {
			this.players.add(new Player(i, tournament_id));
		}
		
		
		try {
			socket = new ServerSocket(0);

			new Thread(){
				@Override
				public void run() {
					String[] args = {"java", "-jar", r_dir, Integer.toString(socket.getLocalPort())};
					String s_name = DatabaseManager.getRankerName(id);
					new File(System.getProperty("user.home") + "/TEResources/Logs/" + DatabaseManager.getTournamentName(tournament_id) + "/ranker" + id + "/").mkdirs();
					String dir = System.getProperty("user.home") + "/TEResources/Logs/" + DatabaseManager.getTournamentName(tournament_id) + "/ranker" + id + "/" + s_name + ".log";
					File log = new File(dir);
					Process engineProcess = ShellExec.buildProcess(args, log);
					try {
						engineProcess.waitFor();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						engineProcess.destroy();
					}
				};
			}.start();
			ps.println("Started ranker..");
			ps.println(r_dir);
			
			Socket s = socket.accept();
			ps.println("Accepted ranker connection..");
			
			this.os = new ObjectOutputStream(s.getOutputStream());
			this.is = new ObjectInputStream(s.getInputStream());
			ps.println("Init Ranker...");
			init_ranker();
			start();
					
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void init_ranker() {
		try {
			
			os.writeObject("init_players");
			ArrayList<Integer> player_ids = new ArrayList<>();
			for (Player p : players) {
				player_ids.add(p.getID());
			}
			os.writeObject(player_ids);
			os.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ps.println("Init ranker done...");
	}
	
	public void add_player(int player_id) {
		this.players.add(new Player(player_id, tournament_id));
		try {
			os.writeObject("add_player");
			os.writeObject(player_id);
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void remove_player(int player_id) {
		this.players.remove((player_id));
		try {
			os.writeObject("remove_player " + player_id);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void start() {
		this.running = true;
	}
	
	public void pause() {
		this.running = false;
	}
	
	public void new_result(String res) {
		ps.println("new result: " + res);
		try {
			os.writeObject("new_result");
			os.flush();
			os.writeObject(res);
			os.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String s = "";
		while (running) {
			try {
				s = (String)this.is.readObject();
				ps.println(s);
				switch (s) {
				case "new_ratings" : {
					Map<Integer, Double> a = new HashMap<>();
					a = (HashMap<Integer, Double>)is.readObject();
					for (int i : a.keySet()) {
						tournament.new_rating(i, a.get(i));
					}
					break;
				}
				case "new_rating" : {
					int player_id = is.readInt();
					double rating = is.readDouble();
					tournament.new_rating(player_id, rating);
					break;
				}
				case "done" : {
					running = false;
					this.stop();
					/* TODO */
					break;
				}
				default : {
					/* TODO */
					break;
				}
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (StreamCorruptedException socke) {
				ps.println(socke.toString());
			} catch (EOFException e) {
				
			}catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				this.stop();
			}
			
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}