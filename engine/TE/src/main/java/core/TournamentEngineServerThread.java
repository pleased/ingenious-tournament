package src.main.java.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TournamentEngineServerThread extends Thread {

	private TournamentEngineServer server = null;
	public Socket socket = null;
	private InputStream in;
	private OutputStream out;
	private int clientPort;

	public TournamentEngineServerThread(TournamentEngineServer _server, Socket _socket, int clientPort) {
		server = _server;
		socket = _socket;
		this.clientPort = clientPort;
	}

	public void send(String msg) {
        /* TODO */
	}

	@Override
	public void run() {
            System.out.println(("Server Thread " + socket.getPort() + " running."));
            while (true) {
            	try {
            		try {
            			in = (socket.getInputStream());
            		} catch (IOException e) {
            			e.printStackTrace();
            		}
            		StringBuilder sb = new StringBuilder("");
            		int s;
            		while ((s = in.read()) != 10) {
            			sb.append(Character.toString((char)s));
            		}
            		server.handle(clientPort, sb.toString());
            		server.remove(clientPort);
            		stop();

            	} catch (IOException e) {
            		e.printStackTrace();
            	}
            }
	}

	public void open() throws IOException {
		in = (socket.getInputStream());
		out = (socket.getOutputStream());
	}

	public void close() throws IOException {
		if (socket != null) {
			socket.close();
		}
		if (in != null) {
			in.close();
		}

		if (out != null) {
			out.close();
		}
	}
}
