package src.main.java.core;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.StreamCorruptedException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;

import src.main.java.components.Player;
import src.main.java.database.DatabaseManager;
import src.main.java.utils.ShellExec;
import src.main.java.constants.Server;

public class SchedulerWrapper extends Thread {
	
	private int id;
	private int tournament_id;
	private String s_dir;
	private ServerSocket socket;
	private ObjectInputStream is;
	private ObjectOutputStream os;
	private boolean running = false;
	private boolean paused = false;
	private int sleep;
	private static PrintStream ps;
	private Collection<Player> players = new ArrayList<Player>();
	private ArrayList<ArrayList<Player>> matches = new ArrayList<ArrayList<Player>>();
	

	public SchedulerWrapper (int id, int t_id, Collection<Integer> players) throws FileNotFoundException {
		new File(System.getProperty("user.home") + "/TEResources/Logs/Engine/Schedulers/").mkdirs();
		String dirr = System.getProperty("user.home") + "/TEResources/Logs/Engine/Schedulers/SchedulerWrapper_" + t_id + ".log";
		File log = new File(dirr);
		ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(log)), true);
		this.id = id;
		this.tournament_id = t_id;
		this.s_dir = DatabaseManager.getSchedulerDir(id);
		this.sleep = Server.SERVER_SLEEP_DURATION;
		for (int i : players) {
			Integer I = i;
			this.players.add(new Player(I, tournament_id));
		}
		
		
		try {
			socket = new ServerSocket(0);

			new Thread(){
				@Override
				public void run() {
					String[] args = {"java", "-jar", s_dir, Integer.toString(socket.getLocalPort())};
					String s_name = DatabaseManager.getSchedulerName(id);
					new File(System.getProperty("user.home") + "/TEResources/Logs/" + DatabaseManager.getTournamentName(tournament_id) + "/scheduler_" + id + "/").mkdirs();
					String dir = System.getProperty("user.home") + "/TEResources/Logs/" + DatabaseManager.getTournamentName(tournament_id) + "/scheduler_" + id + "/" + s_name + ".log";
					File log = new File(dir);
					Process engineProcess = ShellExec.buildProcess(args, log);
					try {
						engineProcess.waitFor();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						engineProcess.destroy();
					}
				};
			}.start();
			
			ps.println("Started scheduler..");
			
			Socket s = socket.accept();
			ps.println("Accepted scheduler connection..");
			
			this.os = new ObjectOutputStream(s.getOutputStream());
			this.is = new ObjectInputStream(s.getInputStream());
			ps.println("Init Scheduler...");
			init_scheduler();
			start();
					
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void init_scheduler() {
		try {
			for (Player p : players) {
				ps.println(p.getID());
			}
			os.writeObject("init");
			ArrayList<Integer> player_ids = new ArrayList<Integer>();
			for (Player p : players) {
				player_ids.add(p.getID());
			}
			os.writeObject(player_ids);		
			os.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void add_player(int player_id) {
		//this.players.add(new Player(player_id, tournament_id));
		ps.println("Writing add player to scheduler");
		try {
			os.writeObject("add_player");
			os.flush();
			os.writeObject(player_id);
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ps.println("DONE WRITING NEW PLAYER");
	}
	
	public void remove_player(int player_id) {
		this.players.remove((player_id));
		try {
			os.writeObject("remove_player");
			os.flush();
			os.writeObject(player_id);
		} catch (IOException e) {
			ps.println("Remove player write failed..");
			return;
		}
	}
	
	public void new_result(String res) {
		try {
			os.writeObject("new_result");
			os.flush();
			os.writeObject(res);
			os.flush();
		} catch (IOException e) {
			ps.println("New result failed to write..");
			return;
		}
	}
	
	@Override
	public void start() {
		if (paused) {
			try {
				os.writeObject("resume");
				this.paused = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.running = true;
	}
	
	public void terminate() {
		this.running = false;
	}
	
	public synchronized boolean hasNextMatch() {
		if (this.matches.size() != 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized ArrayList<Integer> getNextMatch() {
		ArrayList<Player> ps = matches.remove(0);
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Player p : ps) {
			ids.add(p.getID());
		}
		return ids;
	}

	private synchronized void handleNextMatch() throws ClassNotFoundException, IOException {
		ArrayList<Player> e = new ArrayList<Player>();
		ArrayList<Integer> a = new ArrayList<Integer>();
		a = (ArrayList<Integer>)is.readObject();
		for (int i : a) {
			e.add(new Player(i, tournament_id));
		}
		
		matches.add(e);
		//Write match to tournament immediately ?
	}
	
	
	@Override
	public void run() {
		while (running) {
			String s = "";
			try {
				s = (String)this.is.readObject();
				ps.println(s);
				switch (s) {
				case "next_match" : {
					handleNextMatch();
					break;
				}
				case "done" : {
					running = false;
					this.stop();
					/* TODO */
					break;
				}
				default : {
					/* TODO */
					break;
				}
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
			} catch (StreamCorruptedException socke) {
				ps.println(socke.toString());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
			}
			
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}