package src.main.java.core;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import src.main.java.database.DatabaseManager;

public class Tournament implements Runnable {
	private int id;
	private int engine_id;
	private int ranker_id;
	private int scheduler_id;
	private Collection<Integer> players = new ArrayList<>();
	private Collection<Integer> deactivated_players = new ArrayList<>();
	private volatile int running = 0; //0 = Stopped; 1 = running; 2 = paused;
	private boolean moreMatches = true;
	private RankerWrapper rw = null;
	private SchedulerWrapper sw = null;
	Thread r = null;
	Thread s = null;
	private static PrintStream ps;
	private LinkedList<Integer> match_Q = new LinkedList<>();
	private Map<Integer, Integer> match_strikes = new HashMap<>();
	private Map<Integer, Integer> player_strikes = new HashMap<>();
	private Map<Integer, Integer> engine_strikes = new HashMap<>();
	private Map<Integer, Mediator> running_threads = new HashMap<>();

	public Tournament(int id) throws FileNotFoundException {
		new File(src.main.java.constants.Tournament.TOURNAMENT_LOG_DIR).mkdirs();
		String dir = src.main.java.constants.Tournament.TOURNAMENT_LOG_DIR + src.main.java.constants.Tournament.getTournamentLogName(id);
		File log = new File(dir);
		ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(log)), true);
		this.id = id;
		this.engine_id = DatabaseManager.getEngineID(id);
		this.ranker_id = DatabaseManager.getRankerID(id);
		this.scheduler_id = DatabaseManager.getSchedulerID(id);;
		
		getPlayers();

	}

	public void getPlayers() {
		String players = DatabaseManager.getPlayers(this.id);
		String[] ids = players.split(",");
		int id;
		for (String s : ids) {
			id = Integer.parseInt(s);
			this.players.add(id);
		}
	}

	public int getID() {
		return id;
	}

	public void reactivatePlayer(int id) {
		players.add(id);
		deactivated_players.remove(id);
	}

	public void deactivatePlayer(int id) {
		players.remove(id);
		deactivated_players.add(id);
		sw.remove_player(id);
		rw.remove_player(id);
		DatabaseManager.deactivatePlayer(id, this.id);
		
		/* TODO : what about opposing player active match count ? */
	}

	public void addPlayer(int id) {
		ps.println("Add Player");
		//Check if deactivated
		if (deactivated_players.contains(id)) {
			reactivatePlayer(id);
			ps.println("reactivete");
		} else {
			players.add(id);
			ps.println("added");
		}
		if (isRunning()) {
			//Notify scheduler of addition
			this.sw.add_player(id);
			ps.println("notiied scheduler");
			//Notify ranker of addition and get initial rating
			this.rw.add_player(id);	
		} else {
			this.sw.add_player(id);
			ps.println("notiied scheduler");
			//Don't notify ranker since it is not running
		}

	}

	public void removePlayer(int id) {
		deactivatePlayer(id);
		ps.println("Deactivating player!");

		//Notify scheduler of removal
		sw.remove_player(id);
		ps.println("Notified scheduler!");

		//Notify ranker of removal
		rw.remove_player(id);
		ps.println("Notified ranker!");

		//Discard matches currently running with the player
		for (int i : getRunningThreadKeys()) {
			String player_ids = DatabaseManager.getPlayersInMatch(i);
			if (player_ids.contains(Integer.toString(id))) {
				running_threads.get(i).stopMediator();
				DatabaseManager.setPlayersScheduled(i, id);
				DatabaseManager.setPlayersInactive(i, id);
				if (!DatabaseManager.RemoveMatch(i)) {
					ps.println("Failed to remove match:" + i);
				}
				running_threads.remove(i);
			}
		}
		ps.println("Stopped ongoing matches");

		//Remove scheduled matches containing player
		if (match_Q.size() >= 1) {
			Object[] s = match_Q.toArray();
			int j = 0;
			int k = 0;
			ps.println("match_Q size = " + match_Q.size());
			for (Object i : s) {
				k = (int)i;
				DatabaseManager.setPlayersScheduled(k, id);
				DatabaseManager.setPlayersInactive(k, id);
				String player_ids = DatabaseManager.getPlayersInMatch(k);
				if (player_ids.contains(Integer.toString(id))) {
					match_Q.remove(j);
					j--;
					//Removed match from queue due to containing a removed player
					//Remove match from database.
					if (!DatabaseManager.RemoveMatch(k)) {
						//decrease player scheduled matches TODO
						ps.println("Failed to remove match : " + k);
					}
				}
				j++;
			}
		}
		ps.println("Removed queued matches");

		//Remove completed matches

	}

	private Set<Integer> getRunningThreadKeys() {
		return (running_threads.size() >= 1) ? running_threads.keySet() : Collections.emptySet();
	}

	private String getRunningPlayerIds() {
		List<String> run_ids = new ArrayList<String>();

		for (int i : getRunningThreadKeys()) {
			String player_ids = DatabaseManager.getPlayersInMatch(i);
			String[] ids = player_ids.split(",");
			for (String ss : ids) {
				if (!run_ids.contains(ss)) {
					run_ids.add(ss);
				}
			}
		}

		return String.join(",", run_ids);
	}

	public synchronized void new_result(String res, Integer m, int winner) {
		ps.println("===========NEW RESULT============");
		if (running == 1) {

			running_threads.get(m).stopMediator();
			running_threads.remove(m);

			DatabaseManager.setMatchDone(m);
			String player_ids = DatabaseManager.getPlayersInMatch(m);
			if (player_ids.equals("none")) {
				//Discard match
				return;
			}

			DatabaseManager.setPlayersInactive(m, this.id);

			int match_res_id = DatabaseManager.newMatchResult(m, winner);
			DatabaseManager.setMatchResult(m, match_res_id);

			//Notify ranker and scheduler of new result
			rw.new_result(res);
			sw.new_result(res);

			ps.println("Match done, results entered into db");
		} else {
			running_threads.get(m).stopMediator();
			running_threads.remove(m);
			DatabaseManager.setMatchUpcoming(m);
			//DatabaseManager.setPlayersScheduled(m, id);
			//DatabaseManager.setPlayersInactive(m, id);
			// TODO : HERE
			if (!match_Q.contains(m)) {
				match_Q.addFirst(m);
			}
			ps.println("Tournament not running, discard match and add to queue");
		}
		printMatches();
		ps.println("===========NEW RESULT============/done");
	}

	public synchronized void new_rating(int id, double rating) {
		ps.println("===========NEW RATING============");
		ps.println("New rating id = " + id);
		ps.println("New rating = " + rating);
		ps.println("player contains:  " + players.contains(id));
		if (!players.contains(id)) {
			return;
		}
		boolean res = DatabaseManager.newRating(id, rating, this.id);
		if (!res) {
			/* TODO: error updating rating */
		}
		
		ps.println("===========NEW RATING============/done");
	}

	public synchronized void pause() {
		this.running = src.main.java.constants.Tournament.TOURNAMENT_PAUSED;
		this.rw.pause();
		this.rw.stop();
		//this.sw.pause();
		ps.println("===========PAUSE============");
		ps.println("Paused tournament_" + id);
		for (int i : getRunningThreadKeys()) {
			if (!match_Q.contains(i)) {
				match_Q.addFirst(i);
				DatabaseManager.setMatchUpcoming(i);
				DatabaseManager.removePlayerLogDir(i);
				DatabaseManager.setPlayersScheduled(i, id);
				DatabaseManager.setPlayersInactive(i, id);
			}
			running_threads.get(i).stopMediator();
			running_threads.remove(i);
		}
		printMatches();
		ps.println("===========PAUSE============/done");
	}

	public void noMoreMatches() {
		moreMatches = false;
	}

	public void start() throws FileNotFoundException {
		ps.println("===========STARTs============");
		this.running = src.main.java.constants.Tournament.TOURNAMENT_RUNNING;

		String ranker_dir = DatabaseManager.getRankerDir(this.ranker_id);
		String sched_dir = DatabaseManager.getSchedulerDir(this.scheduler_id);

		if (sw == null) {
			this.sw = new SchedulerWrapper(this.scheduler_id, this.id, players);
			s = new Thread(sw);
			s.start();
		} else {
			this.sw.start();
			if (s == null) {
				s = new Thread(sw);
				s.start();
			}
		}

		this.rw = new RankerWrapper(this.ranker_id, this, ranker_dir, players);
		r = new Thread(rw);
		r.start();
		
		printMatches();
		
		ps.println("===========STARTs============/done");
	}

	public void stop() {
		this.running = src.main.java.constants.Tournament.TOURNAMENT_STOPPED;

		/* TODO : Store log files */

		sw.terminate();
		r.stop();
		rw.stop();
		s.stop();
		sw.stop();
		DatabaseManager.setTournamentDone(id);
		ps.println("Shutting down tournament " + id);
		TournamentEngineServer.setTournamentDone(id);

	}

	public void printPlayers() {
		for (int i : players) {
			ps.println(DatabaseManager.getPlayerName(i));
		}
	}

	public synchronized void printMatches() {
		ps.println("====================");
		if (match_Q.size() == 0) {
			ps.println("No Matches!!");
		}
		for (int i = 0; i < match_Q.size(); i++) {
			ps.println("Match " + i + ":");
			ps.println(match_Q.get(i).toString());
		}
		ps.println("====================");
	}

	public synchronized void playerError(int player_id, int match_id) throws IOException {
		ps.println("Player " + player_id + " has encountered a problem");
		
		if (!isPaused()) {
			DatabaseManager.setMatchUpcoming(match_id);
			DatabaseManager.setPlayerScheduled(player_id, this.id);
			DatabaseManager.setPlayerInactive(player_id, this.id);
		}

		if (player_strikes.containsKey(player_id)) {
			int st = player_strikes.get(player_id);
			if (st == src.main.java.constants.Tournament.PLAYER_STRIKES_ALLOWED) {
				deactivatePlayer(player_id);
			}
			player_strikes.replace(match_id, st, st+1);
		} else {
			player_strikes.put(match_id, 1);
		}
		if (running_threads.containsKey(match_id)) {
			running_threads.get(match_id).stopMediator();
		}
		
		//clean up log files of faulty match
		if (!match_Q.contains(match_id)) {
			match_Q.addFirst(match_id);
		}
	}

	public synchronized void engineError(int id, int match_id) {
		ps.println("Engine has encountered a problem!");
		if (running_threads.containsKey(match_id)) {
			running_threads.get(match_id).stopMediator();
			if (!match_Q.contains(match_id)) {
				match_Q.addFirst(match_id);
			}
		}

		if (match_strikes.containsKey(match_id)) {
			int st = match_strikes.get(match_id);
			if (st == src.main.java.constants.Tournament.ENGINE_STRIKES_ALLOWED) {
				/* TODO : Match faulty */
				match_strikes.remove(match_id);
			}
			match_strikes.replace(match_id, st, st+1);
		} else {
			match_strikes.put(match_id, 1);
		}
	}

	public synchronized void gameCreateError(int match_id) {
		ps.println("Could not start the match :" + match_id);
		if (match_strikes.containsKey(match_id)) {
			int st = match_strikes.get(match_id);
			if (st == src.main.java.constants.Tournament.MATCH_STRIKES_ALLOWED) {
				/* TODO : Match faulty */
				match_strikes.remove(match_id);
			} else {
				match_strikes.replace(match_id, st, st+1);
				running_threads.get(match_id).stopMediator();
				running_threads.remove(match_id);
				if (!match_Q.contains(match_id)) {
					match_Q.addFirst(match_id);
				}
			}

		} else {
			match_strikes.put(match_id, 1);
			running_threads.get(match_id).stopMediator();
			running_threads.remove(match_id);
			if (!match_Q.contains(match_id)) {
				match_Q.addFirst(match_id);
			}
		}

	}

	private boolean maxRunningThreads() {
		return(running_threads.size() >= src.main.java.constants.Tournament.MAX_RUNNING_MATCHES);
	}

	private synchronized void startNextMatch() {
		ps.println("===========START MATCH============");
		int id = match_Q.removeFirst();
		ps.println("Running match " + id);
		Tournament thiss = this;
		Mediator m;
		try {
			m = new Mediator(thiss, id);
			running_threads.put(id, m);
			running_threads.get(id).start();
			ps.println("started match");
			ps.println("===========START MATCH============/done");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean isPaused() {
		return (running == src.main.java.constants.Tournament.TOURNAMENT_PAUSED);
	}

	private boolean isRunning() {
		return (running == src.main.java.constants.Tournament.TOURNAMENT_RUNNING);
	}

	private boolean isComplete() {
		return (running == src.main.java.constants.Tournament.TOURNAMENT_COMPLETE);
	}

	private boolean isDone() {
		//Improve done condition
		return (!moreMatches && running_threads.isEmpty());
	}

	private boolean isLegalMatch(Collection<Integer> match) {

		for (Integer i : deactivated_players) {
			if (match.contains(i)) {
				return false;
			}
		}

		return true;
	}

	private synchronized void queueNextMatch() {
		//Get next match from scheduler
		ArrayList<Integer> match = sw.getNextMatch();

		//Check if match contains deactivated player
		if (!isLegalMatch(match)) {
			//Deactivated matches table ?
			return;
		}

		//Add match to database
		int matchID = DatabaseManager.addMatch(this.id, match);
		DatabaseManager.setPlayersScheduled(matchID, id);
		ps.println("match assigned id : " +  matchID);

		//Queue match
		ps.println("Queuing next match from scheduler..");
		match_Q.add(matchID);
		ps.println("Queued match " + matchID);
	}

	@Override
	public void run() {
		int sleep = src.main.java.constants.Server.SERVER_SLEEP_DURATION;

		while (isRunning() || isPaused()) {

			//ps.println("Tournament running!");
			if (sw.hasNextMatch()) {

				queueNextMatch();
				//check if i can run next match
				if (!maxRunningThreads()) {
					startNextMatch();
				}

			} else {
				printMatches();
				System.out.println("is Empty : " + match_Q.isEmpty());
				System.out.println("max running  : " + maxRunningThreads());
				if (!match_Q.isEmpty() && !maxRunningThreads()) {
					if (isPaused()) {
						System.out.println("paused!!");
						continue;
					} else {
						System.out.print("Starting next match!");
						startNextMatch();
					}
				} else if (isDone()) {
					this.running = src.main.java.constants.Tournament.TOURNAMENT_COMPLETE;
					DatabaseManager.setTournamentDone(this.id);
					ps.println("Tournament " + this.id + " completed succesfully");
				}
			}
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (isComplete()) {
			while (!running_threads.isEmpty()) {
				//Maybe add timer if match is stuck.
				ps.println("Waiting for ongoing matches to complete");
			}
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ps.println("Remaing matches complete. Shutting down tournament " + this.id);
			this.stop();
		}

	}

}
