package src.main.java.core;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import src.main.java.constants.Network;
import src.main.java.constants.Server;
import src.main.java.database.DatabaseManager;

public class TournamentEngineServer implements Runnable {

	private static HashMap<Integer, Tournament> tournaments = new HashMap<Integer, Tournament>();
	private static HashMap<Integer, Thread> tournament_threads = new HashMap<Integer, Thread>();
	private ServerSocket listen;
	private ObjectInputStream is;
	private InputStream in;
	private ObjectOutputStream os;
	private int port;
	private Socket socket;
	private Thread thread = null;
	private static PrintStream ps;
	private HashMap<Integer, TournamentEngineServerThread> threads = new HashMap<Integer, TournamentEngineServerThread>();

	public TournamentEngineServer (int port) throws FileNotFoundException {
		this.port = port;
		new File(Server.ENGINE_LOG_DIR).mkdirs();
		String dir = Server.ENGINE_LOG_DIR + Server.ENGINE_LOG_NAME;
		File log = new File(dir);
		ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(log)), true);
		try {
			listen = new ServerSocket(port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start();
	}

	public TournamentEngineServer (int port, String state) {
		this.port = port;
		try {
			listen = new ServerSocket(port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start();
	}

	@Override
	public void run () {
		while (thread != null) {
            try {
                ps.println(("Waiting for a client ..."));
                addThread(listen.accept());
            } catch (IOException ioe) {
            	ps.println(("Server accept error: " + ioe));
                stop();
            }
        }

	}

	public void start() {
	    if (thread == null) {
	        thread = new Thread(this);
	        thread.start();
	    }
	}

	public void stop() {
	    if (thread != null) {
	        thread.stop();
	        thread = null;
	    }
	}

	public static void setTournamentDone(int tournament_id) {
		if (tournaments.containsKey(tournament_id)) {
			if (tournament_threads.containsKey(tournament_id)) {
				tournament_threads.remove(tournament_id);
			}
			tournaments.remove(tournament_id);
		}
	}

	public static void setTournamentPaused(int tournament_id) {
		tournaments.get(tournament_id).pause();
	}

	public static void startTournament(int tournament_id) throws FileNotFoundException {
		int engine_id = DatabaseManager.getEngineID(tournament_id);
		int ranker_id = DatabaseManager.getRankerID(tournament_id);
		int scheduler_id = DatabaseManager.getSchedulerID(tournament_id);

		if (tournaments.containsKey(tournament_id)) {
			tournaments.get(tournament_id).start();
		} else {
			Tournament t = new Tournament(tournament_id);
			ps.println("Created Tournament");
			t.start();
			ps.println("INIT Tournament");
			Thread tt = new Thread(t);
			tournament_threads.put(tournament_id, tt);
			tournament_threads.get(tournament_id).start();
			ps.println("Started Tournament");
			tournaments.put(tournament_id, t);
		}
	}

	public static void stopTournament(int tournament_id) {
		tournaments.get(tournament_id).stop();
		//tournament_threads.get(tournament_id).stop();
		
		/* should you really remove the instance here already ? */
		/* Follow the design document and remove instance when the tournament gets deleted */
		/* Well done */
		//tournaments.remove(tournament_id);
		//tournament_threads.remove(tournament_id);
		//ps.println("tournaments left: " + tournaments.size());
	}

	public static void deleteTournament(int tournament_id) {
		if (!tournaments.containsKey(tournament_id)) {
			if (!tournament_threads.containsKey(tournament_id)) {
				return;
			}
			tournament_threads.get(tournament_id).stop();
			tournament_threads.remove(tournament_id);
		}
		tournaments.get(tournament_id).stop();
		//tournament_threads.get(tournament_id).stop();
		tournaments.remove(tournament_id);
		tournament_threads.remove(tournament_id);
		ps.println("tournaments left: " + tournaments.size());
	}

	public static void addPlayer(int player_id, int tournament_id) {
		ps.println("Player id :" + player_id + " is joining tournament id :" + tournament_id);
		tournaments.get(tournament_id).addPlayer(player_id);
		ps.println("Added player to tournament");
		tournaments.get(tournament_id).printPlayers();
		ps.println("Printed player to tournament");
	}

	public static void removePlayer(int player_id, int tournament_id) {
		ps.println("Player id :" + player_id + " is being removed from tournament id :" + tournament_id);
		tournaments.get(tournament_id).removePlayer(player_id);
		ps.println("Removed player from tournament");
		tournaments.get(tournament_id).printPlayers();
	}

	public synchronized void handle(int clientPort, String input) throws FileNotFoundException {

		String[] cmd = input.split(" ");
		switch (cmd[0]) {
			case "Start_tournament" : {
				int tournament_id = Integer.parseInt(cmd[1]);
				startTournament(tournament_id);
				break;
			}
			case "Pause_tournament" : {
				int tournament_id = Integer.parseInt(cmd[1]);
				setTournamentPaused(tournament_id);
				break;
			}
			case "Stop_tournament" : {
				int tournament_id = Integer.parseInt(cmd[1]);
				stopTournament(tournament_id);
				break;
			}
			case "Delete_tournament" : {
				int tournament_id = Integer.parseInt(cmd[1]);
				deleteTournament(tournament_id);
				break;
			}
			case "Add_player" : {
				int tournament_id = Integer.parseInt(cmd[2]);
				int player_id = Integer.parseInt(cmd[1]);
				addPlayer(player_id, tournament_id);
				break;
			}
			case "Remove_player" : {
				int tournament_id = Integer.parseInt(cmd[2]);
				int player_id = Integer.parseInt(cmd[1]);
				removePlayer(player_id, tournament_id);
				break;
			}

			case "Restart_server" : {
				exportTournaments();
				break;
			}
		}
	}

	public synchronized void exportTournaments() {
		//Ensure all tournaments are paused.
		for (int i : tournaments.keySet()) {
			tournaments.get(i).pause();
			//tournaments.export();
		}
	}

	public synchronized void remove(int clientPort) {
	   try {
		threads.get(clientPort).close();
	} catch (IOException e) {
		e.printStackTrace();
	}
	   threads.get(clientPort).stop();
	   System.out.print("web server api call done");
	   threads.remove(clientPort);
	}

	private void addThread(Socket socket) {
		ps.println(("Client Accepted: " + socket));
		int port = socket.getPort();
		threads.put(port, new TournamentEngineServerThread(this, socket, port));
        try {
        	threads.get(port).open();
        	threads.get(port).start();
        } catch (IOException ioe) {
            ps.println(("Error opening thread: " + ioe));

        }
	}

	public static void main(String[] args) throws FileNotFoundException {
		int port = Network.WEB_SERVER_PORT;
		TournamentEngineServer tes =  new TournamentEngineServer(port);
	}

}
