package src.main.java.core;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.nio.charset.Charset;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import src.main.java.database.DatabaseManager;
import src.main.java.utils.ShellExec;

public class Mediator extends Thread {
	
	private Tournament t;
	private String t_name;
	private String engineDir;
	private String enginejar;
	private String refname;
	private String networkName;
	private String lobbyName;
	private String config;
	private String g_name;
	private String[] resources = new String[2];
	private int res_id;
	private int engine_id;
	private int matchID;
	private int num_players;
	private Map<Integer, String> player_names = new HashMap<>();
	private Map<Integer, String> player_jars = new HashMap<>();
	private Map<Integer, String> player_classnames = new HashMap<>();
	private Map<Integer, Thread> player_threads = new HashMap<>();
	private int gameport;
	private static PrintStream ps;
	static boolean first = true;
	private volatile boolean stop = false;
	Thread engine;
	private int sleep = src.main.java.constants.Server.SERVER_SLEEP_DURATION;
	
	public Mediator(Tournament t, int id) throws FileNotFoundException {
		new File(src.main.java.constants.Tournament.MEDIATOR_LOG_DIR).mkdirs();
		String dir = src.main.java.constants.Tournament.MEDIATOR_LOG_DIR + src.main.java.constants.Tournament.getMediatorLogName(id);
		File log = new File(dir);
		ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(log)), true);
		this.t = t;
		this.matchID = id;
		this.engine_id = DatabaseManager.getEngineID(t.getID());
		
		//Set up names and referee path
		this.refname = "ref_"+ matchID;
		this.networkName = "match_"+matchID;
		this.lobbyName = "lobby_" + matchID;
		this.g_name = DatabaseManager.getEngineName(engine_id);
		this.enginejar = DatabaseManager.getEngineDir(engine_id);
		this.engineDir = this.g_name + "=" + DatabaseManager.getEnginePath(engine_id);
		this.config = "\"" + DatabaseManager.getTournamentConfig(t.getID()) + "\"";
		this.res_id = DatabaseManager.getResourceID(t.getID());
		this.resources = DatabaseManager.getResources(this.res_id);
		this.num_players = DatabaseManager.getNumPlayersInMatch(matchID);
		ps.println(engineDir);
		ps.println(enginejar);
		getPlayers();
		this.gameport = 61234;
		
		/*try {
			ServerSocket s = new ServerSocket(0);
			this.gameport = s.getLocalPort();
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		this.t_name = DatabaseManager.getTournamentName(t.getID());
		
	}
	
	public void stopMediator() {
		this.stop = true;
	}
	
	@Override
	public void run() {
		launchEngine();
		boolean done = false;
		while (!done) {
			done = true;
			Set<Integer> ss = player_threads.keySet();
			for (int i : ss) {
				if (player_threads.get(i).isAlive()) {
					done = false;
					break;
				}
			}
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (stop) {
				cleanup();
				return;
			}
		}
		
		if (stop) {
			cleanup();
			return;
		}
		
		
		ps.println("MATCH DONE!");
		//docker stop ref
		engine.interrupt();
		getResults();
	}
	
	public void getPlayers() {
		String players = DatabaseManager.getPlayersInMatch(this.matchID);
		if (players.equals("none")) {
			//discard match
			return;
		}
		String[] ids = players.split(",");
		int id;
		for (String s : ids) {
			id = Integer.parseInt(s);
			this.player_names.put(id, DatabaseManager.getPlayerName(id));
			this.player_jars.put(id, DatabaseManager.getPlayerDir(id));
			this.player_classnames.put(id, DatabaseManager.getPlayerClassName(id));
		}
	}
	
	private synchronized void killAll() {
		for (int i : player_threads.keySet()) {
			player_threads.get(i).interrupt();
		}
		
		engine.interrupt();
		this.stop();
		this.interrupt();
	}
	
	private void launchEngine() {
		DatabaseManager.setMatchActive(matchID);
		DatabaseManager.setPlayersActive(matchID, t.getID());
		String dirN = "/var/www/TEResources/Logs/";
		new File(dirN).mkdirs();
		File logN = new File(dirN+"network.txt");
		String[] ar = {"docker", "network", "create", "match_"+matchID};
		Process createNet = ShellExec.buildProcess(ar, logN);
		try {
			createNet.waitFor();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		engine = new Thread(){
			@Override
			public void run() {
				String jar = enginejar;
				String caa = engineDir;
				String caaa = refname;
				String nn = networkName;
				String portval = Integer.toString(gameport);
				String[] args = {"docker", "run", "--name", caaa, "--network", nn, "-i", "-e", "JARNAME="+jar, "-e", "REFNAME="+caa, "-e", "PORT="+portval, "ref:latest"};
				new File("/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/").mkdirs();
				String engineName = DatabaseManager.getEngineName(engine_id);
				String dir = "/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/" + engineName + ".log";
				File log = new File(dir);
				if (!DatabaseManager.playerHasLogFile(matchID,-1, dir)) {
					DatabaseManager.setPlayerLogDir(matchID,-1, dir);
				}
				Process engineProcess = ShellExec.buildProcess(args, log);
				try {
					engineProcess.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					engineProcess.destroy();
				}
				while (engineProcess.isAlive()){}
				if (engineProcess.exitValue() != 0) {
					t.engineError(engine_id,matchID);
					
				}
			};
		};
		engine.start();
		
		try {
			Thread.sleep(sleep+1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		launchGameLobby();
		
		try {
			Thread.sleep(sleep+1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		launchPlayers();
		try {
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		copyFiles();
		
	}
	
	public void copyFiles() {
		String cnf = DatabaseManager.getTournamentConfig(t.getID());
		try {
			String[] pargs = {"docker", "cp", enginejar, refname+":/ref.jar"};
			String[] p2args = {"docker", "cp", enginejar, lobbyName+":/ref.jar"};
			String[] p3args = {"docker", "cp", cnf, lobbyName+":/config.json"};
			Process p = Runtime.getRuntime().exec(pargs);
			p.waitFor();
			while (p.exitValue() != 0) {
				p = Runtime.getRuntime().exec(pargs);
				p.waitFor();
			}
			Process p2 = Runtime.getRuntime().exec(p2args);
			p2.waitFor();
			while (p2.exitValue() != 0) {
				p2 = Runtime.getRuntime().exec(p2args);
				p2.waitFor();
			}
			Process p3 = Runtime.getRuntime().exec(p3args);
			p3.waitFor();
			while (p3.exitValue() != 0) {
				p3 = Runtime.getRuntime().exec(p3args);
				p3.waitFor();
			}
			Set<Integer> s = player_names.keySet();
			
			for (int i : s) {
				String[] p4args = {"docker", "cp", cnf, "player_"+i+"_"+matchID+":/config.json"};
				p3 = Runtime.getRuntime().exec(p4args);
				p3.waitFor();
				while (p3.exitValue() != 0) {
					p3 = Runtime.getRuntime().exec(p4args);
					p3.waitFor();
				}
				p3.destroy();
				p4args = null;
				Thread.sleep(sleep);
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void launchGameLobby() {
		Thread create_game = new Thread(){
			@Override
			public void run() {
				//java -jar IngeniousFrame-all-0.0.2.jar create -config "Uno.json" -game "Uno" -lobby "mylobby" -players 3 &
				String jar = enginejar;
				String caaa = refname;
				String nn = networkName;
				String caa = config;
				String caa1 = g_name;
				String caa2 = lobbyName;
				String caa3 = Integer.toString(num_players);
				String caa4 = Integer.toString(gameport);
				
				String[] args = {"docker", "run", "--name", caa2 ,"--network" , nn, "-i", "-e", "JARNAME="+jar, "-e" , "CONFIG="+caa, "-e", "GAME="+caa1, "-e", "PORT="+caa4, "-e", "PLAYERS="+caa3, "-e", "HOSTNAME="+caaa, "lobby:latest"};
				new File("/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/").mkdirs();
				String dir = "/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/create_game.log";
				File log = new File(dir);
				Process engineProcess = ShellExec.buildProcess(args, log);
				try {
					engineProcess.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					engineProcess.destroy();
				}
				if (engineProcess.exitValue() != 0) {
					t.gameCreateError(matchID);
					
				}
			};
		};
		
		create_game.start();
	}
	
	public void getResults() {
		String engineName = DatabaseManager.getEngineName(engine_id);
		String dir = "/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/" + engineName + ".log";
		Charset encoding = Charset.defaultCharset();
		String scores = "";
		boolean results = false;
		boolean state = false;
		String final_state = "";
		try {
			List<String> lines = Files.readAllLines(Paths.get(dir), encoding);
			for (String s : lines) {
				if (s.toUpperCase().contains("SCORES")) {
					results = true;
				} else if (s.toUpperCase().contains("FINAL STATE")) {
					results = false;
					state = true;
				}
				if (results) {
					scores += s + "\n";
				} else if (state) {
					final_state += s + "\n";
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		scores = scores.substring(scores.indexOf('\n')+1, scores.length());
		String[] player_scores = scores.split("\n");
		Set<Integer> s = player_names.keySet();
		
		int j = 0;
		String res = "";
		int winner = -1;
		for (int i : s) { 
			res += i + " " + player_scores[j].substring(player_scores[j].lastIndexOf(':')+1, player_scores[j].length()).trim() + ",";
			if (player_scores[j].substring(player_scores[j].lastIndexOf(':')+1, player_scores[j].length()).trim().equals("1.0")) {
				winner = i;
			}
			j++;
		}
		if (res.charAt(res.length()-1) == ',') {
			res = res.substring(0, res.length()-1);
		}
		
		final_state = final_state.substring(final_state.lastIndexOf(':')+1, final_state.length());
		
		
		ps.println("================");
		ps.println(res);
		ps.println("================");
		ps.println(final_state);
		
		
		t.new_result(res, matchID, winner);
		cleanup();
		this.stop();
		
	}
	
	public void cleanup () { 
		//Stop and remove all player containers
		Set<Integer> s = player_names.keySet();
		for (int i : s) {
			String[] argsStop = {"docker", "kill", "player_"+i+"_"+matchID};
			String[] argsRm = {"docker", "rm", "player_"+i+"_"+matchID};
			try {
				Process p = Runtime.getRuntime().exec(argsStop);
				p.waitFor();
				Process p1 = Runtime.getRuntime().exec(argsRm);
				p1.waitFor();
				p.destroy();
				p1.destroy();
				argsStop = null;
				argsRm = null;
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//Lobby and ref stop and remove
		String[] argsStop = {"docker", "kill", lobbyName, refname};
		String[] argsRm = {"docker", "rm", lobbyName, refname};
		try {
			Process p = Runtime.getRuntime().exec(argsStop);
			p.waitFor();
			Process p1 = Runtime.getRuntime().exec(argsRm);
			p1.waitFor();
			p.destroy();
			p1.destroy();
			argsStop = null;
			argsRm = null;
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Stop and remove network
		String[] argsNetRm = {"docker", "network", "rm", networkName};
		
		try {
			Process pNet = Runtime.getRuntime().exec(argsNetRm);
			pNet.waitFor();
			pNet.destroy();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.stop();
		
		
	}
	
	public void launchPlayers() {
		Set<Integer> s = player_names.keySet();
		for (int i : s) {
			String jar_file = player_jars.get(i);
			String classname = player_classnames.get(i);
			String username = player_names.get(i);
			
			player_threads.put(i, new Thread(){
				@Override
				public void run() {
					String jar = jar_file;
					String nn = networkName;
					String host = refname;
					String caa = username;
					String caa1 = classname;
					String caa2 = g_name;
					String caa3 = refname;
					String caa4 = Integer.toString(gameport);
					if (res_id == -1) {
						String[] args = {"docker", "run", "--name", "player_"+i+"_"+matchID ,"--network" , nn, "-i" ,"-e", "JARNAME="+jar_file, "-e" , "PLAYERNAME="+username, "-e", "PLAYERPATH="+classname, "-e", "GAME="+caa2, "-e", "PORT="+caa4, "-e", "HOSTNAME="+host, "player:latest"};
						new File("/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/players/" + i +"/").mkdirs();
						String dir ="/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/players/" + i +"/" + DatabaseManager.getPlayerName(i) + ".log";
						File log = new File(dir);
						if (!DatabaseManager.playerHasLogFile(matchID, i, dir)) {
							DatabaseManager.setPlayerLogDir(matchID, i, dir);
						}
						Process engineProcess = ShellExec.buildProcess(args, log);
						try {
							engineProcess.waitFor();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							engineProcess.destroy();
						}
						while (engineProcess.isAlive()){}
						if (engineProcess.exitValue() != 0) {
							cleanup();
							try {
								t.playerError(i, matchID);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						String[] args = {"docker", "run", "--name", "player_"+i+"_"+matchID ,"--network" , nn, "--memory=" + resources[0], "--cpus=" + resources[1] , "-i" ,"-e", "JARNAME="+jar_file, "-e" , "PLAYERNAME="+username, "-e", "PLAYERPATH="+classname, "-e", "GAME="+caa2, "-e", "PORT="+caa4, "-e", "HOSTNAME="+host, "player:latest"};
						new File("/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/players/" + i +"/").mkdirs();
						String dir ="/var/www/TEResources/Logs/" + t_name + "/matches/" + matchID + "/players/" + i +"/" + DatabaseManager.getPlayerName(i) + ".log";
						File log = new File(dir);
						if (!DatabaseManager.playerHasLogFile(matchID, i, dir)) {
							DatabaseManager.setPlayerLogDir(matchID, i, dir);
						}
						
						Process engineProcess = ShellExec.buildProcess(args, log);
						try {
							engineProcess.waitFor();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							engineProcess.destroy();
						}
						while (engineProcess.isAlive()){}
						if (engineProcess.exitValue() != 0) {
							try {
								t.playerError(i, matchID);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					
				};
			});
			player_threads.get(i).start();
			
			try {
				Thread.sleep(sleep);
				String[] args = {"docker", "cp", jar_file, "player_"+i+"_"+matchID+":/player.jar"};
				Process p = Runtime.getRuntime().exec(args);
				p.waitFor();
				while (p.exitValue() != 0) {
					p = Runtime.getRuntime().exec(args);
					p.waitFor();
				}
				args = null;
				Thread.sleep(sleep);
			} catch (InterruptedException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
