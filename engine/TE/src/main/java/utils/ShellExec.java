package src.main.java.utils;

import java.io.File;
import java.io.IOException;

public class ShellExec {

    public static Process buildProcess(String[] args, File log){
		ProcessBuilder builder = new ProcessBuilder(args);
		if(!log.exists()){
	        try {
				log.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
		try {
			builder.redirectOutput(log);
			Process process = builder.start();
			return process;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}


