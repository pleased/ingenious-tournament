package src.test.java;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import src.main.java.core.Tournament;

public class TournamentTests {
	

	@Test
	public void TournamentCreatetest() {
		try {
			Tournament t = new Tournament(1);
			Thread tt = new Thread(t);
			t.start();
			tt.start();
			t.pause();
			t.start();
			t.addPlayer(9);
			t.printPlayers();
			t.removePlayer(1);
			assertEquals(1, t.getID());
			Thread.sleep(1000000);
			t.stop();
			//t.gameCreateError(0);
			//t.engineError(1, 1);
			//t.playerError(1, 1);
			Thread.sleep(10000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
