package src.test.java;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import src.main.java.core.SchedulerWrapper;

public class SchedulerWrapperTest {

	@Test
	public void test() throws FileNotFoundException {
		int sched_id = 1;
		int t_id = 1;
		ArrayList<Integer> players = new ArrayList<>();
		players.add(1);
		players.add(2);
		SchedulerWrapper sw = new SchedulerWrapper(sched_id, t_id, players);
		Thread swt = new Thread(sw);
		swt.start();
		ArrayList<Integer> match = new ArrayList<>();
		ArrayList<Integer> posMatch = new ArrayList<>();
		posMatch.add(1);
		posMatch.add(2);
		while (true) {
			if (sw.hasNextMatch()) {
				match = sw.getNextMatch();
				for (int i : match) {
					System.out.println(i);
				}
				assertEquals(posMatch, match);
				System.out.println("True");
				sw.add_player(3);
				sw.remove_player(3);
				String res = "player1 1.0, player2 0.0";
				sw.new_result(res);
				sw.terminate();
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
