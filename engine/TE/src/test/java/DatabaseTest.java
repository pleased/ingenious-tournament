package src.test.java;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import src.main.java.database.DatabaseManager;

public class DatabaseTest {

	@Test
	public void getTournamentInfotest() {
		assertEquals(1, DatabaseManager.getEngineID(1));
		assertEquals(1, DatabaseManager.getRankerID(1));
		assertEquals(1, DatabaseManager.getSchedulerID(1));
		assertEquals(1, DatabaseManager.getResourceID(1));
		assertEquals("1,2", DatabaseManager.getPlayers(1));
		assertEquals("TestTournament", DatabaseManager.getTournamentName(1));
		assertEquals("/var/www/html/TE/uploads/config_uploads/tournament_1.json", DatabaseManager.getTournamentConfig(1));
		assertEquals(true, DatabaseManager.setTournamentDone(1));
		String[] resources = {"1", "2gb"};
		String[] resourceDB = DatabaseManager.getResources(1);
		assertEquals(resources[0],resourceDB[0]);
		assertEquals(resources[1],resourceDB[1]);
	}
	
	@Test
	public void getRankerInfoTest() {
		assertEquals("/var/www/html/TE/uploads/ranker_uploads/EloRanker.jar", DatabaseManager.getRankerDir(1));
		assertEquals("EloRanker", DatabaseManager.getRankerName(1));
	}
	
	
	@Test
	public void getSchedulerInfoTest() {
		assertEquals("/var/www/html/TE/uploads/scheduler_uploads/2PlayerRoundRobin.jar", DatabaseManager.getSchedulerDir(1));
		assertEquals("2PlayerRoundRobin", DatabaseManager.getSchedulerName(1));
	}
	
	@Test
	public void getEngineInfoTest() {
		assertEquals("za.ac.sun.cs.ingenious.games.mnk.MNKReferee", DatabaseManager.getEnginePath(1));
		assertEquals("MNKReferee", DatabaseManager.getEngineName(1));
		assertEquals("/var/www/html/TE/uploads/engine_uploads/MNKReferee.jar", DatabaseManager.getEngineDir(1));
	}
	
	@Test
	public void getPlayerInfoTest() {
		assertEquals("za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine", DatabaseManager.getPlayerClassName(1));
		assertEquals("MNKRandomEngine", DatabaseManager.getPlayerName(1));
		assertEquals("/var/www/html/TE/uploads/player_uploads/MNKRandomEngine.jar", DatabaseManager.getPlayerDir(1));
		
		//Player in tournament
		assertEquals(0, DatabaseManager.getPlayerRating(1, 1));
		assertEquals(true, DatabaseManager.newRating(1, 100, 1));
		assertEquals(100, DatabaseManager.getPlayerRating(1, 1));
		assertEquals(true, DatabaseManager.newRating(1, 0, 1));
		assertEquals(1, DatabaseManager.getPlayerTournamentId(1, 1));
		assertEquals(true, DatabaseManager.deactivatePlayer(1,1));
		assertEquals(true, DatabaseManager.reactivatePlayer(1,1));
	}
	
	@Test
	public void getUserInfoTest() {
		assertEquals((int)1, (int)DatabaseManager.getUser(1));
	}
	
	
	@Test
	public void matchTests() {
		ArrayList<Integer> a = new ArrayList<>();
		a.add(1);
		a.add(2);
		
		int m_id = DatabaseManager.addMatch(1, a);
		assertEquals("1,2", DatabaseManager.getPlayersInMatch(m_id));
		assertEquals(2, DatabaseManager.getNumPlayersInMatch(m_id));
		
		//Match Status
		assertEquals(true, DatabaseManager.setMatchUpcoming(m_id));
		assertEquals(true, DatabaseManager.setMatchActive(m_id));
		assertEquals(true, DatabaseManager.setMatchDone(m_id));
		
		//Match players log dirs
		assertEquals(true, DatabaseManager.setPlayerLogDir(m_id, 1, ""));
		assertEquals(true, DatabaseManager.removePlayerLogDir(m_id));
		
		//Match players statuses
		assertEquals(true, DatabaseManager.setPlayersActive(m_id, 1));
		assertEquals(true, DatabaseManager.setPlayersScheduled(m_id, 1));
		assertEquals(true, DatabaseManager.setPlayersInactive(m_id, 1));
		
		//Match results
		int match_result = DatabaseManager.newMatchResult(m_id, 1);
		assertEquals(true, DatabaseManager.setMatchResult(m_id, match_result));
		
		assertEquals(false, DatabaseManager.RemoveMatch(m_id));
	}

}
